# Broader Horizons
Welcome to Broader Horizons, your guide to cultured cooking. 
Explore thousands of unique and interesting recipes, the ingredients required to construct them, and the various cuisines and cultures they are sourced from. 
Connect to your kitchen to move beyond takeout and rediscover the joy of finding ingredients and constructing recipes from scratch across a wide variety of global food.
Start your journey today and broaden your horizons at https://broaderhorizons.me and discover the world's cuisine.


## Group Members

| Name | EID | GitLab ID |
| --- | --- | --- |
| Amogh Dambal | asd2684 | amogh-dambal |
| Thomas Peavler | tcp663 | thomaspeavler2 |
| Kang (Kevin) Hou | kh37228 | kevinhou912 |
| Jeremy Tajonera | jrt3584 | jtajonera1  |

## Git SHA
Phase I: ceb4accf8970e406022294b197285094518f25d4

Phase II: 6eb98eb6a8e80afa21f048e3a056b04bfca59fd1

Phase III: 78ff5644569038fc2787c4c1739db2fc02e3f5aa

Phase IV: b3261fe19019bb527457ec311849185760f8290b

## Project Leaders
Phase I: Thomas Peavler

Phase II: Amogh Dambal

Phase III: Kang (Kevin) Hou

Phase IV: Jeremy Tajonera

## GitLab Pipelines
https://gitlab.com/thomaspeavler2/cs373-project/-/pipelines

## Postman API
Visit our API at https://api.broaderhorizons.me/test. Our API endpoints
all start with https://api.broaderhorizons.me/api/. For full documentation
on our API, please visit our Postman link at
https://documenter.getpostman.com/view/17727633/UUy1en98

## Website
Check us out at https://broaderhorizons.me or https://www.broaderhorizons.me !

## Estimated Completion Times
### Phase I
| Name | Estimated Completion Time | Actual Completion Time |
| --- | --- | --- |
| Amogh Dambal | 10 hours | 20 hours |
| Thomas Peavler | 10 hours | 25 hours |
| Koustubh Nyshadham | 4 hours | 20 hours |
| Kang (Kevin) Hou | 3 hours | 20 hours |
| Jeremy Tajonera | 5 hours | 21 hours  |

### Phase II
| Name | Estimated Completion Time | Actual Completion Time |
| --- | --- | --- |
| Amogh Dambal | 25 hours | 30 hours |
| Thomas Peavler | 25 hours | 28 hours |
| Kang (Kevin) Hou | 25 hours | 25 hours |
| Jeremy Tajonera | 25 hours | 25 hours  |

### Phase III
| Name | Estimated Completion Time | Actual Completion Time |
| --- | --- | --- |
| Amogh Dambal | 20 hours | 22 hours |
| Thomas Peavler | 15 hours | 20 hours |
| Kang (Kevin) Hou | 15 hours | 15 hours |
| Jeremy Tajonera | 20 hours | 15 hours  |

### Phase IV
| Name | Estimated Completion Time | Actual Completion Time |
| --- | --- | --- |
| Amogh Dambal | 15 hours | 20 hours |
| Thomas Peavler | 10 hours | 28 hours |
| Kang (Kevin) Hou | 20 hours | 15 hours |
| Jeremy Tajonera | 10 hours | 20 hours  |

## Comments
Phase II: Connecting the database to our website was very interesting.

Phase III: So many of the bugs in the backend API we developed were because of spelling errors. So. many.

Phase IV: As we come to the conclusion of our project, we would like to thank Prof. Glenn Downing for being a fantastic instructor throughout the course of this class.
We would also like to extend our incredible gratitude to our TAs: Mengning, Caitlin, Jefferson, and Alexander, for being incredibly helpful and understanding
as we worked on various aspects of front-end, back-end, and full stack development. Our completion of this project would not have been possible without
their help and support. From staying later than required during help sessions and office hours to answering our questions on Discord and providing
us extensions when we needed it the most, we can't thank you enough. Love you all. <3

### References
Phase I:
In the course of completing this phase, we used as reference all of the links
provided in the following [document](https://docs.google.com/document/d/1bZ5iZ8nCBvNXUjDUn9jqkalrLrsphGLqbTiiQpAvCds/edit), 
especially the provided techncial report, and Postman API documentation. 

We also derived some inspiration from https://culturedfoodies.me for some sections of the
About page and other static web pages. 

Phase II:
In the course of completing this phase, we used as reference all of the links
provided in the [Phase 2 Grading Specification](https://docs.google.com/document/d/1k5BUYfYpoE8c4l1g03ZC8C-3u0-W_n-_uOIEFnHqHXM/edit?usp=sharing) document.

We also utilized as reference the repositories of [Texas Votes](https://gitlab.com/forbesye/fitsbits) and [Burnin' Up](https://gitlab.com/caitlinlien/cs373-sustainability) for the development of many parts of the backend API and the documentation, including but not limited to the Postman API documentation, the technical report, database diagram, and Docker setup. These were provided to us by the TAs Caitlin Lien and Jefferson Ye, respectively, and we thank them generously and sincerely for these resources. 

We also utilized as reference the repository of [CulturedFoodies - CS 373 Group 11, Spring 2021](https://gitlab.com/cs373-group-11/cultured-foodies) for the development model of the backend API, APIs to parse data from and continuous integration & build setup. We thank them thoroughly and sincerely. 

Phase III:
In the course of completing this phase, we used as reference all of the links
provided in the [Phase 3 Grading Specifications](https://docs.google.com/document/d/1Gfy3fww82YFlZvr4HmvM8h8zlPXgWEq4LPSApjfbOxc/edit) document.

We also utilized as reference and design model the repository of [Texas Votes](https://gitlab.com/forbesye/fitsbits) for 
the development and refactoring of our backend API. In particular, we utilized their searching, sorting, and filtering models
and SQL function applications. We also used them as a model and source for the design of our backend pagination. This resource
was provided to us by the TA Jefferson Ye, and we thank him and all the TAs generously and sincerely for these resources.

We also utilized as reference the repository of [CulturedFoodies - CS 373 Group 11, Spring 2021](https://gitlab.com/cs373-group-11/cultured-foodies) for the development model of the frontend in both the static and dynamic rendering of the pages. We thank them thoroughly and sincerely. 

Phase IV: 
In the course of completing this phase, we continued to use as reference all of the links
provided in the [Phase 4 Grading Specifications](https://docs.google.com/document/d/1TDnu6RIn9VhgZbOG9mHPWnM4NYFzHsyLINbYgPdwFf0/edit) document.

We also utilized as reference for the visualizations on our website many of the examples provided in the D3 [documentation](https://github.com/d3/d3/wiki).

