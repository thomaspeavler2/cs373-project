import React from "react";
import {
  BrowserRouter as Router,
  Route,
  Switch,
} from "react-router-dom";
import { ThemeProvider } from '@mui/material/styles';
import NavBar from './components/navbar/NavBar';
import { theme } from './utils/constants';
import HomeRoute from "./routes/home";
import SearchRoute from "./routes/search";
import AboutRoute from "./routes/about";
import ProviderVisualizationRoute from "./routes/providerVisualizations";
import VisualizationRoute from "./routes/visualizations";

function App() {
  return (
    <ThemeProvider theme={theme}>
      <Router>
        <NavBar />
        <Switch>
          <Route exact path="/">
            <HomeRoute />
          </Route>
          <Route path="/recipes">
            <SearchRoute name="Recipes" />
          </Route>
          <Route path="/ingredients">
            <SearchRoute name="Ingredients" />
          </Route>
          <Route path="/cuisines">
            <SearchRoute name="Cuisines" />
          </Route>
          <Route path="/searchresults">
            <SearchRoute name="Everything" />
          </Route>
          <Route path="/providerVisualization">
            <ProviderVisualizationRoute />
          </Route>
          <Route path="/visualization">
            <VisualizationRoute />
          </Route>
          <Route path="/about">
            <AboutRoute />
          </Route>
        </Switch>
      </Router>
    </ThemeProvider>
  );
}

export default App;
