
export const recipes = [
  {
    "name": "Alfredo",
    "link_left": "Pasta",
    "link_right": "Italian",
    "image": "https://www.jessicagavin.com/wp-content/uploads/2020/07/how-to-cook-pasta-2.jpg",
    "text": `The first step to making Alfredo is to warm your butter and cream in a large saucepan over low heat.  You will let it simmer (but not boil) for about 2 minutes.\n\n
      Next, you will whisk in your garlic and seasonings.  Whisking constantly for about one minute just to let the garlic cook.
      Lastly, you will add your parmesan cheese and stir just until the cheese has melted and your sauce is smooth.
      Note that it is best to use freshly grated parmesan cheese right off of the block so that your sauce isn't grainy.  
      I do not recommend using the "shaker" style container grated cheese for this recipe.`,
    "images": [
      "https://www.jessicagavin.com/wp-content/uploads/2020/07/how-to-cook-pasta-2.jpg",
      "https://cdn.loveandlemons.com/wp-content/uploads/2020/02/alfredo-sauce.jpg",
      "https://www.jessicagavin.com/wp-content/uploads/2019/08/chicken-alfredo-8-1200.jpg"
    ],
    "video": "https://www.youtube.com/watch?v=FLEnwZvGzOE",
    "preparation_time": 45,
    "servings": 4,
    "ingredients": [
      "Pasta", "Cream", "Chicken"
    ],
    "vegan/vegetarian": false,
    "cost": 15.99,
    "dairy_free": false,
    "gluten_free": false,
    "cuisines": [
      "Italian"
    ]
  },
  {
    "name": "Cajun Spiced Fish Tacos",
    "link_left": "White Fish",
    "link_right": "Cajun",
    "image": "https://www.themealdb.com/images/media/meals/uvuyxu1503067369.jpg",
    "text": `We’re taking a shortcut with this recipe—and weeknight dinners have to be all about shortcuts, right?—and 
      using a really good store-bought marinade and Tony’s Original Creole Seasoning blend. With this little dinner hack,
      these fish tacos come together quick. Here are a few tips to make your taco-making journey just a little easier: 
      Have the fishmonger (and by fishmonger, we mean whoever is behind the fish counter) cut your tilapia or halibut 
      into 2-3 inch pieces for you. Usually they’re happy to do it, and this will save you some messy prep.
      Toss the cut fish with Tony’s Creole Style Seafood Marinade and let it sit in the fridge for at least 30 minutes.
      Coat the marinated fish in panko breadcrumbs and bake until it is crisp on the outside and moist and tender on the inside—just about ten minutes.`,
    "images": [
      "https://www.themealdb.com/images/media/meals/uvuyxu1503067369.jpg",
      "https://www.foodiecrush.com/wp-content/uploads/2017/03/Blackened-Fish-Tacos-Creamy-Avocado-Sauce-foodiecrush.com-012.jpg",
      "https://loveandgoodstuff.com/wp-content/uploads/2021/01/blackened-fish-tacos-11.jpg"
    ],
    "video": "https://www.youtube.com/watch?v=N4EdUt0Ou48",
    "preparation_time": 30,
    "servings": 6,
    "ingredients": [
      "cayenne pepper", "white fish", "vegetable oil", "flour tortilla", "avocado", "little gem lettuce",
      "spring onion", "salsa", "sour cream", "lemon", "garlic"
    ],
    "vegan/vegetarian": false,
    "cost": 10.99,
    "dairy_free": false,
    "gluten_free": false,
    "cuisines": [
      "Cajun"
    ]
  },
  {
    "name": "Irish Stew",
    "link_left": "Lamb Loin Chops",
    "link_right": "Irish",
    "image": "https://www.themealdb.com/images/media/meals/sxxpst1468569714.jpg",
    "text": `Brown the beef:
      Sprinkle about a teaspoon of salt over the beef pieces. Heat the olive oil in a large (6 to 8 quart), thick-bottomed pot over medium-high heat.
      Pat dry the beef with paper towels and working in batches, add the beef (do not crowd the pan, or the meat will steam and not brown) and cook, without stirring, until well browned on one side, then use tongs to turn the pieces over and brown on another side.
      Add garlic and sauté, then add stock, water, Guinness, wine, tomato paste, sugar, thyme Worcestershire, bay leaves, simmer:
      Add garlic to the pot with the beef and sauté 30 seconds or until fragrant. Add the beef stock, water, Guinness, red wine, tomato paste, sugar, thyme, Worcestershire sauce, and bay leaves. Stir to combine.
      Bring mixture to a simmer. Reduce heat to the lowest setting, then cover and cook at a bare simmer for 1 hour, stirring occasionally.
      Sauté onions, carrots in separate pan:
      While the pot of meat and stock is simmering, melt the butter in another pot over medium heat. Add the onions and carrots. Sauté the onions and carrots until the onions are golden, about 15 minutes. Set aside until the beef stew in step 2 has simmered for one hour.
      Add onions, carrots, potatoes to beef stew, simmer:
      Add the onions, carrots, and the potatoes to the beef stew. Add black pepper and two teaspoons of salt. Simmer uncovered until vegetables and beef are very tender, about 40 minutes. Discard the bay leaves. Tilt pan and spoon off any excess fat.
      Transfer stew to serving bowls. Add more salt and pepper to taste. Sprinkle with parsley and serve.`,
    "images": [
      "https://www.themealdb.com/images/media/meals/sxxpst1468569714.jpg",
      "https://hips.hearstapps.com/hmg-prod.s3.amazonaws.com/images/irish-beef-stew-horizontal-1550618467.png",
      "https://www.thespruceeats.com/thmb/MmDG17ztrrKV0pNi4fIUkjL7EHk=/2848x2848/smart/filters:no_upscale()/irish-lamb-stew-recipe-1809131-Hero-5b8bf14e46e0fb0025fa2a9b.jpg",
    ],
    "video": "https://www.youtube.com/watch?v=kYH2qJXnSMo",
    "preparation_time": 135,
    "servings": 6,
    "ingredients": [
      "whole wheat", "lamb loin chops", "olive oil", "shallots", "carrots", "turnips", "celeriac", "charlotte potatoes", "white wine",
      "caster sugar", "fresh thyme", "oregano", "chicken stock"
    ],
    "vegan/vegetarian": false,
    "cost": 25.99,
    "dairy_free": false,
    "gluten_free": false,
    "cuisines": [
      "Irish"
    ]
  }
]

export const ingredients = [
  {
    "name": "Pasta",
    "link_left": "Alfredo",
    "link_right": "Italian",
    "image": "https://www.jessicagavin.com/wp-content/uploads/2020/07/how-to-cook-pasta-3-1200.jpg",
    "text": `Pasta (US: /ˈpɑːstə/, UK: /ˈpæstə/; Italian pronunciation: [ˈpasta]) is a type of food typically 
      made from an unleavened dough of wheat flour mixed with water or eggs, and formed into sheets or other shapes,
      then cooked by boiling or baking. Rice flour, or legumes such as beans or lentils, are sometimes used in place 
      of wheat flour to yield a different taste and texture, or as a gluten-free alternative. Pasta is a staple food 
      of Italian cuisine. Pastas are divided into two broad categories: dried (pasta secca) and fresh (pasta fresca). 
      Most dried pasta is produced commercially via an extrusion process, although it can be produced at home. Fresh 
      pasta is traditionally produced by hand, sometimes with the aid of simple machines. Fresh pastas available in 
      grocery stores are produced commercially by large-scale machines.
      Both dried and fresh pastas come in a number of shapes and varieties, with 310 specific forms known by over 1300 
      documented names. In Italy, the names of specific pasta shapes or types often vary by locale. For example, the 
      pasta form cavatelli is known by 28 different names depending upon the town and region. Common forms of pasta 
      include long and short shapes, tubes, flat shapes or sheets, miniature shapes for soup, those meant to be filled 
      or stuffed, and specialty or decorative shapes.`,
    "images": [
      "https://www.indianhealthyrecipes.com/wp-content/uploads/2019/05/masala-pasta.jpg",
      "https://hips.hearstapps.com/delish/assets/17/36/1504715566-delish-fettuccine-alfredo.jpg",
      "https://www.jessicagavin.com/wp-content/uploads/2020/07/types-of-pasta-7-1200.jpg",
      "https://imagesvc.meredithcorp.io/v3/mm/image?q=85&c=sc&poi=face&w=4515&h=2258&url=https%3A%2F%2Fimages.media-allrecipes.com%2Fuserphotos%2F3512373.jpg"
    ],
    "video": "https://www.youtube.com/watch?v=HdSLKZ6LN94",
    "Estimated Cost": 4.00,
    "Glycemic Load": 10.0,
    "Glycemic Index": 1.0,
    "Nutrition Information": {
      "Calories": 10,
      "Fat": 5,
      "Carbs": 20,
      "Protein": 1,
      "Sugar": 1,
      "Cholesterol": 1,
      "Sodium": 10,
      "Vitamin C": 20,
      "Fiber": 1
    },
    "Macronutrients Percentages": 0.50,
    "Percentage of Protein": 0.40,
    "Percentage of Carbs": 0.05,
    "Percentage Fat": 0.05,
    "Cuisines": [
      "Italian"
    ],
    "Recipes": [
      "Alfredo"
    ]
  },
  {
    "name": "White Fish",
    "link_left": "Cajun Spiced Fish Tacos",
    "link_right": "Cajun",
    "image": "https://caseagrant.ucsd.edu/sites/default/files/oceanwhitefish_iNaturalist.jpg",
    "text": `It is endemic to the Western Cape Province of South Africa, where it occurs in the 
      Breede River's Brandvlei Dam and Sanddrift Dam, as well as the Hex River. Formerly, it was 
      also found in and around the Berg River, but it seems to have disappeared from there. 
      Populations have also been transplanted to farm water catchments, but it is not well known 
      if these have thrived or failed.
      It inhabits the deeper stretches of rivers with rocky shores or riparian trees. While young 
      fish are common in riffles, adults are not rheophilic and will thrive in slow-moving water. 
      Young fish are carnivores, eating zooplankton and small aquatic invertebrates. Adults have 
      more omnivorous diets that consist of benthic invertebrates and algae. The breeding season is 
      in late spring (about October) when the water has warmed to above 20 °C (68 °F). Schools of 
      adults form to migrate to riffles with over one meter/yard deep water and spawn in the late 
      hours of morning. A good-sized adult female produces about 100,000 eggs. In dams, it will use 
      rocky and gravelly substrate in the shallows as a spawning place. The species is long-lived.
    `,
    "images": [
      "https://farmerstoyou.com/spree/images/12263/product/fresh_caught_whitefish_fillet_1462.jpg?1618238368",
      "https://cdn.apartmenttherapy.info/image/fetch/f_auto,q_auto:eco,c_fill,g_auto,w_1500,ar_3:2/https%3A%2F%2Fstorage.googleapis.com%2Fgen-atmedia%2F3%2F2015%2F04%2F4074ec5d7bf0d827972efad48e60cdea31ae7a6e.jpeg",
      "https://www.gannett-cdn.com/-mm-/84d45f0164161b2e163167ac256deb300659623e/c=0-186-3327-2066/local/-/media/2016/12/10/WIGroup/Milwaukee/636170110442468402-MJS-whitefish.jpg"
    ],
    "video": "https://www.youtube.com/watch?v=D2rlOzt0Id8",
    "Estimated Cost": 5.00,
    "Glycemic Load": 0.0,
    "Glycemic Index": 0.0,
    "Nutrition Information": {
      "Calories": 82,
      "Fat": 1,
      "Carbs": 0,
      "Protein": 18,
      "Sugar": 0,
      "Cholesterol": 43,
      "Sodium": 54,
      "Vitamin C": 36,
      "Fiber": 1
    },
    "Macronutrients Percentages": 0.80,
    "Percentage of Protein": 0.71,
    "Percentage of Carbs": 0.00,
    "Percentage Fat": 0.05,
    "Cuisines": [
      "Cajun"
    ],
    "Recipes": [
      "Cajun spiced fish tacos"
    ]
  },
  {
    "name": "Lamb Loin Chops",
    "link_left": "Irish Stew",
    "link_right": "Irish",
    "image": "https://houseofnasheats.com/wp-content/uploads/2020/07/Pan-Seared-Lamb-Chops-8.jpg",
    "text": `
    Lamb loin chops look pretty much like small T-bone steaks, with the lamb loin on one side and 
    the small fillet on the other, just like what you would see on a regular T-bone steak.   They 
    are very tender.  Loin chops are cut from the waist of the lamb, and they don't have the rib 
    bone.
    Lamb chops are generally rib chops, which are cut from the ribs of the lamb, and they often 
    have the rib bone.  Both kinds of chops are great pan-seared, sauteed or prepared on the grill.
    `,
    "images": [
      "https://gimmedelicious.com/wp-content/uploads/2020/05/Pan-Seared-Lamb-Chops-4-500x375.jpg",
      "https://cdn.debragga.com/pub/media/mageplaza/blog/post/d/e/det-lamb-chops.jpg",
      "https://www.craftycookingbyanna.com/wp-content/uploads/2016/10/lamb-loin-mashed-pin-720x720.jpg",
      "https://imagesvc.meredithcorp.io/v3/mm/image?url=https%3A%2F%2Fimages.media-allrecipes.com%2Fuserphotos%2F8740298.jpg&q=85"
    ],
    "video": "https://www.youtube.com/watch?v=DDLczoeOP-M",
    "Estimated Cost": 15.99,
    "Glycemic Load": 8.0,
    "Glycemic Index": 1.0,
    "Nutrition Information": {
      "Calories": 310,
      "Fat": 12,
      "Carbs": 0,
      "Protein": 16,
      "Sugar": 0,
      "Cholesterol": 74,
      "Sodium": 56,
      "Vitamin C": 0,
      "Fiber": 0
    },
    "Macronutrients Percentages": 0.50,
    "Percentage of Protein": 0.40,
    "Percentage of Carbs": 0.05,
    "Percentage Fat": 0.05,
    "Cuisines": [
      "Irish"
    ],
    "Recipes": [
      "Irish stew "
    ]
  }
]

export const cuisines = [
  {
    "name": "Italian",
    "link_left": "Alfredo",
    "link_right": "Pasta",
    "popular_cities": [
      "Rome", "Milan"
    ],
    "image": "https://www.fodors.com/wp-content/uploads/2018/10/HERO_UltimateRome_Hero_shutterstock789412159.jpg",
    "text": `Italian cuisine is a Mediterranean cuisine consisting of the ingredients, recipes and cooking techniques developed across the Italian Peninsula since antiquity, 
      and later spread around the world together with waves of Italian diaspora. Significant changes occurred with the colonization of the Americas and the introduction of 
      potatoes, tomatoes, capsicums, maize and sugar beet - the latter introduced in quantity in the 18th century. Italian cuisine is known for its regional diversity, 
      especially between the north and the south of Italy. It offers an abundance of taste, and is one of the most popular and copied in the world. It influenced several 
      cuisines around the world, chiefly that of the United States.`,
    "images": [
      "https://cloudinary-assets.dostuffmedia.com/res/dostuff-media/image/upload/page-image-9555-bf34dc26-ddfe-48a7-8a42-b460c1bd130d/1547845526.jpg",
      "https://cdn.vox-cdn.com/thumbor/-ZaGaBC9JuCXShitdJfmHYrrVH4=/0x0:960x720/1200x900/filters:focal(370x301:522x453)/cdn.vox-cdn.com/uploads/chorus_image/image/60324123/24232387_888214381335811_7900548467483469351_n.0.jpg",
      "https://c.ndtvimg.com/2020-04/dih4ifhg_pasta_625x300_22_April_20.jpg"
    ],
    "video": "https://www.youtube.com/watch?v=N3lYGgAZWyc",
    "location": {
      "longitude": 41.9028,
      "latitude": 12.4964
    },
    "average_population": [
      2.873, 1.352
    ],
    "population_density": [
      793.0, 2.0, 0.72
    ],
    "type_of_climate": [
      "Mediterranean climate"
    ],
    "other_cuisine_around_the_region": [
      "German"
    ],
    "ingredients": [
      "Pasta", "Cream", "Chicken"
    ],
    "recipes": [
      "Alfredo"
    ]
  },
  {
    "name": "Cajun",
    "link_left": "Cajun Spiced Fish Tacos",
    "link_right": "White Fish",
    "popular_cities": [
      " New Orleans "
    ],
    "image": "https://s7.bluegreenvacations.com/is/image/BGV/louisiana-new-orleans-jackson-quare?$bg2-gallery$&crop=3,256,2492,1401&anchor=1249,956",
    "text": `Cajun cuisine is sometimes referred to as a 'rustic cuisine', meaning that it is based on locally available ingredients and that preparation is relatively simple.
      An authentic Cajun meal is usually a three-pot affair, with one pot dedicated to the main dish, one dedicated to steamed rice, specially made sausages, or some seafood 
      dish, and the third containing whatever vegetable is plentiful or available. Crawfish, shrimp, and andouille sausage are staple meats used in a variety of dishes.
      The aromatic vegetables green bell pepper (piment doux), onion, and celery are called "the trinity" by Cajun chefs in Cajun and Louisiana Creole cuisines. Roughly diced 
      and combined in cooking, the method is similar to the use of the mirepoix in traditional French cuisine which blends roughly diced carrot, onion, and celery. 
      Characteristic aromatics for the Creole version may also include parsley, bay leaf, green onions, dried cayenne pepper, and dried black pepper.`,
    "images": [
      "https://3oy5j122txax31zs2k2pgs3p-wpengine.netdna-ssl.com/wp-content/uploads/2019/04/garlic-cajun-01.jpg",
      "https://3oy5j122txax31zs2k2pgs3p-wpengine.netdna-ssl.com/wp-content/uploads/2019/04/garlic-cajun-02.jpg",
      "https://www.wideopeneats.com/wp-content/uploads/2019/07/cajun-960x540.png"
    ],
    "video": "https://www.youtube.com/watch?v=nORg_aXMsmA&ab_channel=BabishCulinaryUniverse",
    "location": {
      "longitude": -92.329102,
      "latitude": 30.391830
    },
    "average_population": [
      4.649
    ],
    "population_density": [
      106.9
    ],
    "type_of_climate": [
      "Tropical storms"
    ],
    "other_cuisine_around_the_region": [
      "American Food"
    ],
    "ingredients": [
      "white fish", "Garlic", "avocado"
    ],
    "recipes": [
      "Cajun spiced fish tacos"
    ]
  },
  {
    "name": "Irish",
    "link_left": "Irish Stew",
    "link_right": "Lamb Loin Chops",
    "popular_cities": [
      "Dublin"
    ],
    "image": "https://a.cdn-hotels.com/gdcs/production24/d796/6921fa10-f585-11e8-b7e0-0242ac110002.jpg",
    "text": `Irish cuisine is the style of cooking that originated from the island of Ireland or was developed by Irish people. It has evolved from centuries of social and 
      political change and the mixing of the different cultures in Ireland, predominantly from nearby Britain and other European regions. The cuisine is founded upon the crops 
      and animals farmed in its temperate climate and the abundance of fresh fish and seafood from the surrounding waters of the Atlantic Ocean. Chowder, for example, is popular 
      around the coasts.`,
    "images": [
      "https://images.immediate.co.uk/production/volatile/sites/30/2020/08/irish-stew-fe65840.jpg",
      "https://cdn.britannica.com/36/1736-050-3AB11366/Cliffs-of-Moher-coast-County-Clare-Ireland.jpg",
      "https://purewows3.imgix.net/images/articles/2021_02/traditional-irish-food_full-breakfast-irish-fry-up.jpg?auto=format,compress&cs=strip",
      "https://i.natgeofe.com/n/fdd45934-5e1a-4b61-9981-8528dc2a3dc0/cliff-moher-ireland.jpg"
    ],
    "video": "https://www.youtube.com/watch?v=MZO4B1RZU5g",
    "location": {
      "longitude": 6.2603,
      "latitude": 53.3498
    },
    "average_population": [
      0.544
    ],
    "population_density": [
      0.118
    ],
    "type_of_climate": [
      "oceanic, cool and humid"
    ],
    "other_cuisine_around_the_region": [
      "Italian", "German"
    ],
    "ingredients": [
      "whole wheat", "lamb loin chops", "olive oil"
    ],
    "recipes": [
      "Irish stew"
    ]
  }
]
