import { Cuisine, Ingredient, Recipe } from "./models";

export const getAllInstances = (modelName, callback) => {
  console.log('Fetching instances for', modelName);

  fetch(`https://api.broaderhorizons.me/api/${modelName}`).then(res => res.json()).then(data => {
    callback(data.map((item) => {
      if (modelName === 'recipes') {
        return new Recipe(item);
      } else if (modelName === 'ingredients') {
        return new Ingredient(item);
      } else {
        return new Cuisine(item);
      }
    }));
    return data;
  }).catch((err) => console.log(err));
};

export const getInstanceById = (modelName, id) => {
  return fetch(`https://api.broaderhorizons.me/api/${modelName}/id=${id}`).then(res => res.json()).then(data => {
    if (modelName === 'recipe') {
      return new Recipe(data);
    } else if (modelName === 'ingredient') {
      return new Ingredient(data);
    } else {
      return new Cuisine(data);
    }
  }).catch((err) => console.log(err));
};

export const getInstancesWithQueryUrl = (modelName, url, callback) => {
  if (modelName === 'everything') {
    return null;
  }
  return fetch(url).then(res => res.json()).then(data => {
    callback(data["result"].map((item) => {
      if (modelName === 'recipes') {
        return new Recipe(item);
      } else if (modelName === 'ingredients') {
        return new Ingredient(item);
      } else {
        return new Cuisine(item);
      }
    }), data['count']);
    return data;
  }).catch((err) => console.log(err));
};

export const getInstancesWithSearch = (modelName, url, callback) => {
  return fetch(url).then(res => res.json()).then(data => {
    callback(modelName,
      data["result"].map((item) => {
        if (modelName === 'recipes') {
          return new Recipe(item);
        } else if (modelName === 'ingredients') {
          return new Ingredient(item);
        } else {
          return new Cuisine(item);
        }
      }));
    return data;
  }).catch((err) => console.log(err));
};
