// models.js
// Defines the different models and utility functions

export function sortModelObjects(indexOfValue, objects) {
  console.log('Sorting on index', indexOfValue);
  const sortedArray = [...objects].sort((a, b) => {
    const aVal = a.getValueAtIndex(indexOfValue);
    const bVal = b.getValueAtIndex(indexOfValue);
    console.log('aval:', aVal, 'bVal:', bVal);
    if (aVal < bVal) {
      return -1;
    }

    if (aVal > bVal) {
      return 1;
    }

    return 0;
  });
  console.log(sortedArray);
  return sortedArray;
}

/*
calories: 379.6018725490234
common_allergens: "{\"Low Potassium\",Kidney-Friendly,Vegetarian,Pescatarian,Peanut-Free,Tree-Nut-Free,Soy-Free,Fish-Free,Shellfish-Free,Pork-Free,Red-Meat-Free,Crustacean-Free,Celery-Free,Mustard-Free,Sesame-Free,Lupine-Free,Mollusk-Free,Alcohol-Free,Kosher}"
dairy_free: false
description: null
glycemic_index: null
id: 1
image_url: "https://www.edamam.com/web-img/b18/b18ba9b4df54ab3488076c76a8a8a126.jpg"
ingredients: "butter,sugar,eggs,flour,lemon,lemon lime soda,confectioners' sugar,unsalted butter,milk,vanilla,shredded coconut,Lollipops"
name: "7 Up Cake"
nutrition_info: "{\"ENERC_KCAL\": {\"label\": \"Energy\", \"quantity\": 12906.463666666796, \"unit\": \"kcal\"}, \"FAT\": {\"label\": \"Fat\", \"quantity\": 651.9264400000013, \"unit\": \"g\"}, \"FASAT\": {\"label\": \"Saturated\", \"quantity\": 446.23051500000025, \"unit\": \"g\"}, \"FATRN\": {\"label\": \"Trans\", \"quantity\": 18.63518, \"unit\": \"g\"}, \"FAMS\": {\"label\": \"Monounsaturated\", \"quantity\": 135.16201166666676, \"unit\": \"g\"}, \"FAPU\": {\"label\": \"Polyunsaturated\", \"quantity\": 24.927177666667067, \"unit\": \"g\"}, \"CHOCDF\": {\"label\": \"Carbs\", \"quantity\": 1741.617566666708, \"unit\": \"g\"}, \"FIBTG\": {\"label\": \"Fiber\", \"quantity\": 51.93733333334588, \"unit\": \"g\"}, \"SUGAR\": {\"label\": \"Sugars\", \"quantity\": 1381.1882333333447, \"unit\": \"g\"}, \"SUGAR.added\": {\"label\": \"Sugars, added\", \"quantity\": 1304.227, \"unit\": \"g\"}, \"PROCNT\": {\"label\": \"Protein\", \"quantity\": 92.3124566666716, \"unit\": \"g\"}, \"CHOLE\": {\"label\": \"Cholesterol\", \"quantity\": 2028.9, \"unit\": \"mg\"}, \"NA\": {\"label\": \"Sodium\", \"quantity\": 604.0776666666756, \"unit\": \"mg\"}, \"CA\": {\"label\": \"Calcium\", \"quantity\": 544.690666666783, \"unit\": \"mg\"}, \"MG\": {\"label\": \"Magnesium\", \"quantity\": 369.4396666667026, \"unit\": \"mg\"}, \"K\": {\"label\": \"Potassium\", \"quantity\": 2437.1670000006184, \"unit\": \"mg\"}, \"FE\": {\"label\": \"Iron\", \"quantity\": 18.24353000000269, \"unit\": \"mg\"}, \"ZN\": {\"label\": \"Zinc\", \"quantity\": 11.849240000000268, \"unit\": \"mg\"}, \"P\": {\"label\": \"Phosphorus\", \"quantity\": 1597.2273333334049, \"unit\": \"mg\"}, \"VITA_RAE\": {\"label\": \"Vitamin A\", \"quantity\": 4271.648333333338, \"unit\": \"\\u00b5g\"}, \"VITC\": {\"label\": \"Vitamin C\", \"quantity\": 8.506666666904128, \"unit\": \"mg\"}, \"THIA\": {\"label\": \"Thiamin (B1)\", \"quantity\": 0.7791973333335125, \"unit\": \"mg\"}, \"RIBF\": {\"label\": \"Riboflavin (B2)\", \"quantity\": 2.0439066666667562, \"unit\": \"mg\"}, \"NIA\": {\"label\": \"Niacin (B3)\", \"quantity\": 6.860768333333781, \"unit\": \"mg\"}, \"VITB6A\": {\"label\": \"Vitamin B6\", \"quantity\": 1.366640666667025, \"unit\": \"mg\"}, \"FOLDFE\": {\"label\": \"Folate equivalent (total)\", \"quantity\": 245.55166666671596, \"unit\": \"\\u00b5g\"}, \"FOLFD\": {\"label\": \"Folate (food)\", \"quantity\": 245.55166666671596, \"unit\": \"\\u00b5g\"}, \"FOLAC\": {\"label\": \"Folic acid\", \"quantity\": 0.0, \"unit\": \"\\u00b5g\"}, \"VITB12\": {\"label\": \"Vitamin B12\", \"quantity\": 3.4246999999999996, \"unit\": \"\\u00b5g\"}, \"VITD\": {\"label\": \"Vitamin D\", \"quantity\": 14.376, \"unit\": \"\\u00b5g\"}, \"TOCPHA\": {\"label\": \"Vitamin E\", \"quantity\": 16.834350000000672, \"unit\": \"mg\"}, \"VITK1\": {\"label\": \"Vitamin K\", \"quantity\": 42.521, \"unit\": \"\\u00b5g\"}, \"WATER\": {\"label\": \"Water\", \"quantity\": 925.7258200003987, \"unit\": \"g\"}}"
prep_time: 0
servings: 34
vegan: false
vegetarian: true
video_id: "nNiBvG0FVS8"
*/

// Model 1: Recipes
export class Recipe {
  // constructor(string, string, int, int, float)
  constructor(raw_data) {
    this.data = raw_data;
    this.data.image = raw_data.image_url;
    this.data.video = raw_data.video_id;

    let text = 'Calories: ' + raw_data.calories;
    text += '\n\nHealth Labels: ' + raw_data.common_allergens.split(',').slice(1, -1).join(', ');
    text += '\n\nIngredients: ' + raw_data.ingredients.split(',').join(', ');
    text += '\n\nPrep Time: ' + raw_data.prep_time;
    text += '\n\nServings: ' + raw_data.servings;
    text += '\n\nVegan: ' + raw_data.vegan;
    text += '\n\nVegetarian: ' + raw_data.vegetarian;
    text += '\n\nDairy Free: ' + raw_data.dairy_free;
    text += '\n\nGlycemic Index: ' + raw_data.glycemic_index;
    text += '\n\nDescription: ' + raw_data.description;

    this.data.text = text;
  }

  getModelPageKeys() {
    return ['name', 'prep_time', 'servings', 'dairy_free', 'vegetarian']
  }

  getColumnNames() {
    return ['Name', 'Prep Time (min)', 'Servings', 'Dairy Free', 'Vegetarian'];
  }

  getValueAtIndex(index) {
    return this.data[this.getModelPageKeys()[index]];
  }

  getSortQuery(name) {
    const mapping = {
      'Name': 'name', 
      'Prep Time (min)': 'prep', 
      'Servings': 'servings'
    }

    return mapping[name];
  }

  isSortable(name) {
    const mapping = {
      'Name': true, 
      'Prep Time (min)': true, 
      'Servings': true, 
      'Dairy Free': false, 
      'Vegetarian': false
    }

    return mapping[name];
  }
}

/*
estimated_cost: 2.36
estimated_cost_unit: "US Cents"
grocery_aisle: "Nuts;Savory Snacks"
id: 20
macronutrients: "{\"percentProtein\": 13.52, \"percentFat\": 74.68, \"percentCarbs\": 11.8}"
name: "blanched almonds"
nutrition_info: "[{\"title\": \"Vitamin B3\", \"name\": \"Vitamin B3\", \"amount\": 0.04, \"unit\": \"mg\"}, {\"title\": \"Folic Acid\", \"name\": \"Folic Acid\", \"amount\": 0.0, \"unit\": \"\\u00b5g\"}, {\"title\": \"Iron\", \"name\": \"Iron\", \"amount\": 0.03, \"unit\": \"mg\"}, {\"title\": \"Carbohydrates\", \"name\": \"Carbohydrates\", \"amount\": 0.19, \"unit\": \"g\"}, {\"title\": \"Vitamin C\", \"name\": \"Vitamin C\", \"amount\": 0.0, \"unit\": \"mg\"}, {\"title\": \"Vitamin A\", \"name\": \"Vitamin A\", \"amount\": 0.07, \"unit\": \"IU\"}, {\"title\": \"Vitamin B5\", \"name\": \"Vitamin B5\", \"amount\": 0.0, \"unit\": \"mg\"}, {\"title\": \"Mono Unsaturated Fat\", \"name\": \"Mono Unsaturated Fat\", \"amount\": 0.33, \"unit\": \"g\"}, {\"title\": \"Caffeine\", \"name\": \"Caffeine\", \"amount\": 0.0, \"unit\": \"mg\"}, {\"title\": \"Phosphorus\", \"name\": \"Phosphorus\", \"amount\": 4.81, \"unit\": \"mg\"}, {\"title\": \"Sodium\", \"name\": \"Sodium\", \"amount\": 0.19, \"unit\": \"mg\"}, {\"title\": \"Vitamin D\", \"name\": \"Vitamin D\", \"amount\": 0.0, \"unit\": \"\\u00b5g\"}, {\"title\": \"Protein\", \"name\": \"Protein\", \"amount\": 0.21, \"unit\": \"g\"}, {\"title\": \"Vitamin B6\", \"name\": \"Vitamin B6\", \"amount\": 0.0, \"unit\": \"mg\"}, {\"title\": \"Poly Unsaturated Fat\", \"name\": \"Poly Unsaturated Fat\", \"amount\": 0.12, \"unit\": \"g\"}, {\"title\": \"Selenium\", \"name\": \"Selenium\", \"amount\": 0.03, \"unit\": \"\\u00b5g\"}, {\"title\": \"Choline\", \"name\": \"Choline\", \"amount\": 0.52, \"unit\": \"mg\"}, {\"title\": \"Sugar\", \"name\": \"Sugar\", \"amount\": 0.05, \"unit\": \"g\"}, {\"title\": \"Fiber\", \"name\": \"Fiber\", \"amount\": 0.1, \"unit\": \"g\"}, {\"title\": \"Vitamin B12\", \"name\": \"Vitamin B12\", \"amount\": 0.0, \"unit\": \"\\u00b5g\"}, {\"title\": \"Vitamin B1\", \"name\": \"Vitamin B1\", \"amount\": 0.0, \"unit\": \"mg\"}, {\"title\": \"Net Carbohydrates\", \"name\": \"Net Carbohydrates\", \"amount\": 0.09, \"unit\": \"g\"}, {\"title\": \"Calories\", \"name\": \"Calories\", \"amount\": 5.9, \"unit\": \"kcal\"}, {\"title\": \"Calcium\", \"name\": \"Calcium\", \"amount\": 2.36, \"unit\": \"mg\"}, {\"title\": \"Manganese\", \"name\": \"Manganese\", \"amount\": 0.02, \"unit\": \"mg\"}, {\"title\": \"Trans Fat\", \"name\": \"Trans Fat\", \"amount\": 0.0, \"unit\": \"g\"}, {\"title\": \"Copper\", \"name\": \"Copper\", \"amount\": 0.01, \"unit\": \"mg\"}, {\"title\": \"Vitamin B2\", \"name\": \"Vitamin B2\", \"amount\": 0.01, \"unit\": \"mg\"}, {\"title\": \"Saturated Fat\", \"name\": \"Saturated Fat\", \"amount\": 0.04, \"unit\": \"g\"}, {\"title\": \"Vitamin K\", \"name\": \"Vitamin K\", \"amount\": 0.0, \"unit\": \"\\u00b5g\"}, {\"title\": \"Alcohol\", \"name\": \"Alcohol\", \"amount\": 0.0, \"unit\": \"g\"}, {\"title\": \"Zinc\", \"name\": \"Zinc\", \"amount\": 0.03, \"unit\": \"mg\"}, {\"title\": \"Folate\", \"name\": \"Folate\", \"amount\": 0.49, \"unit\": \"\\u00b5g\"}, {\"title\": \"Fat\", \"name\": \"Fat\", \"amount\": 0.53, \"unit\": \"g\"}, {\"title\": \"Potassium\", \"name\": \"Potassium\", \"amount\": 6.59, \"unit\": \"mg\"}, {\"title\": \"Magnesium\", \"name\": \"Magnesium\", \"amount\": 2.68, \"unit\": \"mg\"}, {\"title\": \"Cholesterol\", \"name\": \"Cholesterol\", \"amount\": 0.0, \"unit\": \"mg\"}, {\"title\": \"Vitamin E\", \"name\": \"Vitamin E\", \"amount\": 0.24, \"unit\": \"mg\"}]"
percentProtein: 13.52
weight_per_serving: 1
weight_per_serving_unit: "g"
*/

// Model 2: Ingredients
export class Ingredient {
  // constructor(string, string, int, int, float)
  constructor(raw_data) {
    this.data = raw_data;
    this.data.macronutrients = JSON.parse(raw_data.macronutrients);
    this.data.percentProtein = this.data.macronutrients.percentProtein;
    this.data.image = this.data.image_url ? this.data.image_url.replace('100x100', '500x500') : null;

    let text = 'Estimated Cost: ' + raw_data.estimated_cost + ' ' + raw_data.estimated_cost_unit;
    text += '\n\nGrocery Aisle: ' + raw_data.grocery_aisle;
    text += '\n\nWeight Per Serving: ' + raw_data.weight_per_serving + ' ' + raw_data.weight_per_serving_unit + '\n';

    let nutritionJSON = JSON.parse(raw_data.nutrition_info);

    for (var key in nutritionJSON) {
        if (nutritionJSON.hasOwnProperty(key)) {
          let desc = nutritionJSON[key];
          text += '\n' + desc.title + ": " + desc.amount + desc.unit;
        }
    }

    this.data.text = text;
  }

  getModelPageKeys() {
    return ['name', 'percentProtein', 'grocery_aisle', 'weight_per_serving', 'estimated_cost']
  }

  getColumnNames() {
    return ['Name', 'Protein (%)', 'Grocery Aisle', 'Weight Per Serving (g)', 'Est. Cost ($)'];
  }

  getValueAtIndex(index) {
    return this.data[this.getModelPageKeys()[index]];
  }

  getSortQuery(name) {
    const mapping = {
      'Name': 'name', 
      'Grocery Aisle': 'aisle', 
      'Weight Per Serving (g)': 'wps'
    }

    return mapping[name];
  }

  isSortable(name) {
    const mapping = {
      'Name': true, 
      'Protein (%)': false, 
      'Grocery Aisle': true, 
      'Weight Per Serving (g)': true, 
      'Est. Cost ($)': true
    }

    return mapping[name];
  }
}

/**
average_rainfall: 976.4400583303764
average_temperature: 23.50968556123725
country: "Afghanistan"
description: "The Cuisine from Afghanistan is largely based upon the nation's chief crops, such as wheat, maize, barley and rice. Accompanying these staples are native fruits and vegetables as well as dairy products such as milk, yogurt and whey. Kabuli Palaw is the national dish of Afghanistan. The nation's culinary specialties reflect its ethnic and geographic diversity. Afghanistan is known for its high quality pomegranates, grapes and sweet football-shaped melons.\r\n\r\nChaWal = Rice\r\nDhaniya = Coriander\r\nHaldi = Tumeric\r\nLaal Mirch = Red chilli\r\nLehsan Adrak = Ginger garlic\r\nMoong Daal (Chilky Vali) = Green gram split peas\r\nNamak = Salt\r\nPalak = Spinch\r\nPani = Water\r\nPyaaz = Onion\r\nQeema = Mince\r\nZeera = Cumin"
flag_url: "https://upload.wikimedia.org/wikipedia/commons/thumb/5/5c/Flag_of_the_Taliban.svg/320px-Flag_of_the_Taliban.svg.png"
id: 1
koppen_classification: "Csa"
languages: "Dari,Pashto,Turkmen"
name: "Afghan"
population: 2837743
region: "Asia"
subregion: "Southern Asia"
 */
// Model 3: Cuisines
export class Cuisine {
  // constructor(string, string, int, int, float)
  constructor(raw_data) {
    this.data = raw_data;
    this.data.image = raw_data.flag_url;

    let text = 'Average Rainfall: ' + raw_data.average_rainfall;
    text += '\n\nAverage Temperature: ' + raw_data.average_temperature;
    text += '\n\nKoppen Classification: ' + raw_data.koppen_classification;
    text += '\n\nCountry: ' + raw_data.country;
    text += '\n\nRegion: ' + raw_data.region;
    text += '\n\nSubregion: ' + raw_data.subregion;
    text += '\n\nPopulation: ' + raw_data.population;
    text += '\n\nLanguages: ' + raw_data.languages.split(',').join(', ');
    text += '\n\nDescription: ' + raw_data.description;

    this.data.text = text;
  }

  getModelPageKeys() {
    return ['name', 'country', 'region', 'average_rainfall', 'average_temperature']
  }

  getColumnNames() {
    return ['Name', 'Country', 'Region', 'Avg. Rainfall', 'Avg. Temperature'];
  }

  getValueAtIndex(index) {
    return this.data[this.getModelPageKeys()[index]];
  }

  getSortQuery(name) {
    const mapping = {
      'Name': 'name', 
      'Country': 'country', 
      'Region': 'region', 
      'Avg. Rainfall': 'pr', 
      'Avg. Temperature': 'tas'
    }

    return mapping[name];
  }

  isSortable(name) {
    const mapping = {
      'Name': true, 
      'Country': true, 
      'Region': true, 
      'Avg. Rainfall': true, 
      'Avg. Temperature': true
    }

    return mapping[name];
  }
}
