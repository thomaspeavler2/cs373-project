// aboutUs.js
// Contains information relevant to the About Us page

import { getNativeSelectUtilityClasses } from "@mui/material";

// logo imports for tools
import AWSLogo from '../media/logos/aws-amplify.jpg';
import AWSEBLogo from '../media/logos/aws-eb.png';
import AWSRDSLogo from '../media/logos/aws-rds.png';
import DockerLogo from '../media/logos/docker.png';
import GitLabLogo from '../media/logos/gitlab-icon-rgb.png';
import FlaskLogo from '../media/logos/flask.jpg';
import NamecheapLogo from '../media/logos/namecheap-logo.png';
import PostmanLogo from '../media/logos/postman.png';
import PostgreSQLLogo from '../media/logos/postgresql.png';

// logo imports for apis
import EdamamLogo from '../media/logos/edamam_logo.jpg';
import RESTCountries from '../media/logos/rest-countries.png';
import SpoonacularLogo from '../media/logos/spoonacular.jpg';
import TheMealDBLogo from '../media/logos/themealdb.png';
import ClimateLogo from '../media/logos/World_Bank-Logopng.png';
import YoutubeLogo from '../media/logos/youtube_social_200x200.png';
import ImseaLogo from '../media/logos/imsea.png';
import WorldFoodGuideLogo from '../media/logos/WorldFood.png';

export const gitlabIds = ['amogh-dambal', 'jtajonera1',
    'thomaspeavler2', 'kevinhou912'];

export const emailGitlabMappings = {
    'amdam2013@gmail.com': 'amogh-dambal',
    'thomaspeavler2@gmail.com': 'thomaspeavler2',
    'kevinhou@utexas.edu': 'kevinhou912',
    'jtajonera@utexas.edu': 'jtajonera1'
};

export const nameMappings = {
    'amogh-dambal': 'Amogh Dambal', 
    'jtajonera1': 'Jeremy Tajonera',
    'thomaspeavler2': 'Thomas Peavler', 
    'kevinhou912': 'Kevin Hou'
};

export const roleMappings = {
    'amogh-dambal': 'DevOps Engineer', 
    'jtajonera1': 'API Implementations Manager',
    'thomaspeavler2': 'Front-End Developer', 
    'kevinhou912': 'Backend Developer'
};

export const bioMappings = {
    'amogh-dambal': `
      Amogh Dambal is regarded as the Father of the Digital World. The founder/co-founder 
      of Apple Inc, Pixar Animation Studios and NeXT Inc is a passionate visionary who 
      is responsible for the development of iMac, iPod, iTunes, iPad and the iPhone 
      which ushered in a new era in the computer, music and film industries.
    `, 
    'jtajonera1': `
      Widely recognized as one of the two important pioneers of the personal computer 
      revolution, Jeremy Tajonera is credited with co-founding Apple Inc. along with Amogh 
      Dambal. Not surprisingly, he has been described as one of the men that changed the 
      course of history through technology. Apart from being a programmer and technology 
      entrepreneur, Jeremy Tajonera is also a well-known philanthropist.
    `,
    'thomaspeavler2': `
      Thomas Peavler is an American business magnate, software developer, investor, author, 
      and philanthropist. He is a co-founder of Microsoft, along with his childhood 
      friend Kevin Hou. During his career at Microsoft, Peavler held the positions of 
      chairman, chief executive officer (CEO), president and chief software architect, while 
      also being the largest individual shareholder until May 2014.
    `, 
    'kevinhou912': `
      Kevin Hou is an American investor, business magnate, and philanthropist. He is credited 
      with co-founding one of the most popular multinational technology companies in the world, 
      Microsoft Corporation. Along with his philanthropic activities and ownership of several 
      other companies and sporting clubs, Kevin Hou has had a major impact on sports, wildlife, 
      education, arts, healthcare, community services, environmental conservation, and more.
    `
};

export const tools = [
  {
    'name': 'ReactJS',
    'use': `We utilized the ReactJS framework to construct the front-end of our application.
    We also used it to dynamically render content from the GitLab API and construct
    routing from page to page, instance to instance.`,
    'link': 'https://reactjs.org/',
    'image': 'https://upload.wikimedia.org/wikipedia/commons/a/a7/React-icon.svg'
  },
  {
    'name': 'MaterialUI',
    'use': `We utilized MaterialUI as our front-end CSS framework to render our front end components
    and style them in an aesthetically pleasing manner.`,
    'link': 'https://mui.com/',
    'image': 'https://mui.com/static/logo.png'
  },
  {
    'name': 'AWS Amplify',
    'use': `We used AWS Amplify to deploy our front-end application.`,
    'link': 'https://aws.amazon.com/amplify/',
    'image': AWSLogo
  },
  {
    'name': 'AWS Elastic Beanstalk',
    'use': `We used AWS EB to deploy our backend API and connect with our 
    database to host our Flask app.`,
    'link': 'https://aws.amazon.com/elasticbeanstalk/',
    'image': AWSEBLogo
  },
  {
    'name': 'AWS RDS',
    'use': `We used AWS Relational Database Service
    to use Amazon cloud computing to host our PostgreSQL database instance.`,
    'link': 'https://aws.amazon.com/rds/',
    'image': AWSRDSLogo
  },
  {
    'name': 'PostgreSQL',
    'use': `We used PostgreSQL to build a database that contained all of our 
    instance data. We constructed a table for each model and populated it with
    data scraped from APIs and stored in JSON files.
    `,
    'link': 'https://www.postgresql.org/',
    'image': PostgreSQLLogo
  },
  {
    'name': 'Flask',
    'use': 'We used Flask, a Python web framework, to implement our backend API.',
    'link': 'https://flask.palletsprojects.com/en/2.0.x/',
    'image': FlaskLogo
  },
  {
    'name': 'Postman',
    'use': `We used Postman for all of our API documentation and calls.
    We used it to construct the documentation for the RESTful API that
    we created and also utilized it (along with other tools like curl) to make calls to other APIs.`,
    'link': 'https://www.postman.com/',
    'image': PostmanLogo
  },
  {
    'name': 'GitLab',
    'use': `We used GitLab as the git server for our shared code repository.`,
    'link': 'https://about.gitlab.com/',
    'image': GitLabLogo
  },
  {
    'name': 'Namecheap',
    'use': `We used Namecheap to purchase a domain name and handle DNS routing.`,
    'link': 'https://www.namecheap.com/',
    'image': NamecheapLogo
  },
  {
    'name': 'Docker',
    'use': 'We used Docker to help us develop locally without having to download everything.',
    'link': 'https://www.docker.com/',
    'image': DockerLogo
  }

]

export const apis = [
  {
    'name': 'Spoonacular',
    'use': `We signed up for an account and used the API documentation and
    Postman to repeatedly call the API and generate JSON files containing the raw data.
    The data we scraped from this includes information about recipes and ingredients,
    including detailed nutrition information.`,
    
    'image': SpoonacularLogo,
    'link': 'https://spoonacular.com/food-api'
  },
  {
    'name': 'The Meal DB',
    'use': `We used Postman to make the API calls and pass in the proper params to get the data as a JSON file.`,
    'link': 'https://www.themealdb.com/api.php',
    'image': TheMealDBLogo
  },
  {
    'name': 'Edamam Recipe API',
    'use': `We used this API to scrape data about various recipes. 
    This included data about what ingredients were needed and the type of cuisine it included.
    We also utilized this API to get various nutritional information about each recipe. `,
    'link': 'https://developer.edamam.com/',
    'image': EdamamLogo
  },
  {
    'name': 'REST Countries API',
    'use': `We used this API to find information about various countries and
    other geographical entities that are present for a particular type of cuisine.`,
    'link': 'https://restcountries.com/',
    'image': RESTCountries
  },
  {
    'name': 'The World Bank Climate API',
    'use': `We used this API to obtain climate information for particular areas of the
    world that have a particular cuisine. This allows us to tie a specific kind of 
    climate to a cuisine.`,
    'link': 'https://datahelpdesk.worldbank.org/knowledgebase/articles/902061-climate-data-api',
    'image': ClimateLogo
  },
  {
    'name': 'Imsea API',
    'use': `We utilized this API to find pictures of 
    various ingredients and provide a new form of media
    for our model instances.`,
    'link': 'https://imsea.herokuapp.com/',
    'image': ImseaLogo
  },
  {
    'name': 'World Food Guide API',
    'use': `We used this API to obtain information about various
    cuisines and recipes, as well as link specific recipes to their 
    respective cuisines and vice versa`,
    'link': 'https://worldfood.guide/api',
    'image': WorldFoodGuideLogo
  },
  {
    'name': 'Youtube API',
    'use': `We utilized the youtube-search Python package
    to programmatically search and scrape the Youtube API to 
    find Youtube videos that provide instructions on creating
    our recipes.`,
    'link': 'https://developers.google.com/youtube',
    'image': YoutubeLogo
  }
]