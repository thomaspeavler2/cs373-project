// gitlabAPI.js
// Handles API calls to GitLab

import { emailGitlabMappings, gitlabIds } from "./info";

// curl "https://gitlab.com/api/v4/projects/29851353/issues_statistics?assignee_username[]=amogh-dambal"

const baseUrl = 'https://gitlab.com/api/v4/projects/29851353/';
const apiCallsIndividual = ['issues_statistics?assignee_username[]=',];
const apiCallsProject = [
  'issues_statistics', 
  'repository/commits?all=true&per_page=100&page=1',
  'repository/commits?all=true&per_page=100&page=2',
  'repository/commits?all=true&per_page=100&page=3',
  'repository/commits?all=true&per_page=100&page=4',
  'repository/commits?all=true&per_page=100&page=5',
];

// Hard coded based off of page for commit api call ^^
const numCommitPages = 5;

function httpsGet(url) {
  return new Promise(function (resolve, reject) {
    var xmlHttp = new XMLHttpRequest();
    xmlHttp.onload = function () {
      if (xmlHttp.readyState === 4 && xmlHttp.status === 200)
        resolve(JSON.parse(xmlHttp.responseText));
      else
        reject({
          status: this.status,
          statusText: xmlHttp.statusText
        });
    }
    xmlHttp.onerror = function () {
      reject({
        status: this.status,
        statusText: xmlHttp.statusText
      });
    }
    xmlHttp.open("GET", url, true);
    xmlHttp.send(null);
  });
}

export async function getGitLabData(callback) {
  // Get all stats
  const callUrls = [];

  // Add the project-level api calls
  apiCallsProject.forEach((call) => (
    callUrls.push(`${baseUrl}${call}`)
  ));

  // Add the individual-level api calls
  apiCallsIndividual.forEach((call) => (
    gitlabIds.forEach((id) => (
      callUrls.push(`${baseUrl}${call}${id}`)
    ))
  ));

  /*
    Individual
      no. of commits
      no. of issues
      no. of unit tests
    Project
      total no. of commits
      total no. of issues
      total no. of unit tests
  */

  const calls = callUrls.map((callUrl) => (httpsGet(callUrl)));

  Promise.all(calls)
    .then((statistics) => {
      // Response: [project issues, project commits, individual issues...]
      console.log(statistics);
      const ret = {};
      ret['totalIssues'] = statistics[0].statistics.counts.all;
      gitlabIds.forEach((id) => (ret[id] = {'totalCommits': 0}));
      gitlabIds.forEach((id, index) => (
        ret[id]['totalIssues'] = statistics[index + apiCallsProject.length].statistics.counts.all
      ));

      // Count commits
      ret['totalCommits'] = 0;
      for (var i = 1; i <= numCommitPages; i++) {
        ret['totalCommits'] += statistics[i].length;
        statistics[i].forEach((commit) => {
          if ('author_email' in commit) {
            if (commit.author_email in emailGitlabMappings) {
              ret[emailGitlabMappings[commit.author_email]]['totalCommits'] += 1;
            }
          }
        });
      }

      // note: Amogh refactored this to make it easier to manually
      // update the total number of unit tests we've written
      const unitTestCounts = {
        'thomaspeavler2': 10,
        'amogh-dambal': 9,
        'jtajonera1': 11,
        'kevinhou912': 6
      };

      let totalTestCount = 0;
      for (const [gid, unitTestCount] of Object.entries(unitTestCounts)) {
        ret[gid]['totalTests'] = unitTestCount
        totalTestCount += unitTestCount
      }

      ret['totalTests'] = totalTestCount;
      // ret['thomaspeavler2']['totalTests'] = 10;
      // ret['amogh-dambal']['totalTests'] = 0;
      // ret['jtajonera1']['totalTests'] = 11;
      // ret['kevinhou912']['totalTests'] = 0;

      callback(ret);
    });
}
