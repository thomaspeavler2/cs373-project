import { shallow, configure } from 'enzyme';
import MainContainer from './MainContainer';
import Adapter from '@wojtekmaj/enzyme-adapter-react-17';

configure({adapter: new Adapter()});

test('renders child components', () => {
  const wrapper = shallow(
    <MainContainer>
      <body>Hello World</body>
      <p>Goodbye World</p>
    </MainContainer>
  );

  expect(wrapper.containsAllMatchingElements(
    [<body>Hello World</body>, <p>Goodbye World</p>]
  )).toEqual(true);
});
