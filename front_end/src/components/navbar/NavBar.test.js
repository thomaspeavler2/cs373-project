import { ThemeProvider } from '@emotion/react';
import { render } from '@testing-library/react';
import { theme } from '../../utils/constants';
import renderer from 'react-test-renderer';
import NavBar from './NavBar';
import { MemoryRouter } from 'react-router';

/*
  Tests whether the NavBar is able to render without crashing
*/
test('Renders without crashing.', () => {
  jest.mock('react-router-dom', () => ({
    ...jest.requireActual('react-router-dom'),
    useHistory: () => ({
      push: jest.fn()
    })
  }));

  render(
    <ThemeProvider theme={theme}>
      <MemoryRouter>
        <NavBar />
      </MemoryRouter>
    </ThemeProvider>
  );
});

/*
  Tests whether there are unexpected changes to the NavBar.
*/
test('UI did not change', () => {
  const tree = renderer
    .create(
      <ThemeProvider theme={theme}>
        <MemoryRouter>
          <NavBar />
        </MemoryRouter>
      </ThemeProvider>
    )
    .toJSON();
  expect(tree).toMatchSnapshot();
});
