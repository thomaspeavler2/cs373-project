import React from 'react';
import { Link } from "react-router-dom";
import { Toolbar, Box, AppBar, Button } from '@mui/material';
import { styled } from '@mui/system';

const StyledAppBar = styled(AppBar)({
  backgroundColor: '#03ABD5',
});

const NavBar = (props) => {
  return (
    <>
      <Box sx={{ display: 'flex' }}>
        <StyledAppBar
          position="fixed"
        >
          <Toolbar sx={{ justifyContent: 'space-between' }}>
            <Button
              component={Link}
              to="/"
              color="secondary"
              sx={{ textTransform: 'none' }}
            >
              BroaderHorizons
            </Button>
            <Box sx={{ display: 'flex', flexDirection: 'row', flexWrap: 'wrap', paddingLeft: '8px' }}>
              <Button
                component={Link}
                to="/recipes"
                color="secondary"
              >
                Recipes
              </Button>
              <Button
                component={Link}
                to="/ingredients"
                color="secondary"
              >
                Ingredients
              </Button>
              <Button
                component={Link}
                to="/cuisines"
                color="secondary"
              >
                Cuisines
              </Button>
              <Button
                component={Link}
                to="/visualization"
                color="secondary"
              >
                Visualization
              </Button>
              <Button
                component={Link}
                to="/providerVisualization"
                color="secondary"
              >
                Provider Vis.
              </Button>
              <Button
                component={Link}
                to="/about"
                color="secondary"
              >
                About Us
              </Button>
            </Box>
          </Toolbar>
        </StyledAppBar>
        <Toolbar />
      </Box>
    </>
  );
}

export default NavBar;