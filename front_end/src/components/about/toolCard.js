import React, { useEffect, useState } from 'react';
import {
  Typography, Card, CardMedia, CardContent,
  CardActionArea
} from '@mui/material';

const ToolCard = (props) => {
    const toolName = props.name;
    const toolUse = props.text;
    const toolImageString = props.img;
    const toolLink = props.link;

    return (
        <Card sx={{maxWidth: 350}}>
            <CardActionArea href={toolLink}>
                <CardMedia 
                    height="250"
                    component="img"
                    src={toolImageString}
                />
            
                <CardContent>
                    <Typography
                        variant="h6"
                        align="center"
                    >
                        <b>{toolName}</b>
                    </Typography>
                    <Typography
                        variant="body"
                        align="center"
                    >   
                        {toolUse}
                    </Typography>

                </CardContent>
            </CardActionArea>

        </Card>
    );
}

export default ToolCard;