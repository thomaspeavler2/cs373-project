import React, { useContext, useEffect, useState } from 'react';
import { useHistory, useLocation } from 'react-router-dom'
import { Recipe, Ingredient, Cuisine, sortModelObjects } from '../../utils/models';
import { InstanceObjectContext } from '../../routes/search';
import {
  Box, Typography, Paper, Stack, Card, CardActions,
  CardHeader, Button,
} from '@mui/material';
import ReactPlayer from 'react-player';
import { Chart } from 'react-google-charts';
import { getInstanceById } from '../../utils/apiCalls';

const InstanceView = (props) => {
  const { currentInstance, setCurrentInstance } = useContext(InstanceObjectContext);
  const [leftReferences, setLeftReferences] = useState([]);
  const [rightReferences, setRightReferences] = useState([]);
  const [leftTitle, setLeftTitle] = useState('Recipe');
  const [rightTitle, setRightTitle] = useState('Ingredient');

  let leftReferencesArray = [];
  let rightReferencesArray = [];

  const history = useHistory();
  const location = useLocation();

  useEffect(() => {
    if (!location.pathname.includes('instance'))
      setCurrentInstance(null);
  }, [location]);

  const addLeftReference = (modelName, id) => {
    if (id !== '') {
      getInstanceById(modelName, id).then((newItem) => {
        if (newItem) {
          leftReferencesArray = [...leftReferencesArray, newItem];
          setLeftReferences([...leftReferencesArray]);
        }
      }).catch((err) => console.log(err));
    }
  };

  const addRightReference = (modelName, id) => {
    if (id !== '') {
      getInstanceById(modelName, id).then((newItem) => {
        if (newItem) {
          rightReferencesArray = [...rightReferencesArray, newItem];
          setRightReferences([...rightReferencesArray]);
        }
      }).catch((err) => console.log(err));
    }
  };

  useEffect(() => {
    // setReferences([]);
    if (currentInstance) {
      console.log(currentInstance.data);
      if (currentInstance.data.recipe_ids !== undefined && currentInstance.data.ingredient_ids !== undefined) {
        setLeftTitle('Recipe');
        setRightTitle('Ingredient');
        currentInstance.data.recipe_ids.split(',').forEach((item) => addLeftReference('recipe', item));
        currentInstance.data.ingredient_ids.split(',').forEach((item) => addRightReference('ingredient', item));
      } else if (currentInstance.data.recipe_ids !== undefined && currentInstance.data.cuisine_ids !== undefined) {
        setLeftTitle('Recipe');
        setRightTitle('Cuisine');
        currentInstance.data.recipe_ids.split(',').forEach((item) => addLeftReference('recipe', item));
        currentInstance.data.cuisine_ids.split(',').forEach((item) => addRightReference('cuisines', item));
      } else if (currentInstance.data.ingredient_ids !== undefined && currentInstance.data.cuisine_ids !== undefined) {
        setLeftTitle('Ingredient');
        setRightTitle('Cuisine'); 
        currentInstance.data.ingredient_ids.split(',').forEach((item) => addLeftReference('ingredient', item));
        currentInstance.data.cuisine_ids.split(',').forEach((item) => addRightReference('cuisines', item));
      }
    }
  }, [currentInstance]);

  window.addEventListener('popstate', (event) => {
    setCurrentInstance(null);
  });

  return (
    <>
      {currentInstance !== null && (
        <Box>
          {/* xs is out of 12 -> each row will add up to 12. */}
          <Stack spacing={6} sx={{ width: '100%', paddingTop: '32px' }}>
            <Box sx={{ display: 'flex', justifyContent: 'center' }}>
              <Typography variant="h2"><b>{currentInstance?.data.name}</b></Typography>
            </Box>
            <Box sx={{ padding: '16px' }}>
              <img
                src={currentInstance?.data.image}
                alt='food'
                style={{
                  float: 'left',
                  marginRight: '8px',
                  width: '55vh',
                  height: '55vh'
                }}
              />
              <Typography sx={{ whiteSpace: 'pre-line' }} variant="body">{currentInstance?.data.text}</Typography>
            </Box>
            <Box sx={{
              display: 'flex',
              justifyContent: 'center',
              backgroundColor: '#DCDCDC',
              padding: '32px'
            }}
            >
              {currentInstance?.data.video !== undefined ? (
                <ReactPlayer url={`https://www.youtube.com/watch?v=${currentInstance?.data.video}`} />
              ) : currentInstance?.data.macronutrients !== undefined ? (
                <Chart
                  width={'500px'}
                  height={'300px'}
                  chartType="PieChart"
                  loader={<div>Loading Chart</div>}
                  data={[
                    ['Task', 'Hours per Day'],
                    ['Carbs', currentInstance.data.macronutrients.percentCarbs],
                    ['Fat', currentInstance.data.macronutrients.percentFat],
                    ['Protein', currentInstance.data.macronutrients.percentProtein],
                  ]}
                />
              ) : (
                <iframe src={`https://www.google.com/maps/embed/v1/place?key=AIzaSyBVZC2vn0TVNBn5iZObheWG0Vl3gpAh4RM&q=${currentInstance?.data.country}`}
                  width='600'
                  height='450'
                  style={{border:0}}
                  loading='lazy'
                  title='map'/>
              )}
            </Box>
            <Box sx={{
              display: 'flex',
              flexDirection: 'row',
              justifyContent: 'center',
              flexWrap: 'wrap',
              width: '100%'
            }}
            >
              {leftReferences.map((instance) => (
                <Card sx={{
                  width: '300px',
                  marginRight: '16px',
                  marginBottom: '8px',
                  cursor: 'pointer'
                }}
                  onClick={() => (setCurrentInstance(instance))}
                >
                  <CardHeader
                    title={instance.data.name}
                    subheader={leftTitle}
                  />
                  <CardActions>
                    <Button
                      variant="outlined"
                      onClick={() => (setCurrentInstance(instance))}
                    >
                      Learn More
                    </Button>
                  </CardActions>
                </Card>
              ))}
            </Box>

            <Box sx={{
              display: 'flex',
              flexDirection: 'row',
              justifyContent: 'center',
              flexWrap: 'wrap',
              width: '100%'
            }}
            >
              {rightReferences.map((instance) => (
                <Card sx={{
                  width: '300px',
                  marginRight: '16px',
                  marginBottom: '8px',
                  cursor: 'pointer'
                }}
                  onClick={() => (setCurrentInstance(instance))}
                >
                  <CardHeader
                    title={instance.data.name}
                    subheader={rightTitle}
                  />
                  <CardActions>
                    <Button
                      variant="outlined"
                      onClick={() => (setCurrentInstance(instance))}
                    >
                      Learn More
                    </Button>
                  </CardActions>
                </Card>
              ))}
            </Box>
          </Stack>
        </Box >
      )}
    </>
  );
};

export default InstanceView;