import React from 'react';
import { ThemeProvider } from "@emotion/react";
import { render } from "@testing-library/react";
import { theme } from "../../utils/constants";
import BarAndTable from "./BarAndTable";
import InstanceView from "./InstanceView";
import * as ApiCalls from '../../utils/apiCalls';
import { mount, configure } from 'enzyme';
import Adapter from '@wojtekmaj/enzyme-adapter-react-17';
import { FormControl } from '@mui/material';
import { MemoryRouter } from 'react-router';

configure({ adapter: new Adapter() });

const exampleData = {
  "uid": 44871,
  "name": "apple cider",
  "estimated_cost": 0.14,
  "estimated_cost_unit": "US Cents",
  "weight_per_serving": 1,
  "weight_per_serving_unit": "g",
  "macronutrients": JSON.stringify({
    "percentProtein": 0.86,
    "percentFat": 2.5,
    "percentCarbs": 96.64
  }),
  "nutrition_info": JSON.stringify([
    {
      "title": "Vitamin B3",
      "name": "Vitamin B3",
      "amount": 0,
      "unit": "mg"
    },
    {
      "title": "Folic Acid",
      "name": "Folic Acid",
      "amount": 0,
      "unit": "µg"
    },
    {
      "title": "Iron",
      "name": "Iron",
      "amount": 0,
      "unit": "mg"
    },
    {
      "title": "Carbohydrates",
      "name": "Carbohydrates",
      "amount": 0.11,
      "unit": "g"
    },
    {
      "title": "Vitamin C",
      "name": "Vitamin C",
      "amount": 0.01,
      "unit": "mg"
    },
    {
      "title": "Vitamin A",
      "name": "Vitamin A",
      "amount": 0.01,
      "unit": "IU"
    },
    {
      "title": "Vitamin B5",
      "name": "Vitamin B5",
      "amount": 0,
      "unit": "mg"
    },
    {
      "title": "Mono Unsaturated Fat",
      "name": "Mono Unsaturated Fat",
      "amount": 0,
      "unit": "g"
    },
    {
      "title": "Caffeine",
      "name": "Caffeine",
      "amount": 0,
      "unit": "mg"
    },
    {
      "title": "Phosphorus",
      "name": "Phosphorus",
      "amount": 0.07,
      "unit": "mg"
    },
    {
      "title": "Sodium",
      "name": "Sodium",
      "amount": 0.04,
      "unit": "mg"
    },
    {
      "title": "Vitamin D",
      "name": "Vitamin D",
      "amount": 0,
      "unit": "µg"
    },
    {
      "title": "Protein",
      "name": "Protein",
      "amount": 0,
      "unit": "g"
    },
    {
      "title": "Vitamin B6",
      "name": "Vitamin B6",
      "amount": 0,
      "unit": "mg"
    },
    {
      "title": "Poly Unsaturated Fat",
      "name": "Poly Unsaturated Fat",
      "amount": 0,
      "unit": "g"
    },
    {
      "title": "Selenium",
      "name": "Selenium",
      "amount": 0,
      "unit": "µg"
    },
    {
      "title": "Choline",
      "name": "Choline",
      "amount": 0.02,
      "unit": "mg"
    },
    {
      "title": "Sugar",
      "name": "Sugar",
      "amount": 0.1,
      "unit": "g"
    },
    {
      "title": "Fiber",
      "name": "Fiber",
      "amount": 0,
      "unit": "g"
    },
    {
      "title": "Vitamin B12",
      "name": "Vitamin B12",
      "amount": 0,
      "unit": "µg"
    },
    {
      "title": "Vitamin B1",
      "name": "Vitamin B1",
      "amount": 0,
      "unit": "mg"
    },
    {
      "title": "Net Carbohydrates",
      "name": "Net Carbohydrates",
      "amount": 0.11,
      "unit": "g"
    },
    {
      "title": "Calories",
      "name": "Calories",
      "amount": 0.46,
      "unit": "kcal"
    },
    {
      "title": "Calcium",
      "name": "Calcium",
      "amount": 0.08,
      "unit": "mg"
    },
    {
      "title": "Manganese",
      "name": "Manganese",
      "amount": 0,
      "unit": "mg"
    },
    {
      "title": "Copper",
      "name": "Copper",
      "amount": 0,
      "unit": "mg"
    },
    {
      "title": "Vitamin B2",
      "name": "Vitamin B2",
      "amount": 0,
      "unit": "mg"
    },
    {
      "title": "Saturated Fat",
      "name": "Saturated Fat",
      "amount": 0,
      "unit": "g"
    },
    {
      "title": "Vitamin K",
      "name": "Vitamin K",
      "amount": 0,
      "unit": "µg"
    },
    {
      "title": "Alcohol",
      "name": "Alcohol",
      "amount": 0,
      "unit": "g"
    },
    {
      "title": "Zinc",
      "name": "Zinc",
      "amount": 0,
      "unit": "mg"
    },
    {
      "title": "Folate",
      "name": "Folate",
      "amount": 0,
      "unit": "µg"
    },
    {
      "title": "Fat",
      "name": "Fat",
      "amount": 0,
      "unit": "g"
    },
    {
      "title": "Potassium",
      "name": "Potassium",
      "amount": 1.01,
      "unit": "mg"
    },
    {
      "title": "Magnesium",
      "name": "Magnesium",
      "amount": 0.05,
      "unit": "mg"
    },
    {
      "title": "Cholesterol",
      "name": "Cholesterol",
      "amount": 0,
      "unit": "mg"
    },
    {
      "title": "Vitamin E",
      "name": "Vitamin E",
      "amount": 0,
      "unit": "mg"
    }
  ]),
  "aisle": "Beverages"
}

jest.mock("react-router-dom", () => ({
  ...jest.requireActual("react-router-dom"),
  useLocation: () => ({
    pathname: "localhost:3000/Recipes"
  })
}));

test('Renders without crashing', () => {
  render(
    <ThemeProvider theme={theme}>
      <MemoryRouter>
        <BarAndTable name='Ingredients' />
        <InstanceView name='Ingredients' />
      </MemoryRouter>
    </ThemeProvider>
  );
});

describe('Searching flow tests', () => {
  const apiCall = jest.spyOn(ApiCalls, 'getInstancesWithQueryUrl')
    .mockImplementation((name, callback) => []);

  afterEach(() => {
    jest.clearAllMocks();
  });

  it('Make sure first api call happens', () => {
    render(
      <ThemeProvider theme={theme}>
        <MemoryRouter>
          <BarAndTable name='Ingredients' />
          <InstanceView name='Ingredients' />
        </MemoryRouter>
      </ThemeProvider>
    );

    expect(apiCall).toHaveBeenCalled();
  });

  it('Make sure filtering/sorting api call happens', () => {
    const wrapper = mount(
      <MemoryRouter>
        <BarAndTable name='Ingredients' testData={[exampleData]} />
      </MemoryRouter>
    );
    
    wrapper.find(FormControl).first().simulate('click');
    wrapper.find(FormControl).first().simulate('keypress', {key: 'Enter'});
    expect(apiCall).toHaveBeenCalled();
  });
})