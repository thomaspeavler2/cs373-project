import React, { useContext, useEffect, useState } from 'react';
import {
  Stack, Box, Typography, Paper, CircularProgress,
  FormControl, OutlinedInput, InputLabel, Tooltip,
  InputAdornment, Table, TableHead, IconButton, Select,
  TableRow, TableBody, TableCell, Toolbar, TableSortLabel,
  Card, CardHeader, CardActions, Button, Pagination, MenuItem,
  FormGroup, FormControlLabel, Checkbox,
} from '@mui/material';
import SearchIcon from '@mui/icons-material/Search';
import FilterListIcon from '@mui/icons-material/FilterList';
import { Ingredient, Recipe, Cuisine, sortModelObjects } from '../../utils/models';
import { InstanceObjectContext } from '../../routes/search';
import { useHistory, } from 'react-router';
import { recipes, ingredients, cuisines } from '../../utils/instanceData';
import { getAllInstances, getInstancesWithQueryUrl, getInstancesWithSearch } from '../../utils/apiCalls';
import { aisles, apiUrlBase, countries, regions } from '../../utils/constants';

const BarAndTable = (props) => {
  const [searchText, setSearchText] = useState('');
  const [isLoading, setIsLoading] = useState(false);
  const [searchResults, setSearchResults] = useState([]);
  const [isFilterOpen, setIsFilterOpen] = useState(true);

  const [sortAscending, setSortAscending] = useState(true);
  const [sortIndex, setSortIndex] = useState(0);
  const [page, setPage] = useState(1);
  const [numResults, setNumResults] = useState(0);
  const [numPages, setNumPages] = useState(1);
  const [resultsPerPage, setResultsPerPage] = useState(10);

  const [recipeResults, setRecipeResults] = useState([]);
  const [ingredientResults, setIngredientResults] = useState([]);
  const [cuisineResults, setCuisineResults] = useState([]);


  const { currentInstance, setCurrentInstance } = useContext(InstanceObjectContext);

  // Filtering variables

  // Recipes
  const [servingsQuery, setServingsQuery] = useState('');
  const [vegetarianQuery, setVegetarianQuery] = useState('both');
  const [dairyFreeQuery, setDairyFreeQuery] = useState('both');
  const [prepQuery, setPrepQuery] = useState('');

  // Ingredients
  const [aisleQuery, setAisleQuery] = useState('');
  const [costQuery, setCostQuery] = useState('');
  const [proteinQuery, setProteinQuery] = useState('both');

  // Cuisines
  const [countryQuery, setCountryQuery] = useState('');
  const [regionQuery, setRegionQuery] = useState('');

  // Sort
  const [sortQuery, setSortQuery] = useState('');

  const history = useHistory();

  let searchField;

  useEffect(() => {
    setIsLoading(true);
    setPage(1);
    getInstancesWithQueryUrl(props.name.toLowerCase(), constructQueryUrl(), searchCallback);
  }, [servingsQuery, vegetarianQuery, dairyFreeQuery,
    prepQuery, aisleQuery, costQuery, proteinQuery,
    countryQuery, regionQuery, sortQuery])

  useEffect(() => {
    getInstancesWithQueryUrl(props.name.toLowerCase(), constructQueryUrl(), searchCallback);
  }, [page, resultsPerPage])

  const constructQueryUrl = () => {
    var url = apiUrlBase + props.name.toLowerCase() + '?';

    var variables = [];

    if (props.name === "Recipes") {
      variables = [servingsQuery, vegetarianQuery, dairyFreeQuery, prepQuery];
    } else if (props.name === "Ingredients") {
      variables = [aisleQuery, costQuery, proteinQuery];
    } else {
      variables = [countryQuery, regionQuery];
    }

    variables.forEach((v) => {
      if (v !== '' && v !== 'both') {
        url += v + '&';
      }
    })

    url += `page=${page}&perPage=${resultsPerPage}`

    if (sortQuery !== '') {
      url += `&sort=${sortQuery}`;
    }

    return url;
  };

  const searchCallback = (results, count) => {
    setSearchResults(results);
    setNumResults(count);
    setNumPages(Math.max(Math.ceil(count / resultsPerPage), 1));
    setIsLoading(false);
  };

  const highlightSearchWord = (text) => {
    if (text === undefined || text === null) {
      return 'N/A';
    }

    if (typeof (text) === 'number') {
      text = `${text}`;
    }

    const searchParts = searchText.toLowerCase().split(' ');
    const textParts = text.split(' ');

    const temp = [];

    textParts.forEach((part) => {
      if (searchParts.includes(part.toLowerCase())) {
        temp.push(<mark>{part}</mark>)
      } else {
        temp.push(part)
      }

      temp.push(' ')
    })

    temp.pop();

    return (
      <>
        {temp.map((tmp) => (tmp))}
      </>
    )
  };

  useEffect(() => {
    if (props.testData === undefined) {
      setSearchResults([]);
      if (props.name !== 'Everything') {
        setIsFilterOpen(true);
        setSortIndex(0);
        setSortAscending(true);
        setSearchText('');
        setPage(1);
        setResultsPerPage(10);
        setIsLoading(true);
        setVegetarianQuery('both');
        setDairyFreeQuery('both');
        setProteinQuery('both')
        const variables = [setServingsQuery, setPrepQuery,
          setAisleQuery, setCostQuery, setCountryQuery,
          setRegionQuery];

        variables.forEach((v) => (
          v('')
        ));

        getInstancesWithQueryUrl(props.name.toLowerCase(), constructQueryUrl(), searchCallback);
      } else {
        setIsLoading(false);
      }
    }
  }, [props.name]);

  const sitewideCallback = (modelName, results) => {
    setIsLoading(false);
    if (modelName === "recipes") {
      setRecipeResults(results);
    } else if (modelName === 'ingredients') {
      setIngredientResults(results);
    } else {
      setCuisineResults(results);
    }
  };

  useEffect(() => {
    if (history.location?.state?.search !== undefined) {
      setSearchText(history.location.state.search);
      var state = { ...history.location.state };
      const urls = [`${apiUrlBase}recipes?q=${state.search}`,
      `${apiUrlBase}ingredients?q=${state.search}`,
      `${apiUrlBase}cuisines?q=${state.search}`];
      getInstancesWithSearch('recipes', urls[0], sitewideCallback);
      getInstancesWithSearch('ingredients', urls[1], sitewideCallback);
      getInstancesWithSearch('cuisines', urls[2], sitewideCallback);
      delete state.search;
      history.replace({ ...history.location, state });
    }
  }, [history]);

  useEffect(() => {
    if (searchField) {
      searchField.value = searchText;
    }
  });

  useEffect(() => {
    if (props.testData !== undefined) {
      setSearchResults(props.testData.map((data) => new Ingredient(data)));
      setIsLoading(false);
    }
  }, []);

  return (
    <>
      {currentInstance === null && (
        <Box sx={{ display: 'flex', alignItems: 'stretch', flexDirection: 'column' }}>
          <Box>
            <Stack justifyContent="center" alignItems="stretch">
              <Box sx={{
                display: 'flex',
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'center',
                margin: '64px 32px',
              }}>

                <Typography variant="h3">Let's Find Some Good {props.name}</Typography>
              </Box>
              <Box sx={{ display: 'flex', flexDirection: 'column', alignItems: 'center' }}>
                <FormControl sx={{
                  width: '75vw'
                }}>
                  <InputLabel>Search</InputLabel>
                  <OutlinedInput
                    fullWidth
                    sx={{
                      backgroundColor: '#FFFFFF'
                    }}
                    id="input-with-sx"
                    label="Search"
                    variant="standard"
                    inputRef={(element) => searchField = element}
                    onKeyPress={(event) => {
                      if (event.key === 'Enter') {
                        const search = event.target.value;
                        setSearchText(search);
                        setIsLoading(true);
                        if (props.name === 'Everything') {
                          if (search !== '') {
                            const urls = [`${apiUrlBase}recipes?q=${search}`,
                            `${apiUrlBase}ingredients?q=${search}`,
                            `${apiUrlBase}cuisines?q=${search}`];
                            getInstancesWithSearch('recipes', urls[0], sitewideCallback);
                            getInstancesWithSearch('ingredients', urls[1], sitewideCallback);
                            getInstancesWithSearch('cuisines', urls[2], sitewideCallback);
                          }
                        } else {
                          if (search === '') {
                            const url = `${apiUrlBase}${props.name.toLowerCase()}?page=1&perPage=${resultsPerPage}`;
                            getInstancesWithQueryUrl(props.name.toLowerCase(), url, searchCallback);
                          } else {
                            const url = `${apiUrlBase}${props.name.toLowerCase()}?q=${search}`;
                            getInstancesWithQueryUrl(props.name.toLowerCase(), url, searchCallback);
                          }
                        }
                      }
                    }}
                    startAdornment={
                      <InputAdornment position="start">
                        <SearchIcon />
                      </InputAdornment>
                    }
                  />
                </FormControl>
              </Box>
            </Stack>
          </Box>
          <Box sx={{
            margin: '32px',
            overflow: 'auto'
          }}>
            {isLoading && (
              <Box sx={{ display: 'flex', justifyContent: 'center', marginTop: '16px' }}>
                <CircularProgress />
              </Box>
            )}
            {searchResults.length > 0 && !isLoading && props.name !== 'Ingredients' && (
              <Paper>
                <Toolbar sx={{ display: 'flex', justifyContent: 'space-between' }}>
                  <Box>
                    <Typography variant="h6">Search results for {searchText}</Typography>
                  </Box>
                  <Box>
                    <Tooltip title="Filter list">
                      <IconButton onClick={() => (setIsFilterOpen(!isFilterOpen))}>
                        <FilterListIcon />
                      </IconButton>
                    </Tooltip>
                  </Box>
                </Toolbar>
                {isFilterOpen && (
                  <Toolbar>
                    {props.name === 'Recipes' && (
                      <Box sx={{ width: '100%', paddingBottom: '16px' }}>
                        <FormControl sx={{ minWidth: '150px', paddingRight: '16px' }}>
                          <InputLabel id="prep-label">Prep Time</InputLabel>
                          <Select
                            labelId='prep-label'
                            label='Prep Time'
                            value={prepQuery}
                            onChange={(event) => (setPrepQuery(event.target.value))}
                          >
                            <MenuItem value={'prep=0&prep=30'}>0 - 30</MenuItem>
                            <MenuItem value={'prep=30&prep=60'}>30 - 60</MenuItem>
                            <MenuItem value={'prep=60&prep=999999'}>60+</MenuItem>
                          </Select>
                        </FormControl>
                        <FormControl sx={{ minWidth: '150px', paddingRight: '16px' }}>
                          <InputLabel id="servings-label">Servings</InputLabel>
                          <Select
                            labelId='servings-label'
                            label='Servings'
                            value={servingsQuery}
                            onChange={(event) => (setServingsQuery(event.target.value))}
                          >
                            <MenuItem value={'servings=0&servings=5'}>0 - 5</MenuItem>
                            <MenuItem value={'servings=5&servings=10'}>5 - 10</MenuItem>
                            <MenuItem value={'servings=10&servings=999999'}>10+</MenuItem>
                          </Select>
                        </FormControl>
                        <FormControl sx={{ minWidth: '150px', paddingRight: '16px' }}>
                          <InputLabel id="servings-label">Dairy Free</InputLabel>
                          <Select
                            labelId='servings-label'
                            label='Dairy Free'
                            value={dairyFreeQuery}
                            onChange={(event) => (setDairyFreeQuery(event.target.value))}
                          >
                            <MenuItem value={'dairyfree=true'}>Yes</MenuItem>
                            <MenuItem value={'dairyfree=false'}>No</MenuItem>
                            <MenuItem value={'both'}>Both</MenuItem>
                          </Select>
                        </FormControl>
                        <FormControl sx={{ minWidth: '150px', paddingRight: '16px' }}>
                          <InputLabel id="servings-label">Vegetarian</InputLabel>
                          <Select
                            labelId='servings-label'
                            label='Vegetarian'
                            value={vegetarianQuery}
                            onChange={(event) => (setVegetarianQuery(event.target.value))}
                          >
                            <MenuItem value={'vegetarian=true'}>Yes</MenuItem>
                            <MenuItem value={'vegetarian=false'}>No</MenuItem>
                            <MenuItem value={'both'}>Both</MenuItem>
                          </Select>
                        </FormControl>
                      </Box>
                    )}
                    {props.name === 'Cuisines' && (
                      <Box sx={{ width: '100%', paddingBottom: '16px' }}>
                        <FormControl sx={{ minWidth: '150px', paddingRight: '16px' }}>
                          <InputLabel id="country-label">Country</InputLabel>
                          <Select
                            labelId='country-label'
                            label='Country'
                            value={countryQuery}
                            onChange={(event) => (setCountryQuery(event.target.value))}
                          >
                            {countries.map((country, index) => (
                              <MenuItem value={`country=${country}`}>{country}</MenuItem>
                            ))}
                          </Select>
                        </FormControl>
                        <FormControl sx={{ minWidth: '150px', paddingRight: '16px' }}>
                          <InputLabel id="region-label">Region</InputLabel>
                          <Select
                            labelId='region-label'
                            label='Region'
                            value={regionQuery}
                            onChange={(event) => (setRegionQuery(event.target.value))}
                          >
                            {regions.map((region, index) => (
                              <MenuItem value={`region=${region}`}>{region}</MenuItem>
                            ))}
                          </Select>
                        </FormControl>
                      </Box>
                    )}
                  </Toolbar>
                )}
                <Box sx={{ overflow: 'auto' }}>
                  <Table>
                    <TableHead>
                      <TableRow sx={{ backgroundColor: '#F7F7F7' }}>
                        {searchResults[0].getColumnNames().map((name, index) => (
                          <TableCell>
                            {searchResults[0].isSortable(name) && (
                              <TableSortLabel
                                active={index === sortIndex}
                                direction={sortAscending ? 'asc' : 'desc'}
                                onClick={() => {
                                  setSortIndex(index);
                                  const query = searchResults[0].getSortQuery(name);
                                  if (sortIndex === index) {
                                    if (sortAscending)
                                      setSortQuery(`-${query}`);
                                    else
                                      setSortQuery(query);
                                    setSortAscending(!sortAscending);
                                  } else {
                                    setSortQuery(query);
                                    setSortAscending(true);
                                  }
                                }}
                              >
                                <b>{name}</b>
                              </TableSortLabel>
                            )}
                            {!searchResults[0].isSortable(name) && (
                              <b>{name}</b>
                            )}
                          </TableCell>
                        ))}
                      </TableRow>
                    </TableHead>
                    <TableBody>
                      {searchResults
                        .map((result, index) => (
                          <TableRow
                            hover
                            sx={{ cursor: 'pointer' }}
                            key={index}
                            onClick={() => {
                              history.push(`/${props.name.toLowerCase()}/instance`);
                              setCurrentInstance(searchResults[index]);
                            }}
                          >
                            {result.getModelPageKeys().map((value) => (
                              <TableCell>
                                {
                                  typeof (result.data[value]) === "boolean" ?
                                    (result.data[value] ? 'Yes' : 'No') : highlightSearchWord(result.data[value])
                                }
                              </TableCell>
                            ))}
                          </TableRow>
                        ))}
                    </TableBody>
                  </Table>
                </Box>
              </Paper>
            )}
            {searchResults.length > 0 && !isLoading && props.name === 'Ingredients' && (
              <Box>
                <Paper>
                  <Toolbar sx={{ display: 'flex', justifyContent: 'space-between' }}>
                    <Box>
                      <Typography variant="h6">Search results for {searchText}</Typography>
                    </Box>
                    <Box>
                      <Tooltip title="Filter list">
                        <IconButton onClick={() => (setIsFilterOpen(!isFilterOpen))}>
                          <FilterListIcon />
                        </IconButton>
                      </Tooltip>
                    </Box>
                  </Toolbar>

                  {isFilterOpen && (
                    <Toolbar>
                      <FormControl sx={{ minWidth: '150px', padding: '0px 16px 16px 0px' }}>
                        <InputLabel id="aisle-label">Aisle</InputLabel>
                        <Select
                          labelId='aisle-label'
                          label='Aisle'
                          value={aisleQuery}
                          onChange={(event) => (setAisleQuery(event.target.value))}
                        >
                          {aisles.map((aisle) => (
                            <MenuItem value={`aisle=${aisle}`}>{aisle}</MenuItem>
                          ))}
                        </Select>
                      </FormControl>
                      <FormControl sx={{ minWidth: '150px', padding: '0px 16px 16px 0px' }}>
                        <InputLabel id="cost-label">Cost</InputLabel>
                        <Select
                          labelId='cost-label'
                          label='Cost'
                          value={costQuery}
                          onChange={(event) => (setCostQuery(event.target.value))}
                        >
                          <MenuItem value={'cost=0&cost=1'}>$0 - $1</MenuItem>
                          <MenuItem value={'cost=1&cost=10'}>$1 - $10</MenuItem>
                          <MenuItem value={'cost=10&cost=50'}>$10 - $50</MenuItem>
                          <MenuItem value={'cost=50&cost=999999'}>$50+</MenuItem>
                        </Select>
                      </FormControl>
                      <FormControl sx={{ minWidth: '150px', padding: '0px 16px 16px 0px' }}>
                        <InputLabel id="cost-label">Protein %</InputLabel>
                        <Select
                          labelId='protein-label'
                          label='protein'
                          value={proteinQuery}
                          onChange={(event) => (setProteinQuery(event.target.value))}
                        >
                          <MenuItem value={'both'}>Any</MenuItem>
                          <MenuItem value={'protein=high'}>High</MenuItem>
                        </Select>
                      </FormControl>
                      <FormControl sx={{ minWidth: '150px', padding: '0px 16px 16px 0px' }}>
                        <InputLabel id="prep-label">Sort By</InputLabel>
                        <Select
                          labelId='prep-label'
                          label='Sort By'
                          value={sortQuery}
                          onChange={(event) => (setSortQuery(event.target.value))}
                        >
                          <MenuItem value={'name'}>Name Ascending</MenuItem>
                          <MenuItem value={'-name'}>Name Descending</MenuItem>
                          <MenuItem value={'cost'}>Cost Ascending</MenuItem>
                          <MenuItem value={'-cost'}>Cost Descending</MenuItem>
                          <MenuItem value={'aisle'}>Aisle Ascending</MenuItem>
                          <MenuItem value={'-aisle'}>Aisle Descending</MenuItem>
                          <MenuItem value={'wps'}>Weight Per Serving Asc.</MenuItem>
                          <MenuItem value={'-wps'}>Weight Per Serving Desc.</MenuItem>
                        </Select>
                      </FormControl>
                    </Toolbar>
                  )}
                </Paper>
                <Box
                  sx={{
                    display: 'flex',
                    flexDirection: 'row',
                    flexWrap: 'wrap',
                    justifyContent: 'center',
                    padding: '32px'
                  }}
                >
                  {searchResults
                    .map((result, index) => (
                      <Card
                        sx={{ width: '250px', margin: '0px 8px 16px 0px', cursor: 'pointer' }}
                        onClick={() => {
                          history.push(`/${props.name.toLowerCase()}/instance`);
                          setCurrentInstance(searchResults[index]);
                        }}
                      >
                        <CardHeader
                          title={highlightSearchWord(result.data['name'])}
                        />
                        <Box sx={{ padding: '8px' }}>
                          {result.getModelPageKeys().map((key, index) => (
                            <Typography>
                              <b>{result.getColumnNames()[index]}</b>: {typeof (result.data[key]) === "boolean" ?
                                (result.data[key] ? 'Yes' : 'No') : highlightSearchWord(result.data[key])}
                            </Typography>
                          ))}
                        </Box>
                        <CardActions>
                          <Button
                            variant="outlined"
                            onClick={() => {
                              history.push(`/${props.name.toLowerCase()}/instance`);
                              setCurrentInstance(searchResults[index]);
                            }}
                          >
                            Learn More
                          </Button>
                        </CardActions>
                      </Card>
                    ))}
                </Box>
              </Box>
            )}
            {(recipeResults.length > 0 || ingredientResults.length > 0 || cuisineResults.length > 0)
              && !isLoading && props.name === 'Everything' && (
                <Box>
                  <Typography variant='h4'>Recipes</Typography>
                  <Box
                    sx={{
                      display: 'flex',
                      flexDirection: 'row',
                      flexWrap: 'wrap',
                      justifyContent: 'center',
                      padding: '32px'
                    }}
                  >
                    {recipeResults.map((result, index) => (
                      <Card
                        sx={{ width: '250px', margin: '0px 8px 16px 0px', cursor: 'pointer' }}
                        onClick={() => {
                          history.push(`/searchresults/instance`);
                          setCurrentInstance(recipeResults[index]);
                        }}
                      >
                        <CardHeader
                          title={highlightSearchWord(result.data['name'])}
                        />
                        <Box sx={{ padding: '8px' }}>
                          {result.getModelPageKeys().map((key, index) => (
                            <Typography>
                              <b>{result.getColumnNames()[index]}</b>: {typeof (result.data[key]) === "boolean" ?
                                (result.data[key] ? 'Yes' : 'No') : highlightSearchWord(result.data[key])}
                            </Typography>
                          ))}
                        </Box>
                        <CardActions>
                          <Button
                            variant="outlined"
                            onClick={() => {
                              history.push(`/searchresults/instance`);
                              setCurrentInstance(recipeResults[index]);
                            }}
                          >
                            Learn More
                          </Button>
                        </CardActions>
                      </Card>
                    ))
                    }
                  </Box>
                  <Typography variant='h4'>Ingredients</Typography>
                  <Box
                    sx={{
                      display: 'flex',
                      flexDirection: 'row',
                      flexWrap: 'wrap',
                      justifyContent: 'center',
                      padding: '32px'
                    }}
                  >
                    {ingredientResults.map((result, index) => (
                      <Card
                        sx={{ width: '250px', margin: '0px 8px 16px 0px', cursor: 'pointer' }}
                        onClick={() => {
                          history.push(`/searchresults/instance`);
                          setCurrentInstance(ingredientResults[index]);
                        }}
                      >
                        <CardHeader
                          title={highlightSearchWord(result.data['name'])}
                        />
                        <Box sx={{ padding: '8px' }}>
                          {result.getModelPageKeys().map((key, index) => (
                            <Typography>
                              <b>{result.getColumnNames()[index]}</b>: {typeof (result.data[key]) === "boolean" ?
                                (result.data[key] ? 'Yes' : 'No') : highlightSearchWord(result.data[key])}
                            </Typography>
                          ))}
                        </Box>
                        <CardActions>
                          <Button
                            variant="outlined"
                            onClick={() => {
                              history.push(`/searchresults/instance`);
                              setCurrentInstance(ingredientResults[index]);
                            }}
                          >
                            Learn More
                          </Button>
                        </CardActions>
                      </Card>
                    ))
                    }
                  </Box>

                  <Typography variant='h4'>Cuisines</Typography>
                  <Box
                    sx={{
                      display: 'flex',
                      flexDirection: 'row',
                      flexWrap: 'wrap',
                      justifyContent: 'center',
                      padding: '32px'
                    }}
                  >
                    {cuisineResults.map((result, index) => {
                      return <Card
                        sx={{ width: '250px', margin: '0px 8px 16px 0px', cursor: 'pointer' }}
                        onClick={() => {
                          history.push(`/searchresults/instance`);
                          setCurrentInstance(cuisineResults[index]);
                        }}
                      >
                        <CardHeader
                          title={highlightSearchWord(result.data['name'])}
                        />
                        <Box sx={{ padding: '8px' }}>
                          {result.getModelPageKeys().map((key, index) => (
                            <Typography>
                              <b>{result.getColumnNames()[index]}</b>: {typeof (result.data[key]) === "boolean" ?
                                (result.data[key] ? 'Yes' : 'No') : highlightSearchWord(result.data[key])}
                            </Typography>
                          ))}
                        </Box>
                        <CardActions>
                          <Button
                            variant="outlined"
                            onClick={() => {
                              history.push(`/searchresults/instance`);
                              setCurrentInstance(cuisineResults[index]);
                            }}
                          >
                            Learn More
                          </Button>
                        </CardActions>
                      </Card>
                    })
                    }
                  </Box>
                </Box>
              )}
            {searchResults.length > 0 && !isLoading && (
              <Box sx={{
                display: 'flex',
                width: '100%',
                justifyContent: 'space-between',
                alignItems: 'center',
                flexWrap: 'wrap',
                paddingTop: '32px',
                paddingBottom: '32px'
              }}
              >
                <Typography
                  variant="h7"
                  sx={{ paddingLeft: '32px', paddingBottom: '16px' }}
                >
                  Number of Results: {numResults}
                </Typography>
                <Box sx={{
                  display: 'flex',
                  flexDirection: 'row',
                  alignItems: 'baseline',
                  paddingRight: '32px'
                }}
                >
                  <FormControl sx={{ width: '150px' }}>
                    <InputLabel>Results Per Page</InputLabel>
                    <Select
                      label='Results Per Page'
                      value={resultsPerPage}
                      onChange={(event) => {
                        setPage(1);
                        setIsLoading(true);
                        setResultsPerPage(event.target.value);
                        setNumPages(Math.max(Math.ceil(searchResults.length / event.target.value), 1));
                      }}
                      autoWidth
                    >
                      <MenuItem value={10}>10</MenuItem>
                      <MenuItem value={25}>25</MenuItem>
                      <MenuItem value={50}>50</MenuItem>
                      <MenuItem value={100}>100</MenuItem>
                    </Select>
                  </FormControl>
                  <Pagination
                    count={numPages}
                    page={page}
                    onChange={(event, value) => {
                      setPage(value);
                      setIsLoading(true);
                    }}
                  />
                </Box>
              </Box>
            )}
          </Box>
        </Box>
      )}
    </>
  );
}

export default BarAndTable;
