import React from 'react';
import { Box } from '@mui/material';
import { styled } from '@mui/system';

const StyledContainer = styled(Box)({
  display: 'flex',
  flexDirection: 'column',
  width: '100%',
  minHeight: '100vh',
  height: '100%',
  backgroundColor: '#FAF9F6'
});

const MainContainer = (props) => {
  return (
    <StyledContainer>
      {props.children}
    </StyledContainer>
  );
};

export default MainContainer;