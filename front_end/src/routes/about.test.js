import React from 'react';
import { render } from "@testing-library/react";
import AboutRoute from './about';
import * as gitLabAPI from '../utils/gitlabAPI';

const mockApiCall = jest.spyOn(gitLabAPI, 'getGitLabData');
mockApiCall.mockResolvedValue([
  {
    "statistics": {
      "counts": {
        "all": 60,
        "closed": 34,
        "opened": 26
      }
    }
  },
  [
    {
      "id": "1b3a07330f65e9b601e212143c6804610fa66d71",
      "short_id": "1b3a0733",
      "created_at": "2021-10-04T23:18:16.000+00:00",
      "parent_ids": [
        "7a69bc292c57a5496212340d846c6bf246439ea2",
        "efa29f5843282f4e1bcb08bf12d0aaf22f474524"
      ],
      "title": "Merge branch 'feature-scrape-data' into 'stage'",
      "message": "Merge branch 'feature-scrape-data' into 'stage'\n\nFeature scrape data\n\nSee merge request thomaspeavler2/cs373-project!31",
      "author_name": "Amogh Dambal",
      "author_email": "amdam2013@gmail.com",
      "authored_date": "2021-10-04T23:18:16.000+00:00",
      "committer_name": "Amogh Dambal",
      "committer_email": "amdam2013@gmail.com",
      "committed_date": "2021-10-04T23:18:16.000+00:00",
      "trailers": {},
      "web_url": "https://gitlab.com/thomaspeavler2/cs373-project/-/commit/1b3a07330f65e9b601e212143c6804610fa66d71"
    }
  ],
  {
    "statistics": {
      "counts": {
        "all": 13,
        "closed": 11,
        "opened": 2
      }
    }
  },
  {
    "statistics": {
      "counts": {
        "all": 3,
        "closed": 3,
        "opened": 0
      }
    }
  },
  {
    "statistics": {
      "counts": {
        "all": 5,
        "closed": 5,
        "opened": 0
      }
    }
  },
  {
    "statistics": {
      "counts": {
        "all": 2,
        "closed": 2,
        "opened": 0
      }
    }
  }
]);

test('Renders without crashing', () => {
  render(
    <AboutRoute />
  );
});
