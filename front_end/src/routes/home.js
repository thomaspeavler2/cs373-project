import React from 'react';
import { useHistory } from "react-router-dom";
import {
  Stack, Box, Typography, Paper, Card, CardHeader, CardMedia,
  CardActions, Button, FormControl, OutlinedInput, InputLabel,
  InputAdornment,
} from '@mui/material';
import SearchIcon from '@mui/icons-material/Search';
import { styled } from '@mui/system';
import MainContainer from '../components/MainContainer';
import homeHeader from '../media/homeHeader.jpg';
import personEating from '../media/person_eating.jpg';
import womanEating from '../media/woman_eating.jpg';

const StyledImg = styled(Paper)({
  display: 'block',
  width: '100%',
  height: '325px',
  overflow: 'hidden'
});

const HomeRoute = (props) => {
  const history = useHistory();

  return (
    <MainContainer>
      <Stack spacing={6}>
        <StyledImg>
          <img src={homeHeader} alt="Food" style={{
            minHeight: '325px',
            position: 'relative',
            top: '50%',
            left: '50%',
            transform: 'translate(-50%, -50%)'
          }} />
        </StyledImg>
        <Box sx={{ display: 'flex', justifyContent: 'center' }}>
          <Typography variant="h2" sx={{ paddingLeft: '16px' }}><b>Welcome to BroaderHorizons</b></Typography>
        </Box>
        <Box sx={{
          display: 'flex',
          flexDirection: 'column',
          alignItems: 'center',
          padding: '16px 0px 32px 0px'
        }}>
          <FormControl sx={{
            width: '75vw'
          }}>
            <InputLabel>Search</InputLabel>
            <OutlinedInput
              fullWidth
              sx={{
                backgroundColor: '#FFFFFF'
              }}
              id="input-with-sx"
              label="Search"
              variant="standard"
              onKeyPress={(event) => {
                if (event.key === 'Enter') {
                  const search = event.target.value;
                  if (search !== '') {
                    history.push({
                      pathname: '/searchresults',
                      state: {
                        search: search
                      }
                    })
                  }
                }
              }}
              startAdornment={
                <InputAdornment position="start">
                  <SearchIcon />
                </InputAdornment>
              }
            />
          </FormControl>
        </Box>
        <Box sx={{
          display: 'flex',
          flexDirection: 'row',
          width: '100%',
          alignItems: 'center',
          justifyContent: 'space-between',
          backgroundColor: '#DCDCDC',
        }}
        >
          <img src={personEating} alt="Person Eating" style={{
            maxHeight: '325px',
            position: 'relative',
            padding: '32px 16px'
          }} />
          <Typography variant="h4" sx={{ paddingRight: '16px' }}>Promoting Food Diversity</Typography>
        </Box>
        <Box sx={{
          display: 'flex',
          flexDirection: 'row',
          width: '100%',
          alignItems: 'center',
          justifyContent: 'space-between',
        }}
        >
          <Typography variant="h4" sx={{ paddingLeft: '16px' }}>Enlightening Your Stomach</Typography>
          <img src={womanEating} alt="Person Eating" style={{
            maxHeight: '325px',
            position: 'relative',
            paddingRight: '16px'
          }} />
        </Box>
        <Box sx={{
          display: 'flex',
          flexDirection: 'row',
          justifyContent: 'space-between',
          marginLeft: '64px',
          maxWidth: '100%',
          padding: '64px 32px',
          backgroundColor: '#DCDCDC',
        }}
        >
          <Card sx={{ width: '300px', marginRight: '32px' }}>
            <CardHeader
              title="Search Recipes"
              subheader="Find a bunch of delicious, easy-to-make recipes!"
            />
            <CardActions>
              <Button
                variant="outlined"
                onClick={() => (
                  history.push('/recipes')
                )}
              >
                Search Recipes
              </Button>
            </CardActions>
          </Card>
          <Card sx={{ width: '300px', marginRight: '32px' }}>
            <CardHeader
              title="Search Ingredients"
              subheader="Find some unique, yummy ingredients!"
            />
            <CardActions>
              <Button
                variant="outlined"
                onClick={() => (
                  history.push('/ingredients')
                )}
              >
                Search Ingredients
              </Button>
            </CardActions>
          </Card>
          <Card sx={{ width: '300px', marginRight: '32px' }}>
            <CardHeader
              title="Search Cuisines"
              subheader="Explore diverse cultures from around the world!"
            />
            <CardActions>
              <Button
                variant="outlined"
                onClick={() => (
                  history.push('/cuisines')
                )}
              >
                Search Cuisines
              </Button>
            </CardActions>
          </Card>
        </Box>
      </Stack>
    </MainContainer>
  );
};

export default HomeRoute;
