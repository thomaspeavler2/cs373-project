import React, { useEffect, useState } from 'react';
import {
  Stack, Box, Typography, Paper, Card, CardMedia, CardContent,
  ListItemButton, List, ListItem, ListItemText, CardActionArea,
  Grid
} from '@mui/material';
import MainContainer from '../components/MainContainer';
import ThomasProfilePic from '../media/thomas_profile.jpg';
import JeremyProfilePic from '../media/jeremy_profile.jpg';
import AmoghProfilePic from '../media/amogh_profile.jpg';
import KevinProfilePic from '../media/kevin_profile.jpg';
import { getGitLabData } from '../utils/gitlabAPI';
import {
  gitlabIds, nameMappings, roleMappings, bioMappings, tools, apis
} from '../utils/info';

import ToolCard from '../components/about/toolCard';

import GitLabLogo from '../media/logos/gitlab-icon-rgb.png';

const profileMappings = {
  'amogh-dambal': AmoghProfilePic,
  'jtajonera1': JeremyProfilePic,
  'thomaspeavler2': ThomasProfilePic,
  'kevinhou912': KevinProfilePic,
};

const Biography = (props) => {
  const gitlabData = props.gitlabData;
  const gitlabId = props.gitlabId;

  return (
    <Box sx={{ display: 'flex' }}>
      <Box>
        <Paper elevation={12} sx={{ display: 'flex', marginRight: '16px' }}>
          <img
            src={profileMappings[gitlabId]}
            alt='Person'
            style={{
              minWidth: '150px',
              maxWidth: '300px',
            }}
          />
        </Paper>
      </Box>
      <Box>
        <Stack spacing={1} sx={{ minWidth: '350px' }}>
          <Typography variant='h3'>{nameMappings[gitlabId]}</Typography>
          <Typography variant='h6'>{roleMappings[gitlabId]}</Typography>
          <Typography variant='p'>
            {bioMappings[gitlabId]}
          </Typography>
          <Typography variant="p">
            <b>Total Commits:</b> {gitlabData !== undefined ? gitlabData[gitlabId].totalCommits : 'loading...'}
          </Typography>
          <Typography variant="p"><b>
            Total Issues:</b> {gitlabData !== undefined ? gitlabData[gitlabId].totalIssues : 'loading...'}
          </Typography>
          <Typography variant="p">
            <b>Total Unit Tests:</b> {gitlabData !== undefined ? gitlabData[gitlabId].totalTests : 'loading...'}
          </Typography>
        </Stack>
      </Box>
    </Box>
  );
};

const AboutRoute = (props) => {
  const [gitlabData, setGitlabData] = useState(undefined);

  useEffect(() => {
    getGitLabData((data) => setGitlabData(data));
  }, []);

  return (
    <MainContainer>
      <Stack spacing={6} sx={{ padding: '16px' }}>
        <Box>
          <Typography 
            variant="h3"
            align="center" 
          >
              Our Story
          </Typography>
          <Typography>
          Pixar's Ratatouille espouses the value that "anyone can cook." The act of combining
          together ingredients, spices, herbs, meats, and vegetables and pairing them with
          delicious sauces and wines is a performance that has the power to both sooth
          an empty stomach and nourish an empty soul. We at Broader Horizons believe in
          the power of cooking across cultures and want to make it delectable, and more importantly
          accessible, to everyone. We hope to catalyze cultural connections and help people
          break from the montonony of their current culinary lifestyles.
          <br/>
          <br/>
          Our website allows users to navigate recipes from around the world. Whether you
          have a few ingredients in mind or you want to explore cuisines from around the world,
          our team is dedicated to enhancing your culinary experiences by helping you
          discover the vast array of global food that awaits you. Whether you want to
          discover information about new recipes or ingredients or you want to look into
          a cuisine you haven't tried, we are dedicated to your journey and hope that we can
          broaden your horizons.

          </Typography>
        </Box>
        <Box>
          <Typography variant="h5"><b>Total Commits:</b> {gitlabData !== undefined ? gitlabData.totalCommits : 'loading...'}</Typography>
          <Typography variant="h5"><b>Total Issues:</b> {gitlabData !== undefined ? gitlabData.totalIssues : 'loading...'}</Typography>
          <Typography variant="h5"><b>Total Unit Tests:</b> {gitlabData !== undefined ? gitlabData.totalTests : 'loading...'}</Typography>
        </Box>
        {gitlabIds.map((id) => (
          <Biography key={id} gitlabData={gitlabData} gitlabId={id} />
        ))}

        <Box>
          <Typography variant="h3" align="center"> Developer Info </Typography>
          <Grid container spacing={2} align="center">
            <Grid item xs={6}>
              
              <Card sx={{maxWidth: 350}}>
                <CardActionArea href="https://gitlab.com/thomaspeavler2/cs373-project">
                  <CardMedia component="img" image={GitLabLogo} height="250">
                  </CardMedia>
                  <CardContent>
                    <Typography variant="h6" align="center"> GitLab Repo</Typography>
                  </CardContent>
                </CardActionArea>
              </Card>
              
            </Grid>
            <Grid item xs={6}>
              
              <Card sx={{maxWidth: 350}}>
                <CardActionArea href="https://documenter.getpostman.com/view/17727633/UUy1en98">
                  <CardMedia component="img" image="https://www.postman.com/assets/logos/postman-logo-stacked.svg" height="250">
                  </CardMedia>
                  <CardContent>
                    <Typography variant="h6" align="center"> Postman API</Typography>
                  </CardContent>
                </CardActionArea>
              </Card>
              
            </Grid>
          </Grid>
        </Box>

        <Typography variant="h3" align="center"> Tools </Typography>
        <Grid container spacing={5}>
          {
            tools.map(
              (tool) => (
                <Grid item xs={4}>
                <ToolCard
                  key={tool['name']}
                  name={tool['name']}
                  text={tool['use']}
                  img={tool['image']}
                  link={tool['link']}
                />
                </Grid>
              )
            )
          }
        </Grid>
        <Typography variant="h3" align="center">Data Sources</Typography>
        <Grid container spacing={5}>
          {
            apis.map(
              (api) => (
                <Grid item xs={4}>
                <ToolCard
                  key={api['name']}
                  name={api['name']}
                  text={api['use']}
                  img={api['image']}
                  link={api['link']}
                />
                </Grid>
              )
            )
          }
        </Grid>
      </Stack>
    </MainContainer>
  );
};

export default AboutRoute;
