import React from 'react';
import { Box, Typography } from '@mui/material';
import MainContainer from '../components/MainContainer';
import NetworkGraph from '../utils/visualizationGraphs/NetworkGraph';
import BarGraph from '../utils/visualizationGraphs/BarGraph';
import BeeGraph from '../utils/visualizationGraphs/BeeGraph';

const VisualizationRoute = (props) => {

  return (
    <>
      <MainContainer>
        <Box sx={{
          display: 'flex',
          alignItems: 'center',
          width: '100%',
          flexDirection: 'column'
        }}
        >
          <Typography variant='h4' sx={{ paddingTop: '32px' }}>
            Connections Between Instances
          </Typography>
          <NetworkGraph />
          <Typography variant='h4' sx={{ paddingTop: '32px' }}>
            Recipes and Ingredients Per Cuisine
          </Typography>
          <BarGraph />
          <Typography variant='h4' sx={{ paddingTop: '32px' }}>
            Prep Time for Recipes
          </Typography>
          <BeeGraph />
        </Box>
      </MainContainer>
    </>
  );
};

export default VisualizationRoute;