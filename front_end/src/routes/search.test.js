import React from 'react';
import { render } from "@testing-library/react";
import renderer from 'react-test-renderer';
import * as ApiCalls from '../utils/apiCalls';
import SearchRoute from './search';
import { MemoryRouter } from 'react-router';

jest.mock("react-router-dom", () => ({
  ...jest.requireActual("react-router-dom"),
  useLocation: () => ({
    pathname: "localhost:3000/Recipes"
  })
}));

test('Renders without crashing', () => {
  const apiCall = jest.spyOn(ApiCalls, 'getInstancesWithQueryUrl')
    .mockImplementation((name, callback) => []);
  render(
    <MemoryRouter>
      <SearchRoute name="Recipes"/>
    </MemoryRouter>
  );
});

test('UI did not change', () => {
  const apiCall = jest.spyOn(ApiCalls, 'getInstancesWithQueryUrl')
    .mockImplementation((name, callback) => []);

  const tree = renderer
    .create(
      <MemoryRouter>
        <SearchRoute name='Recipes'/>
      </MemoryRouter>
    )
    .toJSON();
  expect(tree).toMatchSnapshot();
});
