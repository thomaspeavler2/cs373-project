import React, {
  useEffect, useState, useContext, createContext
} from 'react';
import BarAndTable from '../components/search/BarAndTable';
import MainContainer from '../components/MainContainer';
import { Recipe, sortModelObjects } from '../utils/models';
import InstanceView from '../components/search/InstanceView';
import { Switch, Route } from 'react-router';

export const InstanceObjectContext = createContext({
  currentInstance: null,
  setCurrentInstance: () => {}
});

const SearchRoute = (props) => {
  const [currentInstance, setCurrentInstance] = useState(null);

  useEffect(() => {
    setCurrentInstance(null);
  }, [props.name]);

  return (
    <>
      <MainContainer>
        <InstanceObjectContext.Provider value={{ currentInstance, setCurrentInstance }}>
            <BarAndTable name={props.name} />
            <InstanceView name={props.name}/>
        </InstanceObjectContext.Provider>
      </MainContainer>
    </>
  );
};

export default SearchRoute;