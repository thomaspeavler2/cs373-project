import React from 'react';
import { render } from "@testing-library/react";
import renderer from 'react-test-renderer';
import HomeRoute from './home';
import * as ApiCalls from '../utils/apiCalls';
import { mount, configure } from 'enzyme';
import Adapter from '@wojtekmaj/enzyme-adapter-react-17';
import { MemoryRouter } from 'react-router';
import { FormControl } from '@mui/material';
import BarAndTable from '../components/search/BarAndTable';


configure({ adapter: new Adapter() });

test('Renders without crashing', () => {
  render(
    <HomeRoute />
  );
});

/*
  Tests whether there are unexpected changes to the NavBar.
*/
test('UI did not change', () => {
  const tree = renderer
    .create(
      <HomeRoute />
    )
    .toJSON();
  expect(tree).toMatchSnapshot();
});

test('Searching works', () => {
  const apiCall = jest.spyOn(ApiCalls, 'getInstancesWithQueryUrl')
    .mockImplementation((name, callback) => []);

  const wrapper = mount(
    <MemoryRouter>
      <HomeRoute />
      <BarAndTable name="Ingredients"/>
    </MemoryRouter>
  );
  
  wrapper.find(FormControl).first().simulate('change', { target: { value: 'Onions' } });
  wrapper.find(FormControl).first().simulate('keypress', {key: 'Enter'});

  expect(apiCall).toHaveBeenCalled();
});
