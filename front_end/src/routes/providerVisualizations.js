import React, { useEffect } from 'react';
import { Box, Typography } from '@mui/material';
import MainContainer from '../components/MainContainer';
import CircleGraph from '../utils/visualizationGraphs/CircleGraph';
import DonutGraph from '../utils/visualizationGraphs/DonutGraph';
import HistogramGraph from '../utils/visualizationGraphs/HistogramGraph';

const ProviderVisualizationRoute = (props) => {

  return (
    <>
      <MainContainer>
        <Box sx={{
          display: 'flex',
          alignItems: 'center',
          padding: '36px',
          width: '100%',
          flexDirection: 'column'
        }}
        >
          <Typography variant='h4' sx={{ paddingTop: '16px', paddingBottom: '32px' }}>
            Songs Per Artist
          </Typography>
          <CircleGraph />
          <Typography variant='h4' sx={{ paddingTop: '84px', paddingBottom: '32px' }}>
            Relative Songs Per Album
          </Typography>
          <DonutGraph />
          <Typography variant='h4' sx={{ paddingTop: '84px', paddingBottom: '32px' }}>
            Length of Songs
          </Typography>
          <HistogramGraph />
        </Box>
      </MainContainer>
    </>
  );
};

export default ProviderVisualizationRoute;