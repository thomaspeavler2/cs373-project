# # To run: 
# # 1. download webdrivers needed
# #   For safari: Run `safaridriver --enable` once. (If you’re upgrading from a previous macOS release, you may need to use sudo.)
# # 2. Run sudo cp <driver-name> /usr/local/bin (Mac)


import unittest
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import Select

import time

class SeleniumTests(unittest.TestCase):
  def createDriver(self):
    chrome_options = Options()
    chrome_options.add_argument('--no-sandbox')
    chrome_options.add_argument('--window-size=1420,1080')
    chrome_options.add_argument('--headless')
    chrome_options.add_argument('--disable-gpu')
    return webdriver.Chrome(options=chrome_options)

  # Routing Tests:

  # Tests Routing to Recipes
  def testHomeToRecipes(self):
    driver = self.createDriver()
    driver.get('https://broaderhorizons.me')

    time.sleep(2) 

    driver.find_element(By.XPATH, "//a[@href='/recipes']").click()
    
    self.assertEqual("https://www.broaderhorizons.me/recipes", driver.current_url)

    driver.get('https://broaderhorizons.me')
    
    time.sleep(2) 

    driver.find_elements(By.XPATH, "//div/button")[0].click()
    
    self.assertEqual("https://www.broaderhorizons.me/recipes", driver.current_url)

    driver.quit()

  # Tests Routing to Ingredients
  def testHomeToIngredients(self):
    driver = self.createDriver()
    driver.get('https://broaderhorizons.me')
    time.sleep(2) 

    driver.find_element(By.XPATH, "//a[@href='/ingredients']").click()
    
    self.assertEqual("https://www.broaderhorizons.me/ingredients", driver.current_url)

    driver.get('https://broaderhorizons.me')
    
    time.sleep(2) 

    driver.find_elements(By.XPATH, "//div/button")[1].click()
    
    self.assertEqual("https://www.broaderhorizons.me/ingredients", driver.current_url)

    driver.quit()

  # Tests Routing to Cuisines
  def testHomeToCuisines(self):
    driver = self.createDriver()
    driver.get('https://broaderhorizons.me')
    time.sleep(2) 

    driver.find_element(By.XPATH, "//a[@href='/cuisines']").click()
    
    self.assertEqual("https://www.broaderhorizons.me/cuisines", driver.current_url)

    driver.get('https://broaderhorizons.me')
    
    time.sleep(2) 

    driver.find_elements(By.XPATH, "//div/button")[2].click()
    
    self.assertEqual("https://www.broaderhorizons.me/cuisines", driver.current_url)

    driver.quit()

  # Tests Routing to About
  def testHomeToAbout(self):
    driver = self.createDriver()
    driver.get('https://broaderhorizons.me')
    time.sleep(2) 

    driver.find_element(By.XPATH, "//a[@href='/about']").click()
    
    self.assertEqual("https://www.broaderhorizons.me/about", driver.current_url)

    driver.quit()

  # Tests Routing to Home
  def testRecipeToHome(self):
    driver = self.createDriver()
    driver.get('https://broaderhorizons.me/recipes')

    time.sleep(2) 

    driver.find_element(By.XPATH, "//a[@href='/about']").click()

    time.sleep(2) 

    driver.find_element(By.XPATH, "//a[@href='/']").click()
    
    self.assertEqual("https://www.broaderhorizons.me/", driver.current_url)

    driver.quit()

  # Tests viewing instances

  # Tests Viewing Recipe Instance
  def testViewRecipeInstance(self):
    driver = self.createDriver()
    driver.get('https://broaderhorizons.me/recipes')

    time.sleep(10)
    
    recipeInstance = driver.find_element(By.XPATH, "//table/tbody/tr[1]/td[1]").text

    driver.find_element(By.XPATH, "//table/tbody/tr[1]/td[1]").click()

    recipeTitle = driver.find_element(By.XPATH, "//h2/b").text

    self.assertEqual(recipeInstance, recipeTitle)
    
    driver.quit()

  # Tests Viewing Ingredient Instance
  def testViewIngredientInstance(self):
    driver = self.createDriver()
    driver.get('https://broaderhorizons.me/ingredients')

    time.sleep(10)

    # Searching by XPATH is not specific, we would want the second element in the list
    ingredientInstance = driver.find_elements(By.XPATH, "//div/div/div/div/div/div/div/span[1]")[4].text

    driver.find_elements(By.XPATH, "//div/button")[1].click()

    ingredientTitle = driver.find_element(By.XPATH, "//h2/b").text

    self.assertEqual(ingredientInstance, ingredientTitle)
    
    driver.quit()

  # Tests Viewing Cuisine Instance
  def testViewCuisineInstance(self):
    driver = self.createDriver()
    driver.get('https://broaderhorizons.me/cuisines')

    time.sleep(10)

    cuisineInstance = driver.find_element(By.XPATH, "//table/tbody/tr[1]/td[1]").text

    driver.find_element(By.XPATH, "//table/tbody/tr[1]/td[1]").click()

    cuisineTitle = driver.find_element(By.XPATH, "//h2/b").text

    self.assertEqual(cuisineInstance, cuisineTitle)
    
    driver.quit()

  # Tests viewing another instance from current instance

  # Tests Viewing Next Instance from Recipe
  def testNextInstanceFromRecipe(self):
    driver = self.createDriver()
    driver.get('https://broaderhorizons.me/recipes')

    time.sleep(10)

    # Clicks on a recipe
    driver.find_element(By.XPATH, "//table/tbody/tr[1]/td[1]").click()

    time.sleep(10)

    # Click on a related ingredient/cuisine and store it
    instanceTitleOld = driver.find_elements(By.XPATH, "//div/span")[1].text.upper()

    driver.find_elements(By.XPATH, "//div/button")[0].click() 

    time.sleep(10)

    instanceTitleNew = driver.find_element(By.XPATH, "//h2/b").text.upper()

    self.assertEqual(instanceTitleOld, instanceTitleNew)
    
    driver.quit()

  # Tests Viewing Next Ingredient Instance
  def testNextInstanceFromIngredient(self):
    driver = self.createDriver()

    driver.get('https://broaderhorizons.me/')

    driver.find_element(By.XPATH, "//a[@href='/ingredients']").click()

    time.sleep(10)

    # Clicks on a ingredient
    driver.find_elements(By.XPATH, "//div/button")[4].click()

    time.sleep(10)

    # Click on a related ingredient/cuisine and store it
    instanceTitleOld = driver.find_elements(By.XPATH, "//div/span")[1].text.upper()

    driver.find_elements(By.XPATH, "//div/button")[0].click() 

    time.sleep(10)

    instanceTitleNew = driver.find_element(By.XPATH, "//h2/b").text.upper()

    self.assertEqual(instanceTitleOld, instanceTitleNew)
    
    driver.quit()

  # Tests Viewing Next Cuisine Instance
  def testNextInstanceFromCuisine(self):
    driver = self.createDriver()

    driver.get('https://broaderhorizons.me/cuisines')

    time.sleep(10)

    # Clicks on a cuisine
    driver.find_element(By.XPATH, "//table/tbody/tr[1]/td[1]").click()

    time.sleep(10)

    # Click on a related ingredient/cuisine and store it
    instanceTitleOld = driver.find_elements(By.XPATH, "//div/span")[1].text.upper()

    driver.find_elements(By.XPATH, "//div/button")[0].click() 

    time.sleep(10)

    instanceTitleNew = driver.find_element(By.XPATH, "//h2/b").text.upper()

    self.assertEqual(instanceTitleOld, instanceTitleNew)
    
    driver.quit()

  def testFilterRecipes(self):
    driver = self.createDriver()

    driver.get('https://broaderhorizons.me/recipes')

    time.sleep(10)
    driver.find_elements(By.XPATH, "//div/div/div/div/div/div/div/div")[3].click()
    driver.find_element(By.XPATH, "//div/div/ul/li[3]").click()

    time.sleep(2)

    prepTime = driver.find_element(By.XPATH, "//table/tbody/tr[1]/td[2]").text

    exp = "90"

    self.assertEqual(exp, prepTime)

    driver.find_elements(By.XPATH, "//div/div/div/div/div/div/div/div")[5].click()
    driver.find_element(By.XPATH, "//div/div/ul/li[3]").click()

    time.sleep(2)

    act = driver.find_element(By.XPATH, "//table/tbody/tr[1]/td[3]").text

    exp = "16"

    self.assertEqual(exp, act)

    driver.find_elements(By.XPATH, "//div/div/div/div/div/div/div/div")[8].click()
    driver.find_element(By.XPATH, "//div/div/ul/li[2]").click()

    time.sleep(2)

    act = driver.find_element(By.XPATH, "//table/tbody/tr[1]/td[4]").text

    exp = "No"

    self.assertEqual(exp, act)

    driver.find_elements(By.XPATH, "//div/div/div/div/div/div/div/div")[11].click()
    driver.find_element(By.XPATH, "//div/div/ul/li[2]").click()

    time.sleep(2)

    act = driver.find_element(By.XPATH, "//table/tbody/tr[1]/td[5]").text

    exp = "No"

    self.assertEqual(exp, act)

    driver.quit()

  def testFilterCuisines(self):
    driver = self.createDriver()

    driver.get('https://broaderhorizons.me/cuisines')

    time.sleep(10)
    driver.find_elements(By.XPATH, "//div/div/div/div/div/div/div/div")[3].click()
    driver.find_element(By.XPATH, "//div/div/ul/li[3]").click()

    time.sleep(2)

    act = driver.find_element(By.XPATH, "//table/tbody/tr[1]/td[2]").text

    exp = "Albania"

    self.assertEqual(exp, act)

    driver.find_elements(By.XPATH, "//div/div/div/div/div/div/div/div")[5].click()
    driver.find_element(By.XPATH, "//div/div/ul/li[2]").click()

    time.sleep(2)

    act = driver.find_element(By.XPATH, "//table/tbody/tr[1]/td[3]").text

    exp = "Europe"

    self.assertEqual(exp, act)

   
    driver.quit()

  def testFilterIngredients(self):
    driver = self.createDriver()

    driver.get('https://broaderhorizons.me/ingredients')

    time.sleep(10)
    driver.find_elements(By.XPATH, "//div/div/div/div/div/div/div/div")[4].click()
    driver.find_element(By.XPATH, "//div/div/ul/li[3]").click()

    time.sleep(3)

    act = driver.find_elements(By.XPATH, "//div/div/div/div/div/div/p")[2].text

    exp = "Grocery Aisle: Alcoholic Beverages"

    self.assertEqual(exp, act)

    driver.find_elements(By.XPATH, "//div/div/div/div/div/div/div/div")[7].click()
    driver.find_element(By.XPATH, "//div/div/ul/li[2]").click()

    time.sleep(3)

    act = driver.find_elements(By.XPATH, "//div/div/div/div/div/div/p")[4].text

    exp = "Est. Cost ($): 3.08"

    self.assertEqual(exp, act)

    driver.find_elements(By.XPATH, "//div/div/div/div/div/div/div/div")[10].click()
    driver.find_element(By.XPATH, "//div/div/ul/li[1]").click()

    time.sleep(5)

    act = driver.find_elements(By.XPATH, "//div/div/div/div/div/div/p")[1].text

    exp = "Protein (%): 0"

    self.assertEqual(exp, act)

    driver.quit()

  def testSortRecipes(self):
    driver = self.createDriver()

    driver.get('https://broaderhorizons.me/recipes')

    time.sleep(10)

    # Sort by Name 
    driver.find_element(By.XPATH, "//table/thead/tr[1]/th[1]/span").click()
    time.sleep(2)
    cuisineName = driver.find_element(By.XPATH, "//table/tbody/tr[1]/td[1]").text

    lastName = "Zwiebelkuchen"

    self.assertEqual(cuisineName, lastName)

    driver.find_element(By.XPATH, "//table/thead/tr[1]/th[1]/span").click()
    time.sleep(2)
    cuisineName = driver.find_element(By.XPATH, "//table/tbody/tr[1]/td[1]").text

    firstName = "7 Up Cake"

    self.assertEqual(cuisineName, firstName)

    # Sort by Prep Time
    driver.find_element(By.XPATH, "//table/thead/tr[1]/th[2]/span").click()
    time.sleep(2)
    cuisineName = driver.find_element(By.XPATH, "//table/tbody/tr[1]/td[2]").text

    lowestPrep = "0"

    self.assertEqual(cuisineName, lowestPrep)

    driver.find_element(By.XPATH, "//table/thead/tr[1]/th[2]/span").click()
    time.sleep(2)
    cuisineName = driver.find_element(By.XPATH, "//table/tbody/tr[1]/td[2]").text

    highestPrep = "21630"

    self.assertEqual(cuisineName, highestPrep)

    # Sort by Serving
    driver.find_element(By.XPATH, "//table/thead/tr[1]/th[3]/span").click()
    time.sleep(2)
    cuisineName = driver.find_element(By.XPATH, "//table/tbody/tr[1]/td[3]").text

    lowestServe = "1"

    self.assertEqual(cuisineName, lowestServe)

    driver.find_element(By.XPATH, "//table/thead/tr[1]/th[3]/span").click()
    time.sleep(2)
    cuisineName = driver.find_element(By.XPATH, "//table/tbody/tr[1]/td[3]").text

    highestServe = "200"

    self.assertEqual(cuisineName, highestServe)

    driver.quit()

  def testSortIngredients(self):
    driver = self.createDriver()

    driver.get('https://broaderhorizons.me/ingredients')

    time.sleep(10)

    driver.find_elements(By.XPATH, "//div/div/div/div/div/div/div/div")[13].click()
    driver.find_element(By.XPATH, "//div/div/ul/li[1]").click()

    time.sleep(2)

    act = driver.find_elements(By.XPATH, "//div/div/div/div/div/div/p")[0].text

    exp = "Name: 1000 island dressing"

    self.assertEqual(exp, act)

    driver.find_elements(By.XPATH, "//div/div/div/div/div/div/div/div")[13].click()
    driver.find_element(By.XPATH, "//div/div/ul/li[2]").click()

    time.sleep(2)

    act = driver.find_elements(By.XPATH, "//div/div/div/div/div/div/p")[0].text

    exp = "Name: ziti pasta"

    self.assertEqual(exp, act)

    driver.find_elements(By.XPATH, "//div/div/div/div/div/div/div/div")[13].click()
    driver.find_element(By.XPATH, "//div/div/ul/li[3]").click()

    time.sleep(2)

    act = driver.find_elements(By.XPATH, "//div/div/div/div/div/div/p")[4].text

    exp = "Est. Cost ($): 0"

    self.assertEqual(exp, act)

    driver.find_elements(By.XPATH, "//div/div/div/div/div/div/div/div")[13].click()
    driver.find_element(By.XPATH, "//div/div/ul/li[4]").click()

    time.sleep(2)

    act = driver.find_elements(By.XPATH, "//div/div/div/div/div/div/p")[4].text

    exp = "Est. Cost ($): 7552.53"

    self.assertEqual(exp, act)

    driver.find_elements(By.XPATH, "//div/div/div/div/div/div/div/div")[13].click()
    driver.find_element(By.XPATH, "//div/div/ul/li[5]").click()

    time.sleep(2)

    act = driver.find_elements(By.XPATH, "//div/div/div/div/div/div/p")[2].text

    exp = "Grocery Aisle: Alcoholic Beverages"

    self.assertEqual(exp, act)

    driver.find_elements(By.XPATH, "//div/div/div/div/div/div/div/div")[13].click()
    driver.find_element(By.XPATH, "//div/div/ul/li[6]").click()

    time.sleep(2)

    act = driver.find_elements(By.XPATH, "//div/div/div/div/div/div/p")[2].text

    exp = "Grocery Aisle: N/A"

    self.assertEqual(exp, act)

    driver.find_elements(By.XPATH, "//div/div/div/div/div/div/div/div")[13].click()
    driver.find_element(By.XPATH, "//div/div/ul/li[7]").click()

    time.sleep(2)

    act = driver.find_elements(By.XPATH, "//div/div/div/div/div/div/p")[3].text

    exp = "Weight Per Serving (g): 0"

    self.assertEqual(exp, act)

    driver.find_elements(By.XPATH, "//div/div/div/div/div/div/div/div")[13].click()
    driver.find_element(By.XPATH, "//div/div/ul/li[8]").click()

    time.sleep(2)

    act = driver.find_elements(By.XPATH, "//div/div/div/div/div/div/p")[3].text

    exp = "Weight Per Serving (g): 8000"

    self.assertEqual(exp, act)

  def testSortCuisines(self):
    driver = self.createDriver()

    driver.get('https://broaderhorizons.me/cuisines')

    time.sleep(10)

    # Sort by Name 
    driver.find_element(By.XPATH, "//table/thead/tr[1]/th[1]/span").click()
    time.sleep(2)
    inst = driver.find_element(By.XPATH, "//table/tbody/tr[1]/td[1]").text

    lastName = "Zimbabwean"

    self.assertEqual(inst, lastName)

    driver.find_element(By.XPATH, "//table/thead/tr[1]/th[1]/span").click()

    
    time.sleep(2)
    inst = driver.find_element(By.XPATH, "//table/tbody/tr[1]/td[1]").text

    firstName = "Afghan"

    self.assertEqual(inst, firstName)

    # Sort by Country
    driver.find_element(By.XPATH, "//table/thead/tr[1]/th[2]/span").click()
    time.sleep(2)
    inst = driver.find_element(By.XPATH, "//table/tbody/tr[1]/td[2]").text

    country = "Afghanistan"

    self.assertEqual(inst, country)

    driver.find_element(By.XPATH, "//table/thead/tr[1]/th[2]/span").click()
    time.sleep(2)
    inst = driver.find_element(By.XPATH, "//table/tbody/tr[1]/td[2]").text

    country = "Zimbabwe"

    self.assertEqual(inst, country)

    # Sort by Region
    driver.find_element(By.XPATH, "//table/thead/tr[1]/th[3]/span").click()
    time.sleep(2)
    inst = driver.find_element(By.XPATH, "//table/tbody/tr[1]/td[3]").text

    region = "Africa"

    self.assertEqual(inst, region)

    driver.find_element(By.XPATH, "//table/thead/tr[1]/th[3]/span").click()
    time.sleep(2)
    inst = driver.find_element(By.XPATH, "//table/tbody/tr[1]/td[3]").text

    region = "Oceania"

    self.assertEqual(inst, region)

    # Sort by Avg Rainfall
    driver.find_element(By.XPATH, "//table/thead/tr[1]/th[4]/span").click()
    time.sleep(2)
    inst = driver.find_element(By.XPATH, "//table/tbody/tr[1]/td[4]").text

    avg = "54.58587712129826"

    self.assertEqual(inst, avg)

    driver.find_element(By.XPATH, "//table/thead/tr[1]/th[4]/span").click()
    time.sleep(2)
    inst = driver.find_element(By.XPATH, "//table/tbody/tr[1]/td[4]").text

    avg = "2930.900065104667"

    self.assertEqual(inst, avg)

    # Sort by avg temperature
    driver.find_element(By.XPATH, "//table/thead/tr[1]/th[5]/span").click()
    time.sleep(2)
    inst = driver.find_element(By.XPATH, "//table/tbody/tr[1]/td[5]").text

    avg = "-18.22478139573315"

    self.assertEqual(inst, avg)

    driver.find_element(By.XPATH, "//table/thead/tr[1]/th[5]/span").click()
    time.sleep(2)
    inst = driver.find_element(By.XPATH, "//table/tbody/tr[1]/td[5]").text

    avg = "27.524527231850833"

    self.assertEqual(inst, avg)

    driver.quit()

  def testSearchPage(self):
    driver = self.createDriver()
    driver.get('https://broaderhorizons.me/')
    time.sleep(10)
    textbox = driver.find_element(By.XPATH, "//input")

    textbox.send_keys("bacon")

    textbox.send_keys(Keys.ENTER)

    time.sleep(2)

    act = driver.find_elements(By.XPATH, "//div/div/div/span")[1].text

    exp = "Alambre"

    self.assertEqual(exp, act)

    driver.quit

if __name__ == "__main__":
    unittest.main()
