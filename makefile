.DEFAULT_GOAL := docker


docker:
	docker build --file=front_end/Dockerfile -t frontend .
	docker build --file=back_end/Dockerfile -t backend .
	docker-compose -f docker-compose.yml up

clean:
	docker kill $(docker ps -q) #stop all containers
	docker rm -f $(docker ps -a -q) #delete all containers
	docker volume rm $(docker volume ls -q) #delete all volumes
	docker rmi $(docker images -q) #deletes all images

# sourced from and inspired by
# Cultured Foodies makefile
# https://gitlab.com/cs373-group-11/cultured-foodies/~/blob/master/makefile

python-unit-tests:
	python3 back_end/tests.py

selenium-tests:
	python3 front_end/guitests.py

format:
	black ./back_end/*.py

update-db:
	cd back_end
	python3 -m models
