# python 3.7
# Modeled after Cultured Foodies backend testing
# Sourced and modeled from Cultured Foodies (CS373 Group 11, Spring 2021)
# repo: https://gitlab.com/cs373-group-11/cultured-foodies
# file: test.py

from unittest import main, TestCase

from sort_verify import *
import requests


class BackendAPITests(TestCase):
    def test_cuisine_not_found(self):
        r = requests.get("https://api.broaderhorizons.me/api/cuisines/id=-3")
        self.assertEqual(r.status_code, 404)
        self.assertEqual(r.json(), {"error": "-3 not found!"})

    def test_recipe_not_found(self):
        r = requests.get("https://api.broaderhorizons.me/api/recipe/id=-4")
        self.assertEqual(r.status_code, 404)
        self.assertEqual(r.json(), {"error": "-4 not found!"})

    def test_ingredient_not_found(self):
        r = requests.get("https://api.broaderhorizons.me/api/ingredient/id=-2")
        self.assertEqual(r.status_code, 404)
        self.assertEqual(r.json(), {"error": "-2 not found!"})

    def test_all_cuisines(self):
        r = requests.get("https://api.broaderhorizons.me/api/cuisines")
        self.assertEqual(r.status_code, 200)
        js = r.json()
        self.assertEqual(js["count"], 157)

    def test_all_recipes(self):
        r = requests.get("https://api.broaderhorizons.me/api/ingredients")
        self.assertEqual(r.status_code, 200)

        js = r.json()
        self.assertEqual(js["count"], 1264)

    def test_all_ingredients(self):
        r = requests.get("https://api.broaderhorizons.me/api/recipes")
        self.assertEqual(r.status_code, 200)

        js = r.json()
        self.assertEqual(js["count"], 2691)

    def test_get_cuisine_by_id(self):
        r = requests.get("https://api.broaderhorizons.me/api/cuisines/id=3")
        self.assertEqual(r.status_code, 200)

        js = r.json()
        self.assertEqual(len(js), 14)
        self.assertEqual(js["country"], "Algeria")
        self.assertEqual(js["name"], "Algerian")

    def test_get_recipe_by_id(self):
        r = requests.get("https://api.broaderhorizons.me/api/recipe/id=3")
        self.assertEqual(r.status_code, 200)

        js = r.json()
        self.assertEqual(len(js), 17)
        self.assertEqual(js["name"], "Acaraje")

    def test_get_ingredient_by_id(self):
        r = requests.get("https://api.broaderhorizons.me/api/ingredient/id=18")
        self.assertEqual(r.status_code, 200)

        js = r.json()
        self.assertEqual(len(js), 12)
        self.assertEqual(js["name"], "cauliflower")
        self.assertEqual(js["grocery_aisle"], "Produce")

    def test_cuisine_sorting_desc(self):
        r = requests.get("https://api.broaderhorizons.me/api/cuisines?sort=-population")
        self.assertEqual(r.status_code, 200)

        js = r.json()
        results = js["result"]

        L = [obj["population"] for obj in results]
        self.assertTrue(is_sorted_desc(L))

    def test_cuisine_sorting_asc(self):
        r = requests.get("https://api.broaderhorizons.me/api/cuisines?sort=population")
        self.assertEqual(r.status_code, 200)

        js = r.json()
        results = js["result"]

        L = [obj["population"] for obj in results]
        self.assertTrue(is_sorted_asc(L))

    def test_ingredient_sorting_desc(self):
        r = requests.get("https://api.broaderhorizons.me/api/ingredients?sort=-cost")
        self.assertEqual(r.status_code, 200)

        js = r.json()
        results = js["result"]

        L = [obj["estimated_cost"] for obj in results]
        self.assertTrue(is_sorted_desc(L))

    def test_ingredient_sorting_asc(self):
        r = requests.get("https://api.broaderhorizons.me/api/ingredients?sort=cost")
        self.assertEqual(r.status_code, 200)

        js = r.json()
        results = js["result"]

        L = [obj["estimated_cost"] for obj in results]
        self.assertTrue(is_sorted_asc(L))

    def test_recipe_sorting_desc(self):
        r = requests.get("https://api.broaderhorizons.me/api/recipes?sort=-glyidx")
        self.assertEqual(r.status_code, 200)

        js = r.json()
        results = js["result"]

        L = [obj["glycemic_index"] for obj in results]
        self.assertTrue(is_sorted_desc(L))

    def test_recipe_sorting_asc(self):
        r = requests.get("https://api.broaderhorizons.me/api/recipes?sort=glyidx")
        self.assertEqual(r.status_code, 200)

        js = r.json()
        results = js["result"]

        L = [obj["glycemic_index"] for obj in results]
        self.assertTrue(is_sorted_asc(L))


if __name__ == "__main__":
    main()
