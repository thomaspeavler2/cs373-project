# python 3.7.2
# helper functions to
# implement backend
# - sorting
# - searching
# - filtering
# - pagination
# sourced and modeled from the backend API design
# of TexasVotes (Jefferson Ye's Group)
# repo: https://gitlab.com/forbesye/fitsbits
# file: Model.py (Politician, Election, District).py

from models import *

from sqlalchemy import and_, or_, func
from sqlalchemy.sql.expression import false, true

from query_helper import *


def sort_recipe_by(sorting, query, desc=False):
    """
    sort the objects in the current query according to a given
    column/key. Able to sort in either ascending (default) or descending order.

    @param sorting: string that specifies sort column in PostgreSQL DB
    @param query: sqlalchemy Query object that contains all results that
    match URL params at given moment
    @param desc: boolean to sort in descending order (defaults to False)

    @return sqlalchemy same query object that's passed in. function modifies query object.
    """
    col = None

    if sorting == "servings":
        col = Recipe.servings

    elif sorting == "name":
        col = Recipe.name

    elif sorting == "cals":
        col = Recipe.calories

    elif sorting == "prep":
        col = Recipe.prep_time

    elif sorting == "glyidx":
        col = Recipe.glycemic_index

    else:
        col = None

    if col is None:
        return query
    else:
        if desc:
            return query.order_by(col.desc())
        else:
            return query.order_by(col)


def sort_recipes(sorter, query):
    """
    implement sorting according to the URL params

    @param sorter: extracted params from the URL that specifies sort key
    @param query: sqlalchemy Query object that contains all results that
    match URL params at given moment

    @return sqlalchemy same query object that's passed in. function modifies query object.
    """
    if sorter is None:
        return query
    else:
        sorter = sorter[0]

    # sorting in descending order
    # means '-' char is appended to sort string
    sorter = sorter.split("-")
    if len(sorter) > 1:
        return sort_recipe_by(sorter[1], query, True)
    else:
        return sort_recipe_by(sorter[0], query, False)


def filter_recipe_by(query, filtering, what):
    """
    filter the objects in the current query according to a certain
    condition on the given column.

    @param query: sqlalchemy Query object that contains all results that
    match URL params at given moment
    @param filtering: string that specifies column to filter on
    @param what: values to pass in to the constructed PostgreSQL query

    @return sqlalchemy same query object that's passed in. function modifies query object.
    """

    # the true/false hack is bad and i am aware
    # -amogh
    if filtering == "servings":
        query = query.filter(
            and_(Recipe.servings >= what[0], Recipe.servings <= what[1])
        )
    elif filtering == "vegan":
        bool_filter_str = str(what[0])
        if bool_filter_str == "false":
            query = query.filter(Recipe.vegan == false())
        elif bool_filter_str == "true":
            query = query.filter(Recipe.vegan == true())
        else:
            raise ValueError("invalid filtering provided")

    elif filtering == "vegetarian":
        bool_filter_str = str(what[0])
        if bool_filter_str == "false":
            query = query.filter(Recipe.vegetarian == false())
        elif bool_filter_str == "true":
            query = query.filter(Recipe.vegetarian == true())
        else:
            raise ValueError("invalid filtering provided")

    elif filtering == "dairyfree":
        bool_filter_str = str(what[0])
        if bool_filter_str == "false":
            query = query.filter(Recipe.dairy_free == false())
        elif bool_filter_str == "true":
            query = query.filter(Recipe.dairy_free == true())
        else:
            raise ValueError("invalid filtering provided")

    elif filtering == "cals":
        query = query.filter(
            and_(Recipe.calories >= what[0], Recipe.calories <= what[1])
        )

    elif filtering == "prep":
        query = query.filter(
            and_(Recipe.prep_time >= what[0], Recipe.prep_time <= what[1])
        )

    elif filtering == "glyidx":
        query = query.filter(
            and_(Recipe.glycemic_index >= what[0], Recipe.glycemic_index <= what[1])
        )

    return query


def filter_recipes(query, url_params):
    """
    implement filtering according to the specified URL params. applies an
    AND filtering.

    @param query: sqlalchemy Query object that contains all results that
    match URL params at given moment
    @param url_params: all filtering parameters. function will iteratively
    filter through each parameter to enforce an AND relation.

    @return sqlalchemy same query object that's passed in. function modifies query object.
    """

    filterables = [
        "servings",
        "vegan",
        "vegetarian",
        "dairyfree",
        "cals",
        "prep",
        "glyidx",
    ]

    filters = {f: extract_param(f, url_params) for f in filterables}

    for filter_string, filterable in filters.items():
        if filterable is not None:
            query = filter_recipe_by(query, filter_string, filterable)
    return query


def search_recipes(search_terms, query):
    """
    function that implements searching on all instances of the ingredient model.

    @param search_terms: specified search terms as provided in the URL
    @param query: sqlalchemy query object that contains all matching results.

    @return sqlalchemy query object that is modified by the function call
    to return new matches.
    """
    if search_terms is None:
        return query

    terms = search_terms[0].strip().split()
    searches = []

    for term in terms:
        # enables substring matching
        t = "%" + func.lower(term) + "%"

        # sourced from
        # https://stackoverflow.com/questions/16573095/case-insensitive-flask-sqlalchemy-query
        searches.append(func.lower(Recipe.name) == t)

        searches.append(func.lower(Recipe.description).contains(t))

        searches.append(func.lower(Recipe.ingredients).contains(t))

        searches.append(func.lower(Recipe.nutrition_info).contains(t))

    return query.filter(or_(*tuple(searches)))
