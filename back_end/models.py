import flask
from flask import Flask, request, make_response, jsonify
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import create_engine, Column, String, Integer
from flask_cors import CORS
from flask_marshmallow import Marshmallow
from marshmallow import fields, post_dump
from dotenv import load_dotenv

import os
import json


def init_db(app):
    """
    Initialize the database.
    @param app: Flask application
    @return SQLAlchemy object: represents a connection to the PostgreSQL
    database instance on AWS RDS

    Sourced from TexasVotes (Jefferson Ye's Group)
    repo: https://gitlab.com/forbesye/fitsbits
    file: db.py
    function: init_db
    """
    load_dotenv()
    app.debug = True
    app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
    app.config["SQLALCHEMY_DATABASE_URI"] = os.getenv("AWS_DB_KEY")

    db = SQLAlchemy(app)
    return db


def init():
    """
    @returns tuple: Flask application object, SQLAlchemy object, Marshmallow object

    Function to initialize all objects in backend. Initialise
    database, Flask app, Marshmallow object (for schema), and CORS.

    Sourced from TexasVotes (Jefferson Ye's Group)
    repo: https://gitlab.com/forbesye/fitsbits
    file: models.py
    """
    app = Flask(__name__)
    CORS(app)
    db = init_db(app)
    ma = Marshmallow(app)

    return app, db, ma


# initialize application and database and marshmallow container
app, db, ma = init()

"""
All database models and schemas were
sourced and derived from Cultured Foodies (CS373 Group 11, Spring 2021).

repo: https://gitlab.com/cs373-group-11/cultured-foodies
files: models.py, schemas.py
"""


class Cuisine(db.Model):
    """
    represents a Cuisine instance in the database.
    """

    __tablename__ = "cuisines"
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String())
    description = db.Column(db.String())
    country = db.Column(db.String())
    population = db.Column(db.Integer)
    region = db.Column(db.String())
    subregion = db.Column(db.String())
    languages = db.Column(db.String())
    average_rainfall = db.Column(db.Float)
    average_temperature = db.Column(db.Float)
    koppen_classification = db.Column(db.String())
    flag_url = db.Column(db.String())

    # connected components
    recipe_ids = db.Column(db.String())
    ingredient_ids = db.Column(db.String())

    def __repr__(self):
        return f"<Cuisine {self.id}: {self.name}>"


class Recipe(db.Model):
    """
    represents a Recipe instance in the database
    """

    __tablename__ = "recipes"
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String())
    description = db.Column(db.String())
    ingredients = db.Column(db.String())
    servings = db.Column(db.Integer)
    vegan = db.Column(db.Boolean)
    vegetarian = db.Column(db.Boolean)
    dairy_free = db.Column(db.Boolean)
    common_allergens = db.Column(db.String())
    calories = db.Column(db.Float)
    prep_time = db.Column(db.Float)
    glycemic_index = db.Column(db.Float)
    nutrition_info = db.Column(db.String())  # look into storing this as a JSON object
    image_url = db.Column(db.String())
    video_id = db.Column(db.String())

    # connected components
    ingredient_ids = db.Column(db.String())
    cuisine_ids = db.Column(db.String())

    def __repr__(self):
        return f"<Recipe {self.id}: {self.name}>"


class Ingredient(db.Model):
    """
    represents an Ingredient instance in the DB
    """

    __tablename__ = "ingredients"
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String())
    estimated_cost = db.Column(db.Float)
    estimated_cost_unit = db.Column(db.String())
    grocery_aisle = db.Column(db.String())
    macronutrients = db.Column(db.String())
    nutrition_info = db.Column(db.String())
    weight_per_serving = db.Column(db.Float)
    weight_per_serving_unit = db.Column(db.String())
    image_url = db.Column(db.String())

    # connected components
    recipe_ids = db.Column(db.String())
    cuisine_ids = db.Column(db.String())

    def __repr__(self):
        return f"<Ingredient {self.id}: {self.name}>"


# =======
# Schemas
# =======
class CuisineSchema(ma.Schema):
    id = fields.Int(required=True)
    name = fields.Str(required=True)
    description = fields.Str(required=True)
    country = fields.Str(required=True)
    population = fields.Int(required=True)
    flag_url = fields.Str(required=True)
    region = fields.Str(required=True)
    subregion = fields.Str(required=True)
    languages = fields.Str(required=True)
    average_rainfall = fields.Float(required=True)
    average_temperature = fields.Float(required=True)
    koppen_classification = fields.Str(required=True)

    recipe_ids = fields.Str(required=True)
    ingredient_ids = fields.Str(required=True)


class RecipeSchema(ma.Schema):
    id = fields.Int(required=True)
    name = fields.Str(required=True)
    description = fields.Str(required=True)
    ingredients = fields.Str(required=True)
    servings = fields.Int(required=True)
    vegan = fields.Bool(required=True)
    vegetarian = fields.Bool(required=True)
    dairy_free = fields.Bool(required=True)
    common_allergens = fields.Str(required=True)
    calories = fields.Float(required=True)
    prep_time = fields.Float(required=True)
    glycemic_index = fields.Float(required=True)
    nutrition_info = fields.Str(required=True)
    image_url = fields.Str(required=True)
    video_id = fields.Str(required=True)

    cuisine_ids = fields.Str(required=True)
    ingredient_ids = fields.Str(required=True)


class IngredientSchema(ma.Schema):
    id = fields.Int(required=True)
    name = fields.Str(required=True)
    estimated_cost = fields.Float(required=True)
    estimated_cost_unit = fields.Str(required=True)
    grocery_aisle = fields.Str(required=True)
    macronutrients = fields.Str(required=True)
    nutrition_info = fields.Str(required=True)
    weight_per_serving = fields.Float(required=True)
    weight_per_serving_unit = fields.Str(required=True)
    image_url = fields.Str(required=True)

    recipe_ids = fields.Str(required=True)
    cuisine_ids = fields.Str(required=True)


cuisine_schema = CuisineSchema()
recipe_schema = RecipeSchema()
ingredient_schema = IngredientSchema()

cuisines_schema = CuisineSchema(many=True)
recipes_schema = RecipeSchema(many=True)
ingredients_schema = IngredientSchema(many=True)


"""
The push/populate algorithm for populating a database was inspired by 
TexasVotes (Jefferson Ye's Group)
repo: https://gitlab.com/forbesye/fitsbits
file: db_push.py
"""


def push_cuisine(db, json_data: dict):
    """
    Function to add a entry to the database representing one instance
    of a cuisine.
    Sourced from gitlab.com/forbseye/fitsbits/~/blob/master/back-end/db_push.py
    """
    # TODO: move this into a constructor for Cuisine model objects
    entry = dict()
    entry["name"] = json_data["name"]
    entry["description"] = json_data["description"]
    entry["country"] = json_data["country"]
    entry["flag_url"] = json_data["flag"]
    entry["population"] = json_data["population"]
    entry["region"] = json_data["region"]
    entry["subregion"] = json_data["subregion"]
    entry["languages"] = ",".join(json_data["languages"])
    entry["average_rainfall"] = json_data["avg_pr"]
    entry["average_temperature"] = json_data["avg_tas"]
    entry["koppen_classification"] = json_data["koppen"]

    entry["ingredient_ids"] = ",".join([str(x) for x in json_data["ingredient_ids"]])
    entry["recipe_ids"] = ",".join([str(x) for x in json_data["recipe_ids"]])

    instance = Cuisine(**entry)
    db.session.add(instance)
    db.session.commit()


def push_ingredient(db, json_data: dict):
    """
    Function to add an entry to the database representing one
    instance of an ingredient.
    Sourced from same as push_cuisine
    """
    # TODO: move this into a constructor for Ingredient model objects
    entry = dict()
    entry["name"] = json_data["name"]
    entry["estimated_cost"] = json_data["estimated_cost"]
    entry["estimated_cost_unit"] = json_data["estimated_cost_unit"]
    entry["grocery_aisle"] = json_data["aisle"]
    entry["macronutrients"] = json.dumps(json_data["macronutrients"])
    entry["nutrition_info"] = json.dumps(json_data["nutrition_info"])
    entry["weight_per_serving"] = json_data["weight_per_serving"]
    entry["weight_per_serving_unit"] = json_data["weight_per_serving_unit"]
    entry["image_url"] = json_data["image_url"]

    # connected component
    entry["cuisine_ids"] = ",".join([str(x) for x in json_data["cuisine_ids"]])
    entry["recipe_ids"] = ",".join([str(x) for x in json_data["recipe_ids"]])

    instance = Ingredient(**entry)
    db.session.add(instance)
    db.session.commit()


def push_recipe(db, json_data: dict):
    """
    Function to add an entry to the database representing one instance of
    a recipe.
    Sourced from the same as push_cuisine
    """
    entry = dict()
    entry["name"] = json_data["name"]
    entry["description"] = json_data["description"]
    entry["servings"] = json_data["servings"]
    entry["ingredients"] = ",".join(json_data["ingredients"])
    entry["vegetarian"] = json_data["is_vegetarian"]
    entry["vegan"] = json_data["is_vegan"]
    entry["dairy_free"] = json_data["is_dairyfree"]
    # TODO: write an algorithm that filters the health labels
    # from a recipe and then identifies which allergens are
    # present in the recipe
    entry["common_allergens"] = json_data["health_labels"]
    entry["calories"] = json_data["calories"]
    entry["prep_time"] = json_data["prep_time"]
    entry["glycemic_index"] = json_data["glycemic_index"]
    entry["nutrition_info"] = json.dumps(json_data["nutrition_info"])
    entry["image_url"] = json_data["image"]
    entry["video_id"] = json_data["vid"]

    entry["cuisine_ids"] = ",".join([str(x) for x in json_data["cuisine_ids"]])
    entry["ingredient_ids"] = ",".join([str(x) for x in json_data["ingredient_ids"]])

    instance = Recipe(**entry)
    db.session.add(instance)
    db.session.commit()


def populate(db, datapath: str, filename: str):
    """
    @param db: SQLAlchemy object, represents connection to the database
    @param datapath: directory of file
    @param filename: JSON file that stores all instances

    Function to populate the database with all instances sourced from a
    specified input JSON file
    """
    with open(str(datapath + filename), "r") as fp:
        objs = json.load(fp)

    assert type(objs) == list
    assert len(objs) > 0

    if "cuisine" in filename:
        push_func = push_cuisine
    elif "ingredient" in filename:
        push_func = push_ingredient
    elif "recipe" in filename:
        push_func = push_recipe
    else:
        push_func = None

    if push_func is not None:
        for obj in objs:
            push_func(db, obj)
        return True
        db.session.commit()
    else:
        return False


def reset(db):
    """
    Function to clean database before re-populating. BE CAREFUL!

    Sourced from TexasVotes (Jefferson Ye's Group)
    repo: https://gitlab.com/forbesye/fitsbits
    file: db_push.py
    function: reset(db)

    """
    db.session.remove()
    db.drop_all()
    db.create_all()
    print("Database reset!")


if __name__ == "__main__":
    reset(db)

    # hardcoded path where the data is on my local machine
    path = "/Users/amogh/dev/python/cs373/broader-horizons/data/final-instances/"

    c_result = populate(db, datapath=path, filename="cuisines.json")
    if c_result:
        print("Populating cuisines table success!")

    r_result = populate(db, datapath=path, filename="recipes.json")
    if r_result:
        print("Populating recipe table success!")

    i_result = populate(db, datapath=path, filename="ingredients.json")
    if i_result:
        print("Populating ingredients table success!")
