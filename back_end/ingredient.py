# python 3.7.2
# helper functions to
# implement backend
# - sorting
# - searching
# - filtering
# - pagination
# sourced and modeled from the backend API design
# of TexasVotes (Jefferson Ye's Group)
# repo: https://gitlab.com/forbesye/fitsbits
# file: Model.py (Politician, Election, District).py

from models import *

from sqlalchemy import and_, or_, text, func
from query_helper import *


def sort_ingredient_by(sorting, query, desc=False):
    """
    sort the objects in the current query according to a given
    column/key. Able to sort in either ascending (default) or descending order.

    @param sorting: string that specifies sort column in PostgreSQL DB
    @param query: sqlalchemy Query object that contains all results that
    match URL params at given moment
    @param desc: boolean to sort in descending order (defaults to False)

    @return sqlalchemy same query object that's passed in. function modifies query object.
    """
    if sorting == "cost":
        col = Ingredient.estimated_cost

    elif sorting == "aisle":
        col = Ingredient.grocery_aisle

    elif sorting == "wps":
        col = Ingredient.weight_per_serving

    elif sorting == "name":
        col = Ingredient.name

    else:
        col = None

    if col is None:
        return query
    else:
        if desc:
            return query.order_by(col.desc())
        else:
            return query.order_by(col)


def sort_ingredients(sorter, query):
    """
    implement sorting according to the URL params

    @param sorter: extracted params from the URL that specifies sort key
    @param query: sqlalchemy Query object that contains all results that
    match URL params at given moment

    @return sqlalchemy same query object that's passed in. function modifies query object.
    """
    if sorter is None:
        return query

    else:
        sorter = sorter[0]

    # sorting in descending order means
    # '-' char is prepended to sort string
    sorter = sorter.split("-")
    if len(sorter) > 1:
        return sort_ingredient_by(sorter[1], query, True)
    else:
        return sort_ingredient_by(sorter[0], query, False)


def filter_ingredient_by(query, filtering, what):
    """
    filter the objects in the current query according to a certain
    condition on the given column.

    @param query: sqlalchemy Query object that contains all results that
    match URL params at given moment
    @param filtering: string that specifies column to filter on
    @param what: values to pass in to the constructed PostgreSQL query

    @return sqlalchemy same query object that's passed in. function modifies query object.
    """
    if filtering == "cost":
        query = query.filter(
            and_(
                Ingredient.estimated_cost >= what[0],
                Ingredient.estimated_cost <= what[1],
            )
        )
    elif filtering == "aisle":
        query = query.filter(Ingredient.grocery_aisle.in_(what))

    elif filtering == "wps":
        query = query.filter(
            and_(
                Ingredient.weight_per_serving >= what[0],
                Ingredient.weight_per_serving <= what[1],
            )
        )

    elif filtering == "protein":
        what_str = str(what[0])
        if what_str == "high":
            # so.com/questions/32511732/build-sqlalchemy-query-from-string

            where_str = "CAST(macronutrients::json->>'percentProtein' as float) > 50.0"
            query = query.filter(text(where_str))

    elif filtering == "carb":
        what_str = str(what[0])
        if what_str == "low":
            # so.com/questions/32511732/build-sqlalchemy-query-from-string
            where_str = "CAST(macronutrients::json->>'percentCarbs' as float) < 30.0"
            query = query.filter(text(where_str))

    return query


def filter_ingredients(query, url_params):
    """
    implement filtering according to the specified URL params. applies an
    AND filtering.

    @param query: sqlalchemy Query object that contains all results that
    match URL params at given moment
    @param url_params: all filtering parameters. function will iteratively
    filter through each parameter to enforce an AND relation.

    @return sqlalchemy same query object that's passed in. function modifies query object.
    """
    filterables = ["cost", "aisle", "wps", "protein", "carb"]
    filters = {f: extract_param(f, url_params) for f in filterables}

    for filter_str, filterable in filters.items():
        if filterable is not None:
            query = filter_ingredient_by(query, filter_str, filterable)
    return query


def search_ingredients(search_terms, query):
    """
    function that implements searching on all instances of the ingredient model.

    @param search_terms: specified search terms as provided in the URL
    @param query: sqlalchemy query object that contains all matching results.

    @return sqlalchemy query object that is modified by the function call
    to return new matches.
    """
    if search_terms is None:
        return query

    terms = search_terms[0].strip().split()
    searches = []

    for term in terms:
        # enables substring matching
        t = "%" + func.lower(term) + "%"

        # sourced from
        # https://stackoverflow.com/questions/16573095/case-insensitive-flask-sqlalchemy-query
        searches.append(func.lower(Ingredient.name).contains(t))

        searches.append(func.lower(Ingredient.grocery_aisle).contains(t))

        searches.append(func.lower(Ingredient.macronutrients).contains(t))

        searches.append(func.lower(Ingredient.nutrition_info).contains(t))

    return query.filter(or_(*tuple(searches)))
