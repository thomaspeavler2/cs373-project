# python 3.7.2
# helper functions to
# implement backend
# - sorting
# - searching
# - filtering
# - pagination
# sourced and modeled from the backend API design
# of TexasVotes (Jefferson Ye's Group)
# repo: https://gitlab.com/forbesye/fitsbits
# file: Model.py (Politician, Election, District).py

from models import *

from sqlalchemy import and_, or_, func
from query_helper import *


def sort_cuisine_by(sorting, query, desc=False):
    """
    sort the objects in the current query according to a given
    column/key. Able to sort in either ascending (default) or descending order.

    @param sorting: string that specifies sort column in PostgreSQL DB
    @param query: sqlalchemy Query object that contains all results that
    match URL params at given moment
    @param desc: boolean to sort in descending order (defaults to False)

    @return sqlalchemy same query object that's passed in. function modifies query object.
    """

    if sorting == "country":
        col = Cuisine.country

    elif sorting == "population":
        col = Cuisine.population

    elif sorting == "region":
        col = Cuisine.region

    elif sorting == "subregion":
        col = Cuisine.subregion

    elif sorting == "pr":
        col = Cuisine.average_rainfall

    elif sorting == "tas":
        col = Cuisine.average_temperature

    elif sorting == "koppen":
        col = Cuisine.koppen_classification

    elif sorting == "name":
        col = Cuisine.name

    else:
        col = None

    if col is None:
        return query

    else:
        if desc:
            return query.order_by(col.desc())
        else:
            return query.order_by(col)


def sort_cuisines(sorter, query):
    """
    implement sorting according to the URL params

    @param sorter: extracted params from the URL that specifies sort key
    @param query: sqlalchemy Query object that contains all results that
    match URL params at given moment

    @return sqlalchemy same query object that's passed in. function modifies query object.
    """
    if sorter is None:
        return query

    else:
        sorter = sorter[0]

    # sorting in descending order
    # means a '-' char will be prepended to
    # sort string
    sorter = sorter.split("-")
    if len(sorter) > 1:
        return sort_cuisine_by(sorter[1], query, True)
    else:
        return sort_cuisine_by(sorter[0], query, False)


def filter_cuisine_by(query, filtering, what):
    """
    filter the objects in the current query according to a certain
    condition on the given column.

    @param query: sqlalchemy Query object that contains all results that
    match URL params at given moment
    @param filtering: string that specifies column to filter on
    @param what: values to pass in to the constructed PostgreSQL query

    @return sqlalchemy same query object that's passed in. function modifies query object.
    """
    if filtering == "country":
        query = query.filter(Cuisine.country.in_(what))
    elif filtering == "population":
        query = query.filter(
            and_(Cuisine.population >= what[0], Cuisine.population <= what[1])
        )
    elif filtering == "region":
        query = query.filter(Cuisine.region.in_(what))
    elif filtering == "subregion":
        query = query.filter(Cuisine.subregion.in_(what))
    elif filtering == "pr":
        query = query.filter(
            and_(
                Cuisine.average_rainfall >= what[0], Cuisine.average_rainfall <= what[1]
            )
        )
    elif filtering == "tas":
        query = query.filter(
            and_(
                Cuisine.average_temperature >= what[0],
                Cuisine.average_temperature <= what[1],
            )
        )
    elif filtering == "koppen":
        query = query.filter(Cuisine.koppen_classification.in_(what))
    else:
        raise ValueError("invalid filtering specifier")

    return query


def filter_cuisines(query, url_params):
    """
    implement filtering according to the specified URL params. applies an
    AND filtering.

    @param query: sqlalchemy Query object that contains all results that
    match URL params at given moment
    @param url_params: all filtering parameters. function will iteratively
    filter through each parameter to enforce an AND relation.

    @return sqlalchemy same query object that's passed in. function modifies query object.
    """
    filterables = ["country", "pop", "region", "subregion", "pr", "tas", "koppen"]

    filters = {f: extract_param(f, url_params) for f in filterables}

    for filter_string, filterable in filters.items():
        if filterable is not None:
            query = filter_cuisine_by(query, filter_string, filterable)
    return query


def search_cuisines(search_terms, query):
    """
    function that implements searching on all instances of the cuisine model.

    @param search_terms: specified search terms as provided in the URL
    @param query: sqlalchemy query object that contains all matching results.

    @return sqlalchemy query object that is modified by the function call
    to return new matches.
    """
    if search_terms is None:
        return query

    terms = search_terms[0].strip().split()
    searches = []

    for term in terms:
        # enables substring matching
        t = "%" + func.lower(term) + "%"
        # sourced from
        # https://stackoverflow.com/questions/16573095/case-insensitive-flask-sqlalchemy-query
        searches.append(func.lower(Cuisine.name).contains(t))

        searches.append(func.lower(Cuisine.country).contains(t))

        searches.append(func.lower(Cuisine.region).contains(t))

        searches.append(func.lower(Cuisine.subregion).contains(t))

        searches.append(Cuisine.koppen_classification.match(term))

        searches.append(func.lower(Cuisine.languages).contains(t))

    return query.filter(or_(*tuple(searches)))
