from flask import jsonify, Response

from models import *
from query_helper import *

from cuisine import *
from recipe import *
from ingredient import *

NO_PAGINATION = -1
PAGE_DEFAULT = 1
PER_PAGE_DEFAULT = 150

# TODO: make the API results return a JSON object instead of an array
@app.route("/")
def homepage():
    """
    API route for the homepage (https://api.broaderhorizons.me/).
    """
    return "<p>Welcome to the Broader Horizons API!</p>"


@app.route("/test")
def hello_world():
    """
    Initial testing route. Doesn't do anything useful, but it's here for
    backward compatibilty because I am scared to break this file.
    """
    return "<p>Hello, World!</p>"


@app.route("/api/cuisines", methods=["GET"])
def get_cuisines():
    """
    @return dict: JSON-ified data as Python dictionary

    GET endpoint. Fetches all instances of the cuisine model that match
    the specified sort and filter params and search terms.
    Sourced and modeled from design of GET all
    endpoints found in TexasVotes (Jefferson Ye's Group)
    repo: https://gitlab.com/forbesye/fitsbits
    file: app.py
    """
    params = request.args.to_dict(flat=False)
    query = db.session.query(Cuisine)

    # searching
    searcher = extract_param("q", params)
    if searcher is not None:
        query = search_cuisines(searcher, query)

    # filtering
    query = filter_cuisines(query, params)

    # sorting
    sort = extract_param("sort", params)
    query = sort_cuisines(sort, query)

    # pagination
    n_results = query.count()

    p = extract_param("page", params)
    page = PAGE_DEFAULT if p is None else int(p[0])

    pp = extract_param("perPage", params)
    per_page = PER_PAGE_DEFAULT if pp is None else int(pp[0])

    all_cuisines = query.paginate(page=page, per_page=per_page)
    result = cuisines_schema.dump(all_cuisines.items)

    return {"count": n_results, "result": result}


@app.route("/api/cuisines/id=<id>", methods=["GET"])
def get_cuisine(id):
    """
    @param id: unique identifier of instance
    @return dict: JSONified version of instance data

    GET endpoint. Fetches a specified instance of the cuisine model, where
    the key is the ID. This ID is a primary key in the PostgreSQL DB. I accidentally
    kept this one as plural and the other ones as singular; cannot be changed
    because of backwards compatibility with the frontend.

    Sourced and modeled from Cultured Foodies (CS373 Group 11, Spring 2021)
    repo: https://gitlab.com/cs373-group-11/cultured-foodies
    file: app.py
    method: get_cuisine_id
    """
    cuisine = Cuisine.query.get(id)
    if cuisine is None:
        r = flask.Response(
            json.dumps({"error": id + " not found!"}), mimetype="application/json"
        )
        r.status_code = 404
        return r
    return cuisine_schema.jsonify(cuisine)


@app.route("/api/ingredients", methods=["GET"])
def get_ingredients():
    """
    @return dict: JSON-ified data as Python dictionary

    GET endpoint. Fetches all instances of the ingredient model matching the
    applied filtering and sorting params along with specified search terms.
    Sourced and modeled from design of GET all
    endpoints found in TexasVotes (Jefferson Ye's Group)

    repo: https://gitlab.com/forbesye/fitsbits
    file: app.py
    """
    params = request.args.to_dict(flat=False)

    query = db.session.query(Ingredient)

    # searching
    searcher = extract_param("q", params)
    if searcher is not None:
        query = search_ingredients(searcher, query)

    # filtering
    query = filter_ingredients(query, params)

    # sorting
    sort = extract_param("sort", params)
    query = sort_ingredients(sort, query)

    # pagination
    n_results = query.count()

    p = extract_param("page", params)
    page = PAGE_DEFAULT if p is None else int(p[0])

    pp = extract_param("perPage", params)
    per_page = PER_PAGE_DEFAULT if pp is None else int(pp[0])

    all_ingredients = query.paginate(page=page, per_page=per_page)
    result = ingredients_schema.dump(all_ingredients.items)

    return {"count": n_results, "result": result}


@app.route("/api/ingredient/id=<id>", methods=["GET"])
def get_ingredient(id):
    """
    @param id: unique identifier of instance
    @return dict: JSONified version of instance data

    GET endpoint. Fetches a specified instance of the ingredient model, where
    the key is the ID. This ID is a primary key in the PostgreSQL DB.
    Sourced and modeled from Cultured Foodies (CS373 Group 11, Spring 2021)
    repo: https://gitlab.com/cs373-group-11/cultured-foodies
    file: app.py
    method: get_ingredient_id
    """
    ingredient = Ingredient.query.get(id)
    if ingredient is None:
        r = flask.Response(
            json.dumps({"error": id + " not found!"}), mimetype="application/json"
        )
        r.status_code = 404
        return r
    return ingredient_schema.jsonify(ingredient)


@app.route("/api/recipes", methods=["GET"])
def get_recipes():
    """
    GET endpoint. Fetches all instances of the recipe model matching the
    specified filter and sorting params along with search terms.

    @return dict: JSON-ified data as Python dictionary

    Sourced and modeled from design of GET all
    endpoints found in TexasVotes (Jefferson Ye's Group)

    repo: https://gitlab.com/forbesye/fitsbits
    file: app.py
    """
    params = request.args.to_dict(flat=False)

    query = db.session.query(Recipe)

    # searching
    searcher = extract_param("q", params)
    if searcher is not None:
        query = search_recipes(searcher, query)

    # filtering
    query = filter_recipes(query, params)

    # sorting
    sort = extract_param("sort", params)
    query = sort_recipes(sort, query)

    # pagination
    n_results = query.count()

    p = extract_param("page", params)
    page = PAGE_DEFAULT if p is None else int(p[0])

    pp = extract_param("perPage", params)
    per_page = PER_PAGE_DEFAULT if pp is None else int(pp[0])

    all_recipes = query.paginate(page=page, per_page=per_page)
    result = recipes_schema.dump(all_recipes.items)

    return {"count": n_results, "result": result}


@app.route("/api/recipe/id=<id>", methods=["GET"])
def get_recipe(id):
    """
    @param id: unique identifier of instance
    @return dict: JSONified version of instance data

    GET endpoint. Fetches a specified instance of the recipe model, where
    the key is the ID. This ID is a primary key in the PostgreSQL DB.
    """
    recipe = Recipe.query.get(id)
    if recipe is None:
        r = flask.Response(
            json.dumps({"error": id + " not found!"}), mimetype="application/json"
        )
        r.status_code = 404
        return r
    return recipe_schema.jsonify(recipe)


if __name__ == "__main__":
    app.run(host="0.0.0.0", port=5000, debug=True)
