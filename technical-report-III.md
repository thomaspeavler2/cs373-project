# Broader Horizons Technical Report

## Our Story
Pixar's Ratatouille espouses the value that "anyone can cook". The act of combining
together ingredients, spices, herbs, meats, and vegetables and pairing them with
delicious sauces and wines is a performance that has the power to both sooth
an empty stomach and nourish an empty soul. We at Broader Horizons believe in 
the power of cooking across cultures and want to make it delectable, and more importantly
accessible, to everyone. We hope to catalyze cultural connections and help people
break from the montonony of their current culinary lifestyles. 

Our website allows users to navigate recipes from around the world. Whether you 
have a few ingredients in mind or you want to explore cuisines from around the world, 
our team is dedicated to enhancing your culinary experiences by helping you
discover the vast array of global food that awaits you. Whether you want to 
discover information about new recipes or ingredients or you want to look into
a cuisine you haven't tried, we are dedicated to your journey and hope that we can
broaden your horizons.

> "Meals make the society"
> Anthony Bourdain

# Phase 1 #
### User Stories for our project for phase 1 ###
**User Story 1 (The Frugal Vegetarian)**
> I recently started a vegetarian diet, but I’m realizing that I don’t really know what I’m doing when it comes to meal prep. I have a small budget and I’m not the best in the kitchen. I’m looking for some cheap easy recipes to start off with that have a lot of crossover ingredients.

> Response: For this user story, we decided to implement this in Phase II since it's out of scope for Phase I. 
The features of BroaderHorizons enable filtering for vegetarian options.

> Action: We have implemented the vegan label for each recipe and we will add the filtering feature in phase 3.

> Estimated: 1 hour

> Actual: 1.5 hours


**User Story 2 (The Nut Allergen)**
> I have an allergy, but I’ve always enjoyed the little that I’ve tried of Thai food. I’m sort of a nervous eater, and I don’t like ordering things in restaurants unless I know exactly what’s in them. I’m hoping for a resource to check and see if there are any common allergens in Thai recipes.

> Response: For this user story, we pushed the implementation to Phase II; it's out of scope for Phase I. However, it's not a built-in feature of our web app. We decided to make sure to add a "common allergens" attribute to a particular recipe and/or cuisine to help this user.

> Action: We have implemented the allergens label for each recipe.

> Estimated: 1 hour

> Actual: 1.5 hours

**User Story 3 (Historical Recipe Search)**
> I’m starting to cook for myself for the first time, but I’ve never really taken the time to learn how. I remember vaguely some dishes from my childhood that seem easy, but I can’t remember what they’re called and what’s in them. I want to be able to browse through some recipes and see if I can find something that my Grandma used to cook for us on special occasions!

> Response: We determined that this would be out of scope for Phase I. Furthermore, though we think this might be possible, but we would need more information about what type of queries the user would be making before we can determine whether that's something BroaderHorizons can support.

> Action: This is out of scope for phase 2 since it require searching. Also we haven't obtain any further specific information, we can't implement this feature.

> Estimated: Unble to do in this phase

> Actual: Unble to do in this phase

**User Story 4 (A Specific Geographical Query)**
> I’m going on a trip around South America soon, but I don’t know what the food over there tastes like. I’m interested in what kind of foods I’d find there, especially because I don’t want to embarrass myself when I’m there. I’d also like to make some of the dishes from the region to see what I like and don’t like before I go.

> Response: Though this user story is out of scope for Phase I, it's in scope for Phase II, which is where we will implement it! BroaderHorizons offers the option to search for cuisines!

> Action: We have implemented the cuisines page that include the region information 

> Estimated: 2 hours

> Actual: 1.5 hours

**User Story 5 (Ingredient Inquiry)**
> It’s nearing Halloween and I bought way too many pumpkins. They’re fun as a novelty, but I’m worried I’m going to get bored of eating the same pumpkin dish every single day. I need to find varying recipes that feature them and follow them for dinner and breakfast for the rest of the month.

> Response: This is out of scope for Phase I but implementable for Phase II! We pushed the implementation there. Ingredients have connections to common recipes they're featured in.

> Action: We have the ingredients data in the database and we will implement the search function in the next phase.

> Estimated: Unable to do in this phase

> Actual: Unable to do in this phase


### Customer Stories for phase 1 ###
**Customer Story 1 (Lyric Based Recognition)**
> I overheard my crush playing one of their favorite songs, but I wasn't able to hear a lot of the lyrics. I only caught a few keywords and/or phrases, and I don't know much else about it. I would love to be able to find some matching possibilities for a certain set of lyrics to help narrow down what songs it could be so I can impress my crush. Can you help?

> Response from the developers: They are considering implementing this feature in the future


**Customer Story 2 (Album Fight)**
> My friend group is very passionate about music. They have recently spent a lot of time fighting over which is the better album: CLB by Drake or Donda by Kanye. I have hated all of these conversations and want to put an end to this discussion by taking a data-oriented solution to this problem. I want to be able to do a deep dive on both albums and compare the two on metrics like popularity, streams, lyrical comparison, analysis, etc. Can you help me save my friend group?

> Response from the developers: They can implement this feature to offer more information about the song in Phase 2 of the project.

**Customer Story 3 (Hop on the Beat)**
> I love when artists collab on music, and I think that most songs get better when there are multiple artists with lyrics and verses on the track. I want to be able to find good songs that are the results of collaborations by specific artists. Can I search through your website to find the hottest collabs for a given year or a given set of artists?

> Response from the developers: They are planning to implement this in phase 2 to provide more information about collaborations.

**Customer Story 4 (Where Have I Heard That?)**
> I'm a RTF major at UT doing some work researching the use of various musical leitmotifs in media. My research centers around how songs in various genres - pop, rock, classical, country, etc. - are utilized based on their popularity at the time and other attributes to evoke a specific cinematic effect. I want to be able to query your website to find this information, searching for songs used in different works and filtering them based on certain specific criteria.

> Response from the developers: The developers are trying to implement his in the future and they are going to offer filter function for the users.

**Customer Story 5 (Artist Discovery)**
> I just went to ACL and absolutely loved Doja Cat's set. I realized that I have a lot of her music to listen to in order to become a bonafide stan. I want to be able to do a deep dive on Doja Cat, with as much of her music and connected albums as possible - from her own albums to albums and songs where she's featured.

> Response from the developers: The developers have already add more about the artist on the website.



# Phase 2 #
### User Stories for our project for phase 2 ###
**User Story 1 (Stewing over Stew)**
> I used that Irish stew recipe I found on your website and loved it! I ended up buying too many lamb chops though. Can you suggest more recipes that use the same ingredient? 

> Response: Yeah, we are going to add more recipes that are using the same ingredient on the ingredient page.

> Actions: We have the ingredients data in the database and we will implement the search function in the next phase.

> Estimated: Unable to do in this phase

> Actual: Unable to do in this phase

**User Story 2 (Permission to Parmesan)**
> I saw that the Alfredo recipe called for parmesan cheese. I was wondering if there is a way to substitute it with another ingredient, or if there were other Italian dishes without the cheese. I have a mild case of lactose intolerance but have always admired Italian cuisine.

> Response: That is a great idea, we have the feature to show recipes with the allergens. We will implement the filter in phase 3.

> Actions: We have the allergens label for the recipes in the database, and we will implement filter in phase 3.

> Estimated: Unable to do in this phase

> Actual: Unable to do in this phase

**User Story 3 (Pondering Poutine)**
> After moving to America I have found a great variety of foods, but something I miss about my Canadian homeland is the great taste of poutine. I was hoping to find a recipe for it on your website. Any other Canadian dishes would be appreciated!

> Response: I think we have Canadian cuisines on the site and you can probably find the recipes that you want on our website.

> Actions: We have the country information for the cuisines so it can be find on the website.

> Estimated: 1 hour

> Actual: 1.5 hours

**User Story 4 (A lot of Allergies)**
> I found some great recipes on your website which I really want to try out with my friends. However, I cannot seem to grasp how to replace the ingredients which my friends are allergic to. Can I be provided with an alternative ingredient or non-allergic recipe for my allergic friends? Is there a way for me to see common allergies in recipes?

> Response: Each of our recipes comes with the allergens information so you will be safe if you follow the instructions.

> Actions: We have the allergens label for the recipes in the database, and we will implement filter in phase 3.

> Estimated: Unable to do for this phase.

> Actual: Unable to do for this phase.

**User Story 5 (Too Hot To Handle)**
> I really enjoy a lot of different cuisines from around the world, but I’m not the best at handling spice, and usually need a warning ahead of time. Is there a way I could sort by how spicy certain foods are? Also, is there a way to note which cuisines are popular for their spicier dishes?

> Response: That is a great suggestion, we will consider this opinion for phase 3, thank you!

> Actions: We will consider this idea for the next phase.

> Estimated: Unable to do during this phase

> Actual: Unable to do during this phase

### Customer Stories for phase 2 ###
**Customer Story 1 (Music Video)**
> Hi, I am a music lover who listens to music all the time. I follow a couple of artists so that I can keep up on their latest release music. It is always cool to see their music video when they release the song. I wonder if you guys can add MVs for the music and I think it will save me a lot of effort to try to find it on youtube instead.

> Response: The developers say that this is out the scope for the current phase and they will consider this feature in the future.

**Customer Story 2 (I'm Legally Blind)**
> My eyes have not been doing too hot lately, staring at my computer all day. I have difficulty seeing the buttons on the homepage - they blend in with the background. Is there a chance y'all could make it easier for my poor eyes to see your buttons?

> Response: The developer say that they have already change the UI of the website to make the buttons easier to see by changing the background to black and text to white.

**Customer Story 3 (Music Durations!)**
> Hi, I'm a Music-now user and I absolutely love the website. Currently, I am noticing that the music duration for all the music is zero. I want to know if you guys can update it so that I can plan to listen to the music for a certain amount of time. Thank you!

> Response: This is implemented in phase 2 and the developer believe that they have all the duration out on the page.

**Customer Story 4 (So Many Tabs)**
> I am a huge fan of Google Chrome. I constantly have 20 plus tabs open at any given time. I am constantly switching back and forth between tabs, and the first thing I look for when finding a tab is the image and name of the tab. It would be so incredibly awesome if the image and name of your website's tab were descriptive of your website.

> Response: The developers say that they have made the changes and the changes will be deployed in the future.

**Customer Story 5 (Artist Information)**
> Hi, I am a Music-now user and I love to listen to music during my spare time. I sometimes like to browse through the internet to try to understand the music and know more about the artist. It will be great to have more information about the artist and the reason behind the creation of the website.

> Response: This feature is implemented in this phase and the developers indicate that there is many information about the artist on the website.


# Phase 3 #
### User Stories for our project for phase 3 ###
**User Story 1 (Ingredient Recipes)**
> Lox is my absolute favorite food in the world, but I only saw one recipe attached to the ingredient, and it wasn't bagels and lox, which I had used to get to the ingredient page in the first place. I started clicking around to other ingredients, and I realized that a lot of ingredients only had one recipe attached, even if they were part of other recipes. Is there a way to show a few more recipes on the ingredient page?


> Response: Thank you for letting us know, we will look into this a have it fixed as soon as possible.

> Actions: We will look back to the backend and have it fixed. Some of the data in the database might be corrupted,
so we may have to work to fix it by removing corrupted rows in PostgreSQL.

> Estimated: 30 min.

> Actual: 30 min.


**User Story 2 (A Pick of Peppers)**
> I'm really starting to get into nutrition and I've read that there's a lot of health benefits of eating peppers. However, there's a lot of different kinds of peppers, and I don't know where to start. I want to be able to search for 'pepper' and get back a list of all peppers, bell and otherwise.

> Response: This is a great suggestion. We will consider it after we implement the search function.

> Actions: We will consider this in the next phase. This was implemented using model-wide search in the backend API.

> Estimated: 1 hour 30 min.

> Actual: 30 min.


**User Story 3 (Cuisine Recipes)**
> I've spent a while clicking through the different cuisine pages and seeing all the related recipes. I've been really interested in learning how to make some of them, but for whatever reason, the recipe links on some of the cuisine pages (like Indian) don't take me to the recipe instance page. It would be great to be able to click and go, instead of having to track down the recipe on the recipe model page.


> Response: Hi, Sorry for the inconvenience. We think that this is due to the large amount of data that is loading for the website. Maybe give it a couple of seconds and before clicking the recipe. Thank you!

> Actions: This function is working now. 

> Estimated: 1 min.

> Actual: 1 min.


**User Story 4 (Quick Link)**
> I've had a  great time learning about different cuisines from around the world on your website. I've loved the summaries, and I noticed that you included a link to the wikipedia page. This is just a small thing, but it would be nice to be able to click that link and go straight to the page.


> Response: Thank you for the suggestion. It is a great idea; however, We have provided the relevant information for the cuisines. We might consider adding it in the future.

> Actions: This is out of the scope of the phase. We only want to provide a snapshot of information to the user, and want to minimize the 
number of clickable aspect on our webpage and model views as a UI/UX philosophy.

> Estimated: 1 min.

> Actual: 1 min.


**User Story 5 (On a Budget)**
> I loved using your site to find different recipes. I've been on a budget this month, so I was wondering if there was a way to sort recipes by how cheap the ingredients are. If that's not feasible, maybe you could have a range of the prices of the ingredients as an attribute on the recipe instance page?


> Response: Thank you for the suggestion. This feature will be deployed at the end of this phase. 

> Actions: We have implement the sorting base on price range for the recipe and ingredients.

> Estimated: 2 hour

> Actual: 2 hour

### Customer Stories for phase 3 ###
**Customer Story 1 (Artist information)**
> Hi, I am a MusicNow user and I was browsing on the website as usual because I am enjoying using it. When I go to the artist page, I was looking around all the artists, I see some artists are age 0. I am wondering what is that means.  And I wonder if there will be an order based on age function on the artist's page. It would be interesting if the function can be deployed in the future.

>response: Some artists have an age of 0 because it is a group of musicians and there is not a single age that represents them, while others have an unknown age.

**Customer Story 2 (Most popular music)**
> I don't follow the news in music so I don't know what is the top 10 popular music today. But I still want to know the top 10 popular music so I don't feel left out when I talk about music with my friends.  I wonder if MusicNow can add this feature to the song page and the album page.

>response: The developer says that the sorting base on popularity is now available 

**Customer Story 3 (Date)**
> I am currently looking for music that are released in 2021. I was browsing MusicNow and did not find the option of filtering music base on year. It will be great if this feature can be added to the website.

>response: The developer might add this feature in the next phase of the project.

**Customer Story 4 (Sort music by name)**
> Hi, I am a MusicNow user. I want to sort the music in the song by alphabet but I didn't find a sort option. I am wondering if the developers will implement this feature in the future.

>response: The developers have implemented this in this phase.

**Customer Story 5 (Search music by name)**
> Hi, I am a MusicNow User and I wonder if the website will have a searching option in the future. I just asked my friend about music and I was going to search for the music by its name and I don't find the search option. It will be great if the developers can implement a search option.

>response: The developers have implemented the search feature and it will be deployed soon.


## RESTful API
Our API was generated by Postman. To check out the documentation, please visit
https://documenter.getpostman.com/view/17727633/UUy1en98. 

### Cuisines API Endpoints
- GET all cuisines
  - endpoint: `api.broaderhorizons.me/cuisines`
  - Returns a list of JSON objects, where each object represents a cuisine
  instance. Each instance contains data about the cuisine's name, description,
  climate data, country-specific information, and more. 
- GET cuisine by id
  - endpoint: `api.broaderhorizons.me/cuisines/id=<id>`
  - Returns a JSON object representing a single cuisine
  instance. This instance contains data about the cuisine's name, description,
  climate data, country-specific information, and more. 


### Recipes API Endpoints
- GET all recipes
  - endpoint: `api.broaderhorizons.me/recipes`
  - Returns a list of JSON objects, where each object represents a recipe
  instance. This instance contains data about the recipe's name, description,
  nutrition information, ingredient list, prep information, and more. 
- GET recipe by id
  - endpoint: `api.broaderhorizons.me/recipe/id=<id>`
  - Returns a JSON object representing a single recipe
  instance. This instance contains data about the recipe's name, description,
  nutrition information, ingredient list, prep information, and more. 

### Ingredients API Endpoints
- GET all ingredients
  - endpoint: `api.broaderhorizons.me/ingredients`
  - Returns a list of JSON objects, where each object represents a ingredient
  instance. This instance contains data about the ingredient's name, nutrition
  information, macronutrient breakdown, and more. 
- GET ingredient by id
  - endpoint: `api.broaderhorizons.me/ingredient/id=<id>`
  - Returns a JSON object representing a single ingredient
  instance. This instance contains data about the ingredient's name, nutrition
  information, macronutrient breakdown, and more. 

## Models

Recipe
- id 
- name 
- description 
- ingredients
- servings 
- vegan 
- vegeterian 
- dairy_free 
- common_allergens 
- prep_time 
- glycemic_index 
- nutrition_info 
- cuisines_id 
- ingredients_id 

Ingredient
- id 
- name 
- estimated_cost 
- estimated_cost_unit 
- grocery_aisel
- macronutrients 
- nutrition_info 
- weight_per_serving
- weight_per_serving_unit 
- cuisines_id 
- recipes_id 

Cuisine
- id 
- name 
- description 
- country 
- population 
- region 
- subregion 
- language 
- average_rainfall 
- average_temperature 
- koppen_classification 
- recipes_id 
- ingredients_id 

### Filterable/Sortable Attributes
Recipe
- vegan 
- vegeterian 
- dairy_free 
- prep_time 
- glycemic_index

Ingredient
- estimated_cost  
- grocery_aisle
- macronutrients 
- weight_per_serving

Cuisine
- country 
- population 
- region 
- subregion 
- language 
- average_rainfall 
- average_temperature 
- koppen_classification 

### Searchable Attributes
Recipe
- id
- name
Ingredient
- id
- name
Cuisine
- id
- name

### Media
Recipe
- text, images, videos, lists

Ingredient
- text, images, tables

Cuisine
- text, maps, images

### Connections
Recipe - Ingredient
An ingredient is a vital component of a recipe, and likewise, a recipe is made up of ingredients.

Ingredient - Cuisine
An ingredient is often associated with or heavily used in a certain type of cuisine. Likewise, a cuisine can often be reliant on certain ingredients. 

Cuisine - Recipe
A recipe can be an integral part of certain cuisines, or can be a staple of that diet. Likewise, a cuisine can be made up of several key recipes.

## Tools

### Backend
- AWS Amplify
  * used to deploy our frontend application to AWS cloud computers
- AWS Elastic Beanstalk
  * used to deploy our backend application to AWS cloud computers
- Postman
  * Used to document our API calls and test their functionality
- Docker
  * Used to containerize backend to run independently from any machine and 
  not force toolchain requirements
- Namecheap
  * Used to get a domain name for our website
- GitLab
  * Hosts code repository for backend code
- Flask
  * Web framework that we used to write our Python API in
- PostgreSQL and AWS Relational Database Services (RDS)
  * used to create our database and host our database instance

### Frontend
- ReactJS
  * Renders the data from our databases onto our website
- MaterialUI
  * Used for frontend styling in addition to ReactJS
- Selenium
  * Tests the frontend user interface and makes sure that all functionality works for a user
- Docker
  * Used to containerize frontend to run independently from any machine
- GitLab
  * Hosts code repository for frontend code


### APIs and Data Sources

**Spoonacular**
https://spoonacular.com/food-api/docs
  * Gets details for recipes, ingredients, and cuisines
  * scraped detailed nutrition information for recipes and ingredients

**The Meal DB**
https://www.themealdb.com/api.php 
  * We used this to obtain information about various potential ingredients 

**Edamam Recipe API**
https://developer.edamam.com/edamam-docs-recipe-api
  * We used this API to obtain and scrape recipe data, including information
  about connections to ingredients and cuisines as well as detailed nutritional
  breakdowns of recipe by type.

**World Food Guide API**
https://worldfood.guide/api
  * We used this API to obtain information about various cuisines and recipes,
  as well as link specific recipes to their respective cuisines and vice versa

**REST Countries API**
https://restcountries.com/
  * We used this API to find information about various countries and other geographical entities that are present for a particular type of cuisine.

**The World Bank Climate API**
https://datahelpdesk.worldbank.org/knowledgebase/articles/902061-climate-data-api
  * We used this API to obtain climate information for particular areas of the world that have a particular cuisine. This allows us to tie a specific kind of climate to a cuisine

**Imsea API**
https://imsea.herokuapp.com
  * We utilized this API to find pictures of 
  various ingredients and provide a new form of media
  for our model instances

**Youtube API**
https://developers.google.com/youtube
  * We utilized the `youtube-search` Python package
    to programmatically search and scrape the Youtube API to 
    find Youtube videos that provide instructions on creating
    our recipes

## Hosting
We obtained our domain name from Namecheap and used Custom DNS features provided
by AWS Amplify's Route53 to connect that domain to the nameservers provided
by AWS. 

## GitLab Repository
For developers, please visit our repository 
at https://gitlab.com/thomaspeavler2/cs373-project

## Phase 2 Features
**Pagination**
> With regard to pagination, we did all processing in the frontend. We query all of the instances and store the data in an array. Then, we can simply slice the array according to the current page range. We did not realize that the expectation was to incorporate pagination into the flask api. In the next phase, we will refactor our pagination code to account for the api handling pagination.

**Database**
> For the database, we first scrapped the data from the api sources to get the data that we want from the api. We formatted the scrapped data to match our model instances. Then, we construct the table by deciding the columns, which is the attributes of the models, and put them into the postgres database as table. We have 3 table in total and each of the table represent one of our data model.

**Testing**
We implemented unit tests and acceptance tests across both the front and backends 
of our web app.

> For the frontend, we utilized Jest testing and created snapshots; it was relatively simple
to integrate those Jest tests into the Gitlab CI/CD pipelines using Docker images 
and configurations we sourced and built. 

> For frontend integration testing, we utilized Selenium as the testing framework and
made sure that the overall user experience was relatively pleasant. We ensured that
users can navigate from instance page to instance page and from model to model. We
also ensured that the clickable areas of the page were significant and easy to see, 
and that the flow of the page made sense.

> We used Python's built-in `unittest` library for backend unit testing. We were able
to write a `make` recipe that integrated well into the GitLab CI configuration and
thus run those unit tests on each deployment.

> We implemented API testing using Postman. We refined our original API and fixed the
various endpoints. After that, we ensured that the resulting calls to the API were correct
and that the returned the right schema along with a 200 status code. 

## Phase 3 Features
**Filtering**
Our backend API provides a rich and robust filtering mechanism. For each model, we
provide several options for the user to choose from in the UI/frontend layer. Once 
the user choose the filters (the query criteria), the frontend layer constructs a set of 
query parameters and makes an API call with a specific URL with those parameters. In the 
backend Flask API, we extract those parameters and use them to determine how to 
construct specific SQL queries we make on our PostgreSQL database instance with certain 
conditionals. The database returns a set of rows that match the filter criteria, which
the backend returns as a JSON object. The frontend consumes this JSON object and renders 
all the matching results beautifully on the UI layer.

**Searching**
Our backend API provide a Google-like search feature. When the user inputs text into the search bar, 
the frontend parses those search terms and passes them via URL param to the backend via
our established API endpoints. The backend unpacks these search terms and uses loose matching
and substring matching to determine whether certain reasonable columns (such as name, description, etc.)
are a match for the given term(s). We create an OR relationship between all search terms and query our
PostgreSQL database for all matching rows. The backend then renders those results as a JSON object.
The frontend consumes this JSON object and renders all the matching results beautifully on the UI layer.

**Sorting**
Our backend API provides robust sorting functionality. At the time, we only allow for sorting on 
one column/key at a time. The frontend UI layer offers the user a choice of sort option. The user's 
selection is parsed by the frontend, which constructs a query param that is passed to the API call
made to the backend. We provide a feature for sorting in either ascending (default) or descending (character prepend)
order. The backend API unpacks the sort column, parses the order, and makes an SQL query and tells the
database to render results in the requested order. The resulting query is then dispatched to a JSON 
object and returned. The frontend consumes this JSON object and renders all the matching results beautifully on the UI layer.
